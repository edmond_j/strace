/*
** signals.c for strace in /home/edmond_j//rendu/B4/Advanced Unix System/strace/rendu
**
** Made by julien edmond
** Login   <edmond_j@epitech.net>
**
** Started on  Fri May 10 16:51:34 2013 julien edmond
** Last update Thu May 16 13:35:24 2013 julien edmond
*/

#include	<signal.h>
#include	<sys/wait.h>
#include	<stdio.h>
#include	<sys/ptrace.h>
#include	<string.h>
#include	"strace.h"

int		is_sig(int status, int pid)
{
  int		ret;
  siginfo_t	info;
  const char	*reason;

  ret = 0;
  if (WIFSIGNALED(status))
    fputs("signaled\n", stderr);
  if (WIFSTOPPED(status) && WSTOPSIG(status) != SIGTRAP
      && WSTOPSIG(status) != SIGCHLD
      && ptrace(PTRACE_GETSIGINFO, pid, 0, &info) != -1
      && info.si_signo != SIGSTOP && info.si_signo != SIGTRAP)
    {
      if ((reason = get_reason(info.si_signo, info.si_code)))
	fprintf(stderr,
		"--- {si_signo=%s, si_code=%s, si_addr=%p} (%s) ---\n",
		get_signame(info.si_signo), reason, info.si_addr,
		strsignal(info.si_signo));
      else
	fprintf(stderr,
		"--- {si_signo=%s, si_code=%d, si_addr=%p} (%s) ---\n",
		get_signame(info.si_signo), info.si_code, info.si_addr,
		strsignal(info.si_signo));
      ret = 1;
    }
  return (ret);
}

int	print_end_status(int status)
{
  if (WIFSTOPPED(status))
    fprintf(stderr, "+++ killed by %s +++\n", get_signame(WSTOPSIG(status)));
  return (0);
}

const char		*get_signame(int sigil)
{
  const char		*sent;
  static const char	*sigent[] = {"SIG_0", "SIGHUP", "SIGINT", "SIGQUIT",
				     "SIGILL", "SIGTRAP", "SIGABRT", "SIGBUS",
				     "SIGFPE", "SIGKILL", "SIGUSR1", "SIGSEGV",
				     "SIGUSR2", "SIGPIPE", "SIGALRM",
				     "SIGTERM", "SIGSTKFLT", "SIGCHLD",
				     "SIGCONT", "SIGSTOP", "SIGTSTP",
				     "SIGTTIN", "SIGTTOU", "SIGURG", "SIGXCPU",
				     "SIGXFSZ", "SIGVTALRM", "SIGPROF",
				     "SIGWINCH", "SIGIO", "SIGPWR", "SIGSYS",
				     "SIGRTMIN"};

  if (sigil >= 0 && sigil < (int)(sizeof(sigent) / sizeof(char *)))
    sent = sigent[sigil];
  else
    sent = 0;
  return (sent);
}
