/*
** strace.c for strace in /home/edmond_j//rendu/B4/Advanced Unix System/strace/rendu
**
** Made by julien edmond
** Login   <edmond_j@epitech.net>
**
** Started on  Wed May  8 09:05:20 2013 julien edmond
** Last update Thu May 16 11:46:12 2013 julien edmond
*/

#include	<signal.h>
#include	<errno.h>
#include	<unistd.h>
#include	<stdio.h>
#include	<string.h>
#include	<sys/ptrace.h>
#include	<sys/user.h>
#include	<sys/wait.h>
#include	"strace.h"

int			is_syscall(unsigned long instr)
{
  unsigned short	*ptr;

  ptr = (unsigned short *)&instr;
  return (ptr[0] == 0x050f);
}

int				father(int pid)
{
  char				quit;
  struct user_regs_struct	regs;
  long				instr;
  char				ret;
  int				status;

  ret = 0;
  quit = 0;
  while (!quit && wait4(pid, &status, 0, NULL) == pid)
    if (ptrace(PTRACE_GETREGS, pid, NULL, &regs) == -1
	|| ((instr = ptrace(PTRACE_PEEKTEXT, pid, regs.rip, NULL)) == -1
	    && errno != 0) || is_sig(status, pid))
      quit = 1;
    else
      {
	if (!ret && is_syscall(instr) && !(quit = print_syscall(&regs, pid)))
	  ret = 1;
	else if (ret && (int)regs.orig_rax != -1
		 && !(quit = print_ret(&regs, pid)))
	  ret = 0;
	quit = (ptrace(PTRACE_SINGLESTEP, pid, NULL, NULL) == -1);
      }
  if (ret)
    fputs("|Ret ?\n", stderr);
  return (print_end_status(status));
}

int	son(char **av)
{
  if (execvpe(av[0], av, environ) == -1
      && fprintf(stderr, "\nstrace: %s: %s\n", av[0], strerror(errno)) == -1)
    perror("fprintf");
  return (1);
}

int	strace(char **av)
{
  int	pid;
  int	ret;

  if ((ret = (pid = fork()) == -1))
    perror("fork");
  else if (pid)
    {
      if (ptrace(PTRACE_ATTACH, pid, 0, 0) == -1)
	{
	  perror("ptrace attach");
	  ret = 1;
	}
      else
	{
	  ret = father(pid);
	  ret = (ptrace(PTRACE_DETACH, pid, 0, 0) == -1);
	}
    }
  else
    ret = son(av);
  return (ret);
}
