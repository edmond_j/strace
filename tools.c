/*
** tools.c for strace in /home/edmond_j//rendu/B4/Advanced Unix System/strace/strace
**
** Made by julien edmond
** Login   <edmond_j@epitech.net>
**
** Started on  Mon May 13 09:11:27 2013 julien edmond
** Last update Wed May 15 13:17:25 2013 julien edmond
*/

#include	<string.h>
#include	<stdio.h>
#include	<errno.h>
#include	<sys/ptrace.h>
#include	<ctype.h>

static const char * const	g_errno_ent[] =
  {
#include "errnoent.txt"
  };

void		read_from_son(int pid, void *from, void *to, unsigned size)
{
  long		ret;
  unsigned	to_read;

  while (size > 0
	 && ((ret = ptrace(PTRACE_PEEKDATA, pid, from, 0)) != -1
	     || errno == 0))
    {
      to_read = (size > 8 ? 8 : size);
      memcpy(to, &ret, to_read);
      size -= to_read;
      from += to_read;
      to += to_read;
    }
  if (ret == -1 && errno != 0)
    perror("ptrace PEEKDATA");
}

void		print_string(int pid, void *from)
{
  int		i;
  char		*buff;
  long		ret;

  i = 8;
  if (!from)
    fprintf(stderr, "null");
  else
    {
      fprintf(stderr, "\"");
      while (i >= 8
	     && ((ret = ptrace(PTRACE_PEEKDATA, pid, from, 0)) != -1
		 || errno == 0))
	{
	  i = -1;
	  buff = (char *)&ret;
	  while (++i < 8 && buff[i])
	    fprintf(stderr, "%c", buff[i]);
	  from += 8;
	}
      fprintf(stderr, "\"");
    }
}

void		print_errno(int ret)
{
  int		errcode;

  errcode = 0;
  if (ret < -1)
    {
      errcode = ret / -1;
      ret = -1;
    }
  fprintf(stderr, "%d", ret);
  if (errcode)
    {
      if (errcode < (int)(sizeof(g_errno_ent) / (sizeof(const char *))))
	fprintf(stderr, " %s (%s)", g_errno_ent[errcode], strerror(errcode));
      else
	fprintf(stderr, " %d (%s)", errcode, strerror(errcode));
    }
}

void		print_mem(char *buff, int size)
{
  int		i;

  i = -1;
  while (++i < size)
    fprintf(stderr, (isprint(buff[i]) ? "%c" : "\\%o"), buff[i]);
}
