/*
** strace.h for strace in /home/edmond_j//rendu/B4/Advanced Unix System/strace/rendu
** 
** Made by julien edmond
** Login   <edmond_j@epitech.net>
** 
** Started on  Wed May  8 09:04:40 2013 julien edmond
** Last update Mon May 13 14:42:33 2013 julien edmond
*/

#ifndef		STRACE_H__
# define	STRACE_H__

# include	<sys/user.h>
# include	<unistd.h>

typedef	struct
{
  int		code;
  const char	*reason;
}		t_sig_reason;

typedef	struct
{
  int		sigil;
  const char	*(*get_reason)(int code);
}		t_get_reason;

typedef void	(*t_printer)(struct user_regs_struct *regs, pid_t pid);

typedef struct
{
  long		sys_code;
  t_printer	print_arg;
  t_printer	print_ret;
}		t_syscall;

int		strace(char **av);
int		print_syscall(struct user_regs_struct *regs, pid_t pid);
int		print_ret(struct user_regs_struct *regs, pid_t pid);

int		is_sig(int status, int pid);
int		print_end_status(int status);
const char	*get_signame(int sigil);

const char	*get_reason(int sigil, int code);

const char	*get_sigill_reason(int code);
const char	*get_sigfpe_reason(int code);
const char	*get_sigsegv_reason(int code);
const char	*get_sigbus_reason(int code);
const char	*get_sigchld_reason(int code);
const char	*get_sigpoll_reason(int code);

void		read_from_son(int pid, void *from, void *to, unsigned size);
void		print_errno(int ret);
void		print_mem(char *buff, int size);
void		print_string(int pid, void *from);

#endif		/* STRACE_H__ */
