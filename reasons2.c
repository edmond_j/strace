/*
** reasons2.c for strace in /home/edmond_j//rendu/B4/Advanced Unix System/strace/rendu
** 
** Made by julien edmond
** Login   <edmond_j@epitech.net>
** 
** Started on  Fri May 10 18:20:31 2013 julien edmond
** Last update Fri May 10 18:20:54 2013 julien edmond
*/

#include	<signal.h>
#include	"strace.h"

const char			*get_sigbus_reason(int code)
{
  const char			*sent;
  unsigned			i;
  static const t_sig_reason	reasons[] = { {BUS_ADRALN, "BUS_ADRALN"},
					      {BUS_ADRERR, "BUS_ADRERR"},
					      {BUS_OBJERR, "BUS_OBJERR"} };

  i = 0;
  sent = 0;
  while (i < sizeof(reasons) / sizeof(t_sig_reason) && reasons[i].code != code)
    ++i;
  if (i < sizeof(reasons) / sizeof(t_sig_reason))
    sent = reasons[i].reason;
  return (sent);
}

const char			*get_sigchld_reason(int code)
{
  const char			*sent;
  unsigned			i;
  static const t_sig_reason	reasons[] = { {CLD_EXITED, "CLD_EXITED"},
					      {CLD_KILLED, "CLD_KILLED"},
					      {CLD_DUMPED, "CLD_DUMPED"},
					      {CLD_TRAPPED, "CLD_TRAPPED"},
					      {CLD_STOPPED, "CLD_STOPPED"},
					      {CLD_CONTINUED,
					       "CLD_CONTINUED"} };

  i = 0;
  sent = 0;
  while (i < sizeof(reasons) / sizeof(t_sig_reason) && reasons[i].code != code)
    ++i;
  if (i < sizeof(reasons) / sizeof(t_sig_reason))
    sent = reasons[i].reason;
  return (sent);
}

const char			*get_sigpoll_reason(int code)
{
  const char			*sent;
  unsigned			i;
  static const t_sig_reason	reasons[] = { {POLL_IN, "POLL_IN"},
					      {POLL_OUT, "POLL_OUT"},
					      {POLL_MSG, "POLL_MSG"},
					      {POLL_ERR, "POLL_ERR"},
					      {POLL_PRI, "POLL_PRI"},
					      {POLL_HUP, "POLL_HUP"} };

  i = 0;
  sent = 0;
  while (i < sizeof(reasons) / sizeof(t_sig_reason) && reasons[i].code != code)
    ++i;
  if (i < sizeof(reasons) / sizeof(t_sig_reason))
    sent = reasons[i].reason;
  return (sent);
}
