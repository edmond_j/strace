/*
** reasons.c for strace in /home/edmond_j//rendu/B4/Advanced Unix System/strace/rendu
** 
** Made by julien edmond
** Login   <edmond_j@epitech.net>
** 
** Started on  Fri May 10 17:47:10 2013 julien edmond
** Last update Fri May 10 18:20:49 2013 julien edmond
*/

#include	<signal.h>
#include	"strace.h"

const char			*get_sig_reason(int sigil, int code)
{
  static const t_get_reason	getters[] = { {SIGILL, &get_sigill_reason},
					      {SIGFPE, &get_sigfpe_reason},
					      {SIGSEGV, &get_sigsegv_reason},
					      {SIGBUS, &get_sigbus_reason},
					      {SIGCHLD, &get_sigchld_reason},
					      {SIGPOLL, &get_sigpoll_reason} };
  unsigned			i;
  const char			*sent;

  i = 0;
  sent = 0;
  while (i < sizeof(getters) / sizeof(t_get_reason)
	 && getters[i].sigil != sigil)
    ++i;
  if (i < sizeof(getters) / sizeof(t_get_reason))
    sent = getters[i].get_reason(code);
  return (sent);
}

const char			*get_reason(int sigil, int code)
{
  const char			*sent;
  unsigned			i;
  static const t_sig_reason	reasons[] = { {SI_USER, "SI_USER"},
					      {SI_KERNEL, "SI_KERNEL"},
					      {SI_QUEUE, "SI_QUEUE"},
					      {SI_TIMER, "SI_TIMER"},
					      {SI_MESGQ, "SI_MESGQ"},
					      {SI_ASYNCIO, "SI_ASYNCIO"},
					      {SI_SIGIO, "SI_SIGIO"},
					      {SI_TKILL, "Si_TKILL"} };

  i = 0;
  while (i < sizeof(reasons) / sizeof(t_sig_reason) && reasons[i].code != code)
    ++i;
  if (i < sizeof(reasons) / sizeof(t_sig_reason))
    sent = reasons[i].reason;
  else
    sent = get_sig_reason(sigil, code);
  return (sent);
}

const char			*get_sigill_reason(int code)
{
  const char			*sent;
  unsigned			i;
  static const t_sig_reason	reasons[] = { {ILL_ILLOPC, "ILL_ILLOPC"},
					      {ILL_ILLOPN, "ILL_ILLOPN"},
					      {ILL_ILLADR, "ILL_ILLADR"},
					      {ILL_ILLTRP, "ILL_ILLTRP"},
					      {ILL_PRVOPC, "ILL_PRVOPC"},
					      {ILL_PRVREG, "ILL_PRVREG"},
					      {ILL_COPROC, "ILL_COPROC"},
					      {ILL_BADSTK, "ILL_BADSTK"} };

  i = 0;
  sent = 0;
  while (i < sizeof(reasons) / sizeof(t_sig_reason) && reasons[i].code != code)
    ++i;
  if (i < sizeof(reasons) / sizeof(t_sig_reason))
    sent = reasons[i].reason;
  return (sent);
}

const char			*get_sigfpe_reason(int code)
{
  const char			*sent;
  unsigned			i;
  static const t_sig_reason	reasons[] = { {FPE_INTDIV, "FPE_INTDIV"},
					      {FPE_INTOVF, "FPE_INTOVF"},
					      {FPE_FLTDIV, "FPE_FLTDIV"},
					      {FPE_FLTOVF, "FPE_FLTOVF"},
					      {FPE_FLTUND, "FPE_FLTUND"},
					      {FPE_FLTRES, "FPE_FLTRES"},
					      {FPE_FLTINV, "FPE_FLTINV"},
					      {FPE_FLTSUB, "FPE_FLTSUB"} };

  i = 0;
  sent = 0;
  while (i < sizeof(reasons) / sizeof(t_sig_reason) && reasons[i].code != code)
    ++i;
  if (i < sizeof(reasons) / sizeof(t_sig_reason))
    sent = reasons[i].reason;
  return (sent);
}

const char			*get_sigsegv_reason(int code)
{
  const char			*sent;
  unsigned			i;
  static const t_sig_reason	reasons[] = { {SEGV_MAPERR, "SEGV_MAPERR"},
					      {SEGV_ACCERR, "SEGV_ACCERR"} };

  i = 0;
  sent = 0;
  while (i < sizeof(reasons) / sizeof(t_sig_reason) && reasons[i].code != code)
    ++i;
  if (i < sizeof(reasons) / sizeof(t_sig_reason))
    sent = reasons[i].reason;
  return (sent);
}
