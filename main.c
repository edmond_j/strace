/*
** main.c for strace in /home/edmond_j//rendu/B4/Advanced Unix System/strace/rendu
** 
** Made by julien edmond
** Login   <edmond_j@epitech.net>
** 
** Started on  Wed May  8 09:00:44 2013 julien edmond
** Last update Mon May 13 11:44:46 2013 julien edmond
*/

#include	<stdio.h>
#include	"strace.h"

int	main(int ac, char **av)
{
  int	ret;

  if (ac < 2)
    ret = (fprintf(stderr, "Usage: %s command\n", av[0]) == -1);
  else
    ret = strace(&av[1]);
  return (ret);
}
