#!/bin/bash

FILENAME=$1

cat $FILENAME | while read LINE
do
       echo -e "void\t\tprint_args_$LINE(struct user_regs_struct *);" >> "aff_prototypes.h"
       echo -e "void\t\tprint_ret_$LINE(struct user_regs_struct *);" >> "aff_prototypes.h"
	echo "" >> "aff_prototypes.h"
done

echo "PROTOTYPES DONE"
