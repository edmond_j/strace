##
## Makefile for strace in /home/edmond_j//rendu/B4/Advanced Unix System/strace/rendu
##
## Made by julien edmond
## Login   <edmond_j@epitech.net>
##
## Started on  Wed May  8 08:57:39 2013 julien edmond
## Last update Thu May 16 10:18:11 2013 martin lequeux-gruninger
##

include		printers/syscalls

NAME=		strace

SRC=		main.c \
		strace.c \
		printer.c \
		signals.c \
		reasons.c \
		reasons2.c \
		tools.c

SRC+=		$(SYSCALL_FILES)

OBJ=		$(SRC:.c=.o)

CFLAGS=		-W -Wall -Wextra -Werror -D _GNU_SOURCE -I .

$(NAME):	$(OBJ)
		@ echo -e "Compilation of \033[31;1m"$(NAME)"\033[m starting..."
		cc -o $(NAME) $(CFLAGS) $(OBJ)
		@ echo -e "\033[31;1mCompilation has finished sucessfully.\033[m"
		@ cat .make_win
		@ play -q .fanfare.ogg&

all:		$(NAME)

clean:
		rm -f $(OBJ) *~ \#*\#
		@ echo -e "\033[35;1mClean done.\033[m"

fclean:		clean
		rm -f $(NAME)
		@ echo -e "\033[35mFclean "$(NAME)" done.\033[m"

re:		fclean all

.c.o:
		@ $(CC) -c -o $@ $< -I $(CFLAGS) \
		&& echo -e "File" "\"\033[36m"$<"\033[m\" to \"\033[33m"$@"\033[m\" with flags \"\033[32m"$(CFLAGS)"\033[m\" => \033[31;1mOk.\033[m" \
		|| (echo -e "File" "\"\033[31m"$<"\033[m\" => \033[31mERROR\033[m." && exit 1)

.PHONY:		all clean fclean re .c.o
