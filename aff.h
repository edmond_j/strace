/*
** aff.h for strace in /home/edmond_j//rendu/B4/Advanced Unix System/strace/strace/AFF_SCRIPTS
**
** Made by julien edmond
** Login   <edmond_j@epitech.net>
**
** Started on  Sat May 11 11:38:08 2013 julien edmond
** Last update Thu May 16 09:26:38 2013 julien edmond
*/

#ifndef		AFF_H__
# define	AFF_H__

# include	<sys/user.h>
# include	<stdio.h>

# ifndef	_LINUX_TIME_H
#  include	<sched.h>

void	print_sched_param(pid_t pid, struct sched_param *);

# endif

void	print_open_flags(int flags);
void	print_stat(pid_t pid, void *);

void		print_args_read(struct user_regs_struct *, pid_t);
void		print_ret_read(struct user_regs_struct *, pid_t);

void		print_args_write(struct user_regs_struct *, pid_t);
void		print_ret_write(struct user_regs_struct *, pid_t);

void		print_args_open(struct user_regs_struct *, pid_t);
void		print_ret_open(struct user_regs_struct *, pid_t);

void		print_args_close(struct user_regs_struct *, pid_t);
void		print_ret_close(struct user_regs_struct *, pid_t);

void		print_args_stat(struct user_regs_struct *, pid_t);
void		print_ret_stat(struct user_regs_struct *, pid_t);

void		print_args_fstat(struct user_regs_struct *, pid_t);
void		print_ret_fstat(struct user_regs_struct *, pid_t);

void		print_args_lstat(struct user_regs_struct *, pid_t);
void		print_ret_lstat(struct user_regs_struct *, pid_t);

void		print_args_poll(struct user_regs_struct *, pid_t);
void		print_ret_poll(struct user_regs_struct *, pid_t);

void		print_args_lseek(struct user_regs_struct *, pid_t);
void		print_ret_lseek(struct user_regs_struct *, pid_t);

void		print_args_mmap(struct user_regs_struct *, pid_t);
void		print_ret_mmap(struct user_regs_struct *, pid_t);

void		print_args_mprotect(struct user_regs_struct *, pid_t);
void		print_ret_mprotect(struct user_regs_struct *, pid_t);

void		print_args_munmap(struct user_regs_struct *, pid_t);
void		print_ret_munmap(struct user_regs_struct *, pid_t);

void		print_args_brk(struct user_regs_struct *, pid_t);
void		print_ret_brk(struct user_regs_struct *, pid_t);

void		print_args_rt_sigaction(struct user_regs_struct *, pid_t);
void		print_ret_rt_sigaction(struct user_regs_struct *, pid_t);

void		print_args_rt_sigprocmask(struct user_regs_struct *, pid_t);
void		print_ret_rt_sigprocmask(struct user_regs_struct *, pid_t);

void		print_args_rt_sigreturn(struct user_regs_struct *, pid_t);
void		print_ret_rt_sigreturn(struct user_regs_struct *, pid_t);

void		print_args_ioctl(struct user_regs_struct *, pid_t);
void		print_ret_ioctl(struct user_regs_struct *, pid_t);

void		print_args_pread(struct user_regs_struct *, pid_t);
void		print_ret_pread(struct user_regs_struct *, pid_t);

void		print_args_pwrite(struct user_regs_struct *, pid_t);
void		print_ret_pwrite(struct user_regs_struct *, pid_t);

void		print_args_readv(struct user_regs_struct *, pid_t);
void		print_ret_readv(struct user_regs_struct *, pid_t);

void		print_args_writev(struct user_regs_struct *, pid_t);
void		print_ret_writev(struct user_regs_struct *, pid_t);

void		print_args_access(struct user_regs_struct *, pid_t);
void		print_ret_access(struct user_regs_struct *, pid_t);

void		print_args_pipe(struct user_regs_struct *, pid_t);
void		print_ret_pipe(struct user_regs_struct *, pid_t);

void		print_args_select(struct user_regs_struct *, pid_t);
void		print_ret_select(struct user_regs_struct *, pid_t);

void		print_args_sched_yield(struct user_regs_struct *, pid_t);
void		print_ret_sched_yield(struct user_regs_struct *, pid_t);

void		print_args_mremap(struct user_regs_struct *, pid_t);
void		print_ret_mremap(struct user_regs_struct *, pid_t);

void		print_args_msync(struct user_regs_struct *, pid_t);
void		print_ret_msync(struct user_regs_struct *, pid_t);

void		print_args_mincore(struct user_regs_struct *, pid_t);
void		print_ret_mincore(struct user_regs_struct *, pid_t);

void		print_args_madvise(struct user_regs_struct *, pid_t);
void		print_ret_madvise(struct user_regs_struct *, pid_t);

void		print_args_shmget(struct user_regs_struct *, pid_t);
void		print_ret_shmget(struct user_regs_struct *, pid_t);

void		print_args_shmat(struct user_regs_struct *, pid_t);
void		print_ret_shmat(struct user_regs_struct *, pid_t);

void		print_args_shmctl(struct user_regs_struct *, pid_t);
void		print_ret_shmctl(struct user_regs_struct *, pid_t);

void		print_args_dup(struct user_regs_struct *, pid_t);
void		print_ret_dup(struct user_regs_struct *, pid_t);

void		print_args_dup2(struct user_regs_struct *, pid_t);
void		print_ret_dup2(struct user_regs_struct *, pid_t);

void		print_args_pause(struct user_regs_struct *, pid_t);
void		print_ret_pause(struct user_regs_struct *, pid_t);

void		print_args_nanosleep(struct user_regs_struct *, pid_t);
void		print_ret_nanosleep(struct user_regs_struct *, pid_t);

void		print_args_getitimer(struct user_regs_struct *, pid_t);
void		print_ret_getitimer(struct user_regs_struct *, pid_t);

void		print_args_alarm(struct user_regs_struct *, pid_t);
void		print_ret_alarm(struct user_regs_struct *, pid_t);

void		print_args_setitimer(struct user_regs_struct *, pid_t);
void		print_ret_setitimer(struct user_regs_struct *, pid_t);

void		print_args_getpid(struct user_regs_struct *, pid_t);
void		print_ret_getpid(struct user_regs_struct *, pid_t);

void		print_args_sendfile(struct user_regs_struct *, pid_t);
void		print_ret_sendfile(struct user_regs_struct *, pid_t);

void		print_args_socket(struct user_regs_struct *, pid_t);
void		print_ret_socket(struct user_regs_struct *, pid_t);

void		print_args_connect(struct user_regs_struct *, pid_t);
void		print_ret_connect(struct user_regs_struct *, pid_t);

void		print_args_accept(struct user_regs_struct *, pid_t);
void		print_ret_accept(struct user_regs_struct *, pid_t);

void		print_args_sendto(struct user_regs_struct *, pid_t);
void		print_ret_sendto(struct user_regs_struct *, pid_t);

void		print_args_recvfrom(struct user_regs_struct *, pid_t);
void		print_ret_recvfrom(struct user_regs_struct *, pid_t);

void		print_args_sendmsg(struct user_regs_struct *, pid_t);
void		print_ret_sendmsg(struct user_regs_struct *, pid_t);

void		print_args_recvmsg(struct user_regs_struct *, pid_t);
void		print_ret_recvmsg(struct user_regs_struct *, pid_t);

void		print_args_shutdown(struct user_regs_struct *, pid_t);
void		print_ret_shutdown(struct user_regs_struct *, pid_t);

void		print_args_bind(struct user_regs_struct *, pid_t);
void		print_ret_bind(struct user_regs_struct *, pid_t);

void		print_args_listen(struct user_regs_struct *, pid_t);
void		print_ret_listen(struct user_regs_struct *, pid_t);

void		print_args_getsockname(struct user_regs_struct *, pid_t);
void		print_ret_getsockname(struct user_regs_struct *, pid_t);

void		print_args_getpeername(struct user_regs_struct *, pid_t);
void		print_ret_getpeername(struct user_regs_struct *, pid_t);

void		print_args_socketpair(struct user_regs_struct *, pid_t);
void		print_ret_socketpair(struct user_regs_struct *, pid_t);

void		print_args_setsockopt(struct user_regs_struct *, pid_t);
void		print_ret_setsockopt(struct user_regs_struct *, pid_t);

void		print_args_getsockopt(struct user_regs_struct *, pid_t);
void		print_ret_getsockopt(struct user_regs_struct *, pid_t);

void		print_args_clone(struct user_regs_struct *, pid_t);
void		print_ret_clone(struct user_regs_struct *, pid_t);

void		print_args_fork(struct user_regs_struct *, pid_t);
void		print_ret_fork(struct user_regs_struct *, pid_t);

void		print_args_vfork(struct user_regs_struct *, pid_t);
void		print_ret_vfork(struct user_regs_struct *, pid_t);

void		print_args_execve(struct user_regs_struct *, pid_t);
void		print_ret_execve(struct user_regs_struct *, pid_t);

void		print_args__exit(struct user_regs_struct *, pid_t);
void		print_ret__exit(struct user_regs_struct *, pid_t);

void		print_args_wait4(struct user_regs_struct *, pid_t);
void		print_ret_wait4(struct user_regs_struct *, pid_t);

void		print_args_kill(struct user_regs_struct *, pid_t);
void		print_ret_kill(struct user_regs_struct *, pid_t);

void		print_args_uname(struct user_regs_struct *, pid_t);
void		print_ret_uname(struct user_regs_struct *, pid_t);

void		print_args_semget(struct user_regs_struct *, pid_t);
void		print_ret_semget(struct user_regs_struct *, pid_t);

void		print_args_semop(struct user_regs_struct *, pid_t);
void		print_ret_semop(struct user_regs_struct *, pid_t);

void		print_args_semctl(struct user_regs_struct *, pid_t);
void		print_ret_semctl(struct user_regs_struct *, pid_t);

void		print_args_shmdt(struct user_regs_struct *, pid_t);
void		print_ret_shmdt(struct user_regs_struct *, pid_t);

void		print_args_msgget(struct user_regs_struct *, pid_t);
void		print_ret_msgget(struct user_regs_struct *, pid_t);

void		print_args_msgsnd(struct user_regs_struct *, pid_t);
void		print_ret_msgsnd(struct user_regs_struct *, pid_t);

void		print_args_msgrcv(struct user_regs_struct *, pid_t);
void		print_ret_msgrcv(struct user_regs_struct *, pid_t);

void		print_args_msgctl(struct user_regs_struct *, pid_t);
void		print_ret_msgctl(struct user_regs_struct *, pid_t);

void		print_args_fcntl(struct user_regs_struct *, pid_t);
void		print_ret_fcntl(struct user_regs_struct *, pid_t);

void		print_args_flock(struct user_regs_struct *, pid_t);
void		print_ret_flock(struct user_regs_struct *, pid_t);

void		print_args_fsync(struct user_regs_struct *, pid_t);
void		print_ret_fsync(struct user_regs_struct *, pid_t);

void		print_args_fdatasync(struct user_regs_struct *, pid_t);
void		print_ret_fdatasync(struct user_regs_struct *, pid_t);

void		print_args_truncate(struct user_regs_struct *, pid_t);
void		print_ret_truncate(struct user_regs_struct *, pid_t);

void		print_args_ftruncate(struct user_regs_struct *, pid_t);
void		print_ret_ftruncate(struct user_regs_struct *, pid_t);

void		print_args_getdents(struct user_regs_struct *, pid_t);
void		print_ret_getdents(struct user_regs_struct *, pid_t);

void		print_args_getcwd(struct user_regs_struct *, pid_t);
void		print_ret_getcwd(struct user_regs_struct *, pid_t);

void		print_args_chdir(struct user_regs_struct *, pid_t);
void		print_ret_chdir(struct user_regs_struct *, pid_t);

void		print_args_fchdir(struct user_regs_struct *, pid_t);
void		print_ret_fchdir(struct user_regs_struct *, pid_t);

void		print_args_rename(struct user_regs_struct *, pid_t);
void		print_ret_rename(struct user_regs_struct *, pid_t);

void		print_args_mkdir(struct user_regs_struct *, pid_t);
void		print_ret_mkdir(struct user_regs_struct *, pid_t);

void		print_args_rmdir(struct user_regs_struct *, pid_t);
void		print_ret_rmdir(struct user_regs_struct *, pid_t);

void		print_args_creat(struct user_regs_struct *, pid_t);
void		print_ret_creat(struct user_regs_struct *, pid_t);

void		print_args_link(struct user_regs_struct *, pid_t);
void		print_ret_link(struct user_regs_struct *, pid_t);

void		print_args_unlink(struct user_regs_struct *, pid_t);
void		print_ret_unlink(struct user_regs_struct *, pid_t);

void		print_args_symlink(struct user_regs_struct *, pid_t);
void		print_ret_symlink(struct user_regs_struct *, pid_t);

void		print_args_readlink(struct user_regs_struct *, pid_t);
void		print_ret_readlink(struct user_regs_struct *, pid_t);

void		print_args_chmod(struct user_regs_struct *, pid_t);
void		print_ret_chmod(struct user_regs_struct *, pid_t);

void		print_args_fchmod(struct user_regs_struct *, pid_t);
void		print_ret_fchmod(struct user_regs_struct *, pid_t);

void		print_args_chown(struct user_regs_struct *, pid_t);
void		print_ret_chown(struct user_regs_struct *, pid_t);

void		print_args_fchown(struct user_regs_struct *, pid_t);
void		print_ret_fchown(struct user_regs_struct *, pid_t);

void		print_args_lchown(struct user_regs_struct *, pid_t);
void		print_ret_lchown(struct user_regs_struct *, pid_t);

void		print_args_umask(struct user_regs_struct *, pid_t);
void		print_ret_umask(struct user_regs_struct *, pid_t);

void		print_args_gettimeofday(struct user_regs_struct *, pid_t);
void		print_ret_gettimeofday(struct user_regs_struct *, pid_t);

void		print_args_getrlimit(struct user_regs_struct *, pid_t);
void		print_ret_getrlimit(struct user_regs_struct *, pid_t);

void		print_args_getrusage(struct user_regs_struct *, pid_t);
void		print_ret_getrusage(struct user_regs_struct *, pid_t);

void		print_args_sysinfo(struct user_regs_struct *, pid_t);
void		print_ret_sysinfo(struct user_regs_struct *, pid_t);

void		print_args_times(struct user_regs_struct *, pid_t);
void		print_ret_times(struct user_regs_struct *, pid_t);

void		print_args_ptrace(struct user_regs_struct *, pid_t);
void		print_ret_ptrace(struct user_regs_struct *, pid_t);

void		print_args_getuid(struct user_regs_struct *, pid_t);
void		print_ret_getuid(struct user_regs_struct *, pid_t);

void		print_args_syslog(struct user_regs_struct *, pid_t);
void		print_ret_syslog(struct user_regs_struct *, pid_t);

void		print_args_getgid(struct user_regs_struct *, pid_t);
void		print_ret_getgid(struct user_regs_struct *, pid_t);

void		print_args_setuid(struct user_regs_struct *, pid_t);
void		print_ret_setuid(struct user_regs_struct *, pid_t);

void		print_args_setgid(struct user_regs_struct *, pid_t);
void		print_ret_setgid(struct user_regs_struct *, pid_t);

void		print_args_geteuid(struct user_regs_struct *, pid_t);
void		print_ret_geteuid(struct user_regs_struct *, pid_t);

void		print_args_getegid(struct user_regs_struct *, pid_t);
void		print_ret_getegid(struct user_regs_struct *, pid_t);

void		print_args_setpgid(struct user_regs_struct *, pid_t);
void		print_ret_setpgid(struct user_regs_struct *, pid_t);

void		print_args_getppid(struct user_regs_struct *, pid_t);
void		print_ret_getppid(struct user_regs_struct *, pid_t);

void		print_args_getpgrp(struct user_regs_struct *, pid_t);
void		print_ret_getpgrp(struct user_regs_struct *, pid_t);

void		print_args_setsid(struct user_regs_struct *, pid_t);
void		print_ret_setsid(struct user_regs_struct *, pid_t);

void		print_args_setreuid(struct user_regs_struct *, pid_t);
void		print_ret_setreuid(struct user_regs_struct *, pid_t);

void		print_args_setregid(struct user_regs_struct *, pid_t);
void		print_ret_setregid(struct user_regs_struct *, pid_t);

void		print_args_getgroups(struct user_regs_struct *, pid_t);
void		print_ret_getgroups(struct user_regs_struct *, pid_t);

void		print_args_setgroups(struct user_regs_struct *, pid_t);
void		print_ret_setgroups(struct user_regs_struct *, pid_t);

void		print_args_setresuid(struct user_regs_struct *, pid_t);
void		print_ret_setresuid(struct user_regs_struct *, pid_t);

void		print_args_getresuid(struct user_regs_struct *, pid_t);
void		print_ret_getresuid(struct user_regs_struct *, pid_t);

void		print_args_setresgid(struct user_regs_struct *, pid_t);
void		print_ret_setresgid(struct user_regs_struct *, pid_t);

void		print_args_getresgid(struct user_regs_struct *, pid_t);
void		print_ret_getresgid(struct user_regs_struct *, pid_t);

void		print_args_getpgid(struct user_regs_struct *, pid_t);
void		print_ret_getpgid(struct user_regs_struct *, pid_t);

void		print_args_setfsuid(struct user_regs_struct *, pid_t);
void		print_ret_setfsuid(struct user_regs_struct *, pid_t);

void		print_args_setfsgid(struct user_regs_struct *, pid_t);
void		print_ret_setfsgid(struct user_regs_struct *, pid_t);

void		print_args_getsid(struct user_regs_struct *, pid_t);
void		print_ret_getsid(struct user_regs_struct *, pid_t);

void		print_args_capget(struct user_regs_struct *, pid_t);
void		print_ret_capget(struct user_regs_struct *, pid_t);

void		print_args_capset(struct user_regs_struct *, pid_t);
void		print_ret_capset(struct user_regs_struct *, pid_t);

void		print_args_rt_sigpending(struct user_regs_struct *, pid_t);
void		print_ret_rt_sigpending(struct user_regs_struct *, pid_t);

void		print_args_rt_sigtimedwait(struct user_regs_struct *, pid_t);
void		print_ret_rt_sigtimedwait(struct user_regs_struct *, pid_t);

void		print_args_rt_sigqueueinfo(struct user_regs_struct *, pid_t);
void		print_ret_rt_sigqueueinfo(struct user_regs_struct *, pid_t);

void		print_args_rt_sigsuspend(struct user_regs_struct *, pid_t);
void		print_ret_rt_sigsuspend(struct user_regs_struct *, pid_t);

void		print_args_sigaltstack(struct user_regs_struct *, pid_t);
void		print_ret_sigaltstack(struct user_regs_struct *, pid_t);

void		print_args_utime(struct user_regs_struct *, pid_t);
void		print_ret_utime(struct user_regs_struct *, pid_t);

void		print_args_mknod(struct user_regs_struct *, pid_t);
void		print_ret_mknod(struct user_regs_struct *, pid_t);

void		print_args_uselib(struct user_regs_struct *, pid_t);
void		print_ret_uselib(struct user_regs_struct *, pid_t);

void		print_args_personality(struct user_regs_struct *, pid_t);
void		print_ret_personality(struct user_regs_struct *, pid_t);

void		print_args_ustat(struct user_regs_struct *, pid_t);
void		print_ret_ustat(struct user_regs_struct *, pid_t);

void		print_args_statfs(struct user_regs_struct *, pid_t);
void		print_ret_statfs(struct user_regs_struct *, pid_t);

void		print_args_fstatfs(struct user_regs_struct *, pid_t);
void		print_ret_fstatfs(struct user_regs_struct *, pid_t);

void		print_args_sysfs(struct user_regs_struct *, pid_t);
void		print_ret_sysfs(struct user_regs_struct *, pid_t);

void		print_args_getpriority(struct user_regs_struct *, pid_t);
void		print_ret_getpriority(struct user_regs_struct *, pid_t);

void		print_args_setpriority(struct user_regs_struct *, pid_t);
void		print_ret_setpriority(struct user_regs_struct *, pid_t);

void		print_args_sched_setparam(struct user_regs_struct *, pid_t);
void		print_ret_sched_setparam(struct user_regs_struct *, pid_t);

void		print_args_sched_getparam(struct user_regs_struct *, pid_t);
void		print_ret_sched_getparam(struct user_regs_struct *, pid_t);

void		print_args_sched_setscheduler(struct user_regs_struct *, pid_t);
void		print_ret_sched_setscheduler(struct user_regs_struct *, pid_t);

void		print_args_sched_getscheduler(struct user_regs_struct *, pid_t);
void		print_ret_sched_getscheduler(struct user_regs_struct *, pid_t);

void		print_args_sched_get_priority_max(struct user_regs_struct *, pid_t);
void		print_ret_sched_get_priority_max(struct user_regs_struct *, pid_t);

void		print_args_sched_get_priority_min(struct user_regs_struct *, pid_t);
void		print_ret_sched_get_priority_min(struct user_regs_struct *, pid_t);

void		print_args_sched_rr_get_interval(struct user_regs_struct *, pid_t);
void		print_ret_sched_rr_get_interval(struct user_regs_struct *, pid_t);

void		print_args_mlock(struct user_regs_struct *, pid_t);
void		print_ret_mlock(struct user_regs_struct *, pid_t);

void		print_args_munlock(struct user_regs_struct *, pid_t);
void		print_ret_munlock(struct user_regs_struct *, pid_t);

void		print_args_mlockall(struct user_regs_struct *, pid_t);
void		print_ret_mlockall(struct user_regs_struct *, pid_t);

void		print_args_munlockall(struct user_regs_struct *, pid_t);
void		print_ret_munlockall(struct user_regs_struct *, pid_t);

void		print_args_vhangup(struct user_regs_struct *, pid_t);
void		print_ret_vhangup(struct user_regs_struct *, pid_t);

void		print_args_modify_ldt(struct user_regs_struct *, pid_t);
void		print_ret_modify_ldt(struct user_regs_struct *, pid_t);

void		print_args_pivot_root(struct user_regs_struct *, pid_t);
void		print_ret_pivot_root(struct user_regs_struct *, pid_t);

void		print_args__sysctl(struct user_regs_struct *, pid_t);
void		print_ret__sysctl(struct user_regs_struct *, pid_t);

void		print_args_prctl(struct user_regs_struct *, pid_t);
void		print_ret_prctl(struct user_regs_struct *, pid_t);

void		print_args_arch_prctl(struct user_regs_struct *, pid_t);
void		print_ret_arch_prctl(struct user_regs_struct *, pid_t);

void		print_args_adjtimex(struct user_regs_struct *, pid_t);
void		print_ret_adjtimex(struct user_regs_struct *, pid_t);

void		print_args_setrlimit(struct user_regs_struct *, pid_t);
void		print_ret_setrlimit(struct user_regs_struct *, pid_t);

void		print_args_chroot(struct user_regs_struct *, pid_t);
void		print_ret_chroot(struct user_regs_struct *, pid_t);

void		print_args_sync(struct user_regs_struct *, pid_t);
void		print_ret_sync(struct user_regs_struct *, pid_t);

void		print_args_acct(struct user_regs_struct *, pid_t);
void		print_ret_acct(struct user_regs_struct *, pid_t);

void		print_args_settimeofday(struct user_regs_struct *, pid_t);
void		print_ret_settimeofday(struct user_regs_struct *, pid_t);

void		print_args_mount(struct user_regs_struct *, pid_t);
void		print_ret_mount(struct user_regs_struct *, pid_t);

void		print_args_umount(struct user_regs_struct *, pid_t);
void		print_ret_umount(struct user_regs_struct *, pid_t);

void		print_args_swapon(struct user_regs_struct *, pid_t);
void		print_ret_swapon(struct user_regs_struct *, pid_t);

void		print_args_swapoff(struct user_regs_struct *, pid_t);
void		print_ret_swapoff(struct user_regs_struct *, pid_t);

void		print_args_reboot(struct user_regs_struct *, pid_t);
void		print_ret_reboot(struct user_regs_struct *, pid_t);

void		print_args_sethostname(struct user_regs_struct *, pid_t);
void		print_ret_sethostname(struct user_regs_struct *, pid_t);

void		print_args_setdomainname(struct user_regs_struct *, pid_t);
void		print_ret_setdomainname(struct user_regs_struct *, pid_t);

void		print_args_iopl(struct user_regs_struct *, pid_t);
void		print_ret_iopl(struct user_regs_struct *, pid_t);

void		print_args_ioperm(struct user_regs_struct *, pid_t);
void		print_ret_ioperm(struct user_regs_struct *, pid_t);

void		print_args_create_module(struct user_regs_struct *, pid_t);
void		print_ret_create_module(struct user_regs_struct *, pid_t);

void		print_args_init_module(struct user_regs_struct *, pid_t);
void		print_ret_init_module(struct user_regs_struct *, pid_t);

void		print_args_delete_module(struct user_regs_struct *, pid_t);
void		print_ret_delete_module(struct user_regs_struct *, pid_t);

void		print_args_get_kernel_syms(struct user_regs_struct *, pid_t);
void		print_ret_get_kernel_syms(struct user_regs_struct *, pid_t);

void		print_args_query_module(struct user_regs_struct *, pid_t);
void		print_ret_query_module(struct user_regs_struct *, pid_t);

void		print_args_quotactl(struct user_regs_struct *, pid_t);
void		print_ret_quotactl(struct user_regs_struct *, pid_t);

void		print_args_nfsservctl(struct user_regs_struct *, pid_t);
void		print_ret_nfsservctl(struct user_regs_struct *, pid_t);

void		print_args_getpmsg(struct user_regs_struct *, pid_t);
void		print_ret_getpmsg(struct user_regs_struct *, pid_t);

void		print_args_putpmsg(struct user_regs_struct *, pid_t);
void		print_ret_putpmsg(struct user_regs_struct *, pid_t);

void		print_args_afs_syscall(struct user_regs_struct *, pid_t);
void		print_ret_afs_syscall(struct user_regs_struct *, pid_t);

void		print_args_tuxcall(struct user_regs_struct *, pid_t);
void		print_ret_tuxcall(struct user_regs_struct *, pid_t);

void		print_args_security(struct user_regs_struct *, pid_t);
void		print_ret_security(struct user_regs_struct *, pid_t);

void		print_args_gettid(struct user_regs_struct *, pid_t);
void		print_ret_gettid(struct user_regs_struct *, pid_t);

void		print_args_readahead(struct user_regs_struct *, pid_t);
void		print_ret_readahead(struct user_regs_struct *, pid_t);

void		print_args_setxattr(struct user_regs_struct *, pid_t);
void		print_ret_setxattr(struct user_regs_struct *, pid_t);

void		print_args_lsetxattr(struct user_regs_struct *, pid_t);
void		print_ret_lsetxattr(struct user_regs_struct *, pid_t);

void		print_args_fsetxattr(struct user_regs_struct *, pid_t);
void		print_ret_fsetxattr(struct user_regs_struct *, pid_t);

void		print_args_getxattr(struct user_regs_struct *, pid_t);
void		print_ret_getxattr(struct user_regs_struct *, pid_t);

void		print_args_lgetxattr(struct user_regs_struct *, pid_t);
void		print_ret_lgetxattr(struct user_regs_struct *, pid_t);

void		print_args_fgetxattr(struct user_regs_struct *, pid_t);
void		print_ret_fgetxattr(struct user_regs_struct *, pid_t);

void		print_args_listxattr(struct user_regs_struct *, pid_t);
void		print_ret_listxattr(struct user_regs_struct *, pid_t);

void		print_args_llistxattr(struct user_regs_struct *, pid_t);
void		print_ret_llistxattr(struct user_regs_struct *, pid_t);

void		print_args_flistxattr(struct user_regs_struct *, pid_t);
void		print_ret_flistxattr(struct user_regs_struct *, pid_t);

void		print_args_removexattr(struct user_regs_struct *, pid_t);
void		print_ret_removexattr(struct user_regs_struct *, pid_t);

void		print_args_lremovexattr(struct user_regs_struct *, pid_t);
void		print_ret_lremovexattr(struct user_regs_struct *, pid_t);

void		print_args_fremovexattr(struct user_regs_struct *, pid_t);
void		print_ret_fremovexattr(struct user_regs_struct *, pid_t);

void		print_args_tkill(struct user_regs_struct *, pid_t);
void		print_ret_tkill(struct user_regs_struct *, pid_t);

void		print_args_time(struct user_regs_struct *, pid_t);
void		print_ret_time(struct user_regs_struct *, pid_t);

void		print_args_futex(struct user_regs_struct *, pid_t);
void		print_ret_futex(struct user_regs_struct *, pid_t);

void		print_args_sched_setaffinity(struct user_regs_struct *, pid_t);
void		print_ret_sched_setaffinity(struct user_regs_struct *, pid_t);

void		print_args_sched_getaffinity(struct user_regs_struct *, pid_t);
void		print_ret_sched_getaffinity(struct user_regs_struct *, pid_t);

void		print_args_set_thread_area(struct user_regs_struct *, pid_t);
void		print_ret_set_thread_area(struct user_regs_struct *, pid_t);

void		print_args_io_setup(struct user_regs_struct *, pid_t);
void		print_ret_io_setup(struct user_regs_struct *, pid_t);

void		print_args_io_destroy(struct user_regs_struct *, pid_t);
void		print_ret_io_destroy(struct user_regs_struct *, pid_t);

void		print_args_io_getevents(struct user_regs_struct *, pid_t);
void		print_ret_io_getevents(struct user_regs_struct *, pid_t);

void		print_args_io_submit(struct user_regs_struct *, pid_t);
void		print_ret_io_submit(struct user_regs_struct *, pid_t);

void		print_args_io_cancel(struct user_regs_struct *, pid_t);
void		print_ret_io_cancel(struct user_regs_struct *, pid_t);

void		print_args_get_thread_area(struct user_regs_struct *, pid_t);
void		print_ret_get_thread_area(struct user_regs_struct *, pid_t);

void		print_args_lookup_dcookie(struct user_regs_struct *, pid_t);
void		print_ret_lookup_dcookie(struct user_regs_struct *, pid_t);

void		print_args_epoll_create(struct user_regs_struct *, pid_t);
void		print_ret_epoll_create(struct user_regs_struct *, pid_t);

void		print_args_epoll_ctl_old(struct user_regs_struct *, pid_t);
void		print_ret_epoll_ctl_old(struct user_regs_struct *, pid_t);

void		print_args_epoll_wait_old(struct user_regs_struct *, pid_t);
void		print_ret_epoll_wait_old(struct user_regs_struct *, pid_t);

void		print_args_remap_file_pages(struct user_regs_struct *, pid_t);
void		print_ret_remap_file_pages(struct user_regs_struct *, pid_t);

void		print_args_getdents64(struct user_regs_struct *, pid_t);
void		print_ret_getdents64(struct user_regs_struct *, pid_t);

void		print_args_set_tid_address(struct user_regs_struct *, pid_t);
void		print_ret_set_tid_address(struct user_regs_struct *, pid_t);

void		print_args_restart_syscall(struct user_regs_struct *, pid_t);
void		print_ret_restart_syscall(struct user_regs_struct *, pid_t);

void		print_args_semtimedop(struct user_regs_struct *, pid_t);
void		print_ret_semtimedop(struct user_regs_struct *, pid_t);

void		print_args_fadvise64(struct user_regs_struct *, pid_t);
void		print_ret_fadvise64(struct user_regs_struct *, pid_t);

void		print_args_timer_create(struct user_regs_struct *, pid_t);
void		print_ret_timer_create(struct user_regs_struct *, pid_t);

void		print_args_timer_settime(struct user_regs_struct *, pid_t);
void		print_ret_timer_settime(struct user_regs_struct *, pid_t);

void		print_args_timer_gettime(struct user_regs_struct *, pid_t);
void		print_ret_timer_gettime(struct user_regs_struct *, pid_t);

void		print_args_timer_getoverrun(struct user_regs_struct *, pid_t);
void		print_ret_timer_getoverrun(struct user_regs_struct *, pid_t);

void		print_args_timer_delete(struct user_regs_struct *, pid_t);
void		print_ret_timer_delete(struct user_regs_struct *, pid_t);

void		print_args_clock_settime(struct user_regs_struct *, pid_t);
void		print_ret_clock_settime(struct user_regs_struct *, pid_t);

void		print_args_clock_gettime(struct user_regs_struct *, pid_t);
void		print_ret_clock_gettime(struct user_regs_struct *, pid_t);

void		print_args_clock_getres(struct user_regs_struct *, pid_t);
void		print_ret_clock_getres(struct user_regs_struct *, pid_t);

void		print_args_clock_nanosleep(struct user_regs_struct *, pid_t);
void		print_ret_clock_nanosleep(struct user_regs_struct *, pid_t);

void		print_args_exit_group(struct user_regs_struct *, pid_t);
void		print_ret_exit_group(struct user_regs_struct *, pid_t);

void		print_args_epoll_wait(struct user_regs_struct *, pid_t);
void		print_ret_epoll_wait(struct user_regs_struct *, pid_t);

void		print_args_epoll_ctl(struct user_regs_struct *, pid_t);
void		print_ret_epoll_ctl(struct user_regs_struct *, pid_t);

void		print_args_tgkill(struct user_regs_struct *, pid_t);
void		print_ret_tgkill(struct user_regs_struct *, pid_t);

void		print_args_utimes(struct user_regs_struct *, pid_t);
void		print_ret_utimes(struct user_regs_struct *, pid_t);

void		print_args_vserver(struct user_regs_struct *, pid_t);
void		print_ret_vserver(struct user_regs_struct *, pid_t);

void		print_args_mbind(struct user_regs_struct *, pid_t);
void		print_ret_mbind(struct user_regs_struct *, pid_t);

void		print_args_set_mempolicy(struct user_regs_struct *, pid_t);
void		print_ret_set_mempolicy(struct user_regs_struct *, pid_t);

void		print_args_get_mempolicy(struct user_regs_struct *, pid_t);
void		print_ret_get_mempolicy(struct user_regs_struct *, pid_t);

void		print_args_mq_open(struct user_regs_struct *, pid_t);
void		print_ret_mq_open(struct user_regs_struct *, pid_t);

void		print_args_mq_unlink(struct user_regs_struct *, pid_t);
void		print_ret_mq_unlink(struct user_regs_struct *, pid_t);

void		print_args_mq_timedsend(struct user_regs_struct *, pid_t);
void		print_ret_mq_timedsend(struct user_regs_struct *, pid_t);

void		print_args_mq_timedreceive(struct user_regs_struct *, pid_t);
void		print_ret_mq_timedreceive(struct user_regs_struct *, pid_t);

void		print_args_mq_notify(struct user_regs_struct *, pid_t);
void		print_ret_mq_notify(struct user_regs_struct *, pid_t);

void		print_args_mq_getsetattr(struct user_regs_struct *, pid_t);
void		print_ret_mq_getsetattr(struct user_regs_struct *, pid_t);

void		print_args_kexec_load(struct user_regs_struct *, pid_t);
void		print_ret_kexec_load(struct user_regs_struct *, pid_t);

void		print_args_waitid(struct user_regs_struct *, pid_t);
void		print_ret_waitid(struct user_regs_struct *, pid_t);

void		print_args_add_key(struct user_regs_struct *, pid_t);
void		print_ret_add_key(struct user_regs_struct *, pid_t);

void		print_args_request_key(struct user_regs_struct *, pid_t);
void		print_ret_request_key(struct user_regs_struct *, pid_t);

void		print_args_keyctl(struct user_regs_struct *, pid_t);
void		print_ret_keyctl(struct user_regs_struct *, pid_t);

void		print_args_ioprio_set(struct user_regs_struct *, pid_t);
void		print_ret_ioprio_set(struct user_regs_struct *, pid_t);

void		print_args_ioprio_get(struct user_regs_struct *, pid_t);
void		print_ret_ioprio_get(struct user_regs_struct *, pid_t);

void		print_args_inotify_init(struct user_regs_struct *, pid_t);
void		print_ret_inotify_init(struct user_regs_struct *, pid_t);

void		print_args_inotify_add_watch(struct user_regs_struct *, pid_t);
void		print_ret_inotify_add_watch(struct user_regs_struct *, pid_t);

void		print_args_inotify_rm_watch(struct user_regs_struct *, pid_t);
void		print_ret_inotify_rm_watch(struct user_regs_struct *, pid_t);

void		print_args_migrate_pages(struct user_regs_struct *, pid_t);
void		print_ret_migrate_pages(struct user_regs_struct *, pid_t);

void		print_args_openat(struct user_regs_struct *, pid_t);
void		print_ret_openat(struct user_regs_struct *, pid_t);

void		print_args_mkdirat(struct user_regs_struct *, pid_t);
void		print_ret_mkdirat(struct user_regs_struct *, pid_t);

void		print_args_mknodat(struct user_regs_struct *, pid_t);
void		print_ret_mknodat(struct user_regs_struct *, pid_t);

void		print_args_fchownat(struct user_regs_struct *, pid_t);
void		print_ret_fchownat(struct user_regs_struct *, pid_t);

void		print_args_futimesat(struct user_regs_struct *, pid_t);
void		print_ret_futimesat(struct user_regs_struct *, pid_t);

void		print_args_newfstatat(struct user_regs_struct *, pid_t);
void		print_ret_newfstatat(struct user_regs_struct *, pid_t);

void		print_args_unlinkat(struct user_regs_struct *, pid_t);
void		print_ret_unlinkat(struct user_regs_struct *, pid_t);

void		print_args_renameat(struct user_regs_struct *, pid_t);
void		print_ret_renameat(struct user_regs_struct *, pid_t);

void		print_args_linkat(struct user_regs_struct *, pid_t);
void		print_ret_linkat(struct user_regs_struct *, pid_t);

void		print_args_symlinkat(struct user_regs_struct *, pid_t);
void		print_ret_symlinkat(struct user_regs_struct *, pid_t);

void		print_args_readlinkat(struct user_regs_struct *, pid_t);
void		print_ret_readlinkat(struct user_regs_struct *, pid_t);

void		print_args_fchmodat(struct user_regs_struct *, pid_t);
void		print_ret_fchmodat(struct user_regs_struct *, pid_t);

void		print_args_faccessat(struct user_regs_struct *, pid_t);
void		print_ret_faccessat(struct user_regs_struct *, pid_t);

void		print_args_pselect6(struct user_regs_struct *, pid_t);
void		print_ret_pselect6(struct user_regs_struct *, pid_t);

void		print_args_ppoll(struct user_regs_struct *, pid_t);
void		print_ret_ppoll(struct user_regs_struct *, pid_t);

void		print_args_unshare(struct user_regs_struct *, pid_t);
void		print_ret_unshare(struct user_regs_struct *, pid_t);

void		print_args_set_robust_list(struct user_regs_struct *, pid_t);
void		print_ret_set_robust_list(struct user_regs_struct *, pid_t);

void		print_args_get_robust_list(struct user_regs_struct *, pid_t);
void		print_ret_get_robust_list(struct user_regs_struct *, pid_t);

void		print_args_splice(struct user_regs_struct *, pid_t);
void		print_ret_splice(struct user_regs_struct *, pid_t);

void		print_args_tee(struct user_regs_struct *, pid_t);
void		print_ret_tee(struct user_regs_struct *, pid_t);

void		print_args_sync_file_range(struct user_regs_struct *, pid_t);
void		print_ret_sync_file_range(struct user_regs_struct *, pid_t);

void		print_args_vmsplice(struct user_regs_struct *, pid_t);
void		print_ret_vmsplice(struct user_regs_struct *, pid_t);

void		print_args_move_pages(struct user_regs_struct *, pid_t);
void		print_ret_move_pages(struct user_regs_struct *, pid_t);

void		print_args_utimensat(struct user_regs_struct *, pid_t);
void		print_ret_utimensat(struct user_regs_struct *, pid_t);

void		print_args_epoll_pwait(struct user_regs_struct *, pid_t);
void		print_ret_epoll_pwait(struct user_regs_struct *, pid_t);

void		print_args_signalfd(struct user_regs_struct *, pid_t);
void		print_ret_signalfd(struct user_regs_struct *, pid_t);

void		print_args_timerfd_create(struct user_regs_struct *, pid_t);
void		print_ret_timerfd_create(struct user_regs_struct *, pid_t);

void		print_args_eventfd(struct user_regs_struct *, pid_t);
void		print_ret_eventfd(struct user_regs_struct *, pid_t);

void		print_args_fallocate(struct user_regs_struct *, pid_t);
void		print_ret_fallocate(struct user_regs_struct *, pid_t);

void		print_args_timerfd_settime(struct user_regs_struct *, pid_t);
void		print_ret_timerfd_settime(struct user_regs_struct *, pid_t);

void		print_args_timerfd_gettime(struct user_regs_struct *, pid_t);
void		print_ret_timerfd_gettime(struct user_regs_struct *, pid_t);

void		print_args_accept4(struct user_regs_struct *, pid_t);
void		print_ret_accept4(struct user_regs_struct *, pid_t);

void		print_args_signalfd4(struct user_regs_struct *, pid_t);
void		print_ret_signalfd4(struct user_regs_struct *, pid_t);

void		print_args_eventfd2(struct user_regs_struct *, pid_t);
void		print_ret_eventfd2(struct user_regs_struct *, pid_t);

void		print_args_epoll_create1(struct user_regs_struct *, pid_t);
void		print_ret_epoll_create1(struct user_regs_struct *, pid_t);

void		print_args_dup3(struct user_regs_struct *, pid_t);
void		print_ret_dup3(struct user_regs_struct *, pid_t);

void		print_args_pipe2(struct user_regs_struct *, pid_t);
void		print_ret_pipe2(struct user_regs_struct *, pid_t);

void		print_args_inotify_init1(struct user_regs_struct *, pid_t);
void		print_ret_inotify_init1(struct user_regs_struct *, pid_t);

void		print_args_preadv(struct user_regs_struct *, pid_t);
void		print_ret_preadv(struct user_regs_struct *, pid_t);

void		print_args_pwritev(struct user_regs_struct *, pid_t);
void		print_ret_pwritev(struct user_regs_struct *, pid_t);

void		print_args_rt_tgsigqueueinfo(struct user_regs_struct *, pid_t);
void		print_ret_rt_tgsigqueueinfo(struct user_regs_struct *, pid_t);

void		print_args_perf_event_open(struct user_regs_struct *, pid_t);
void		print_ret_perf_event_open(struct user_regs_struct *, pid_t);

void		print_args_recvmmsg(struct user_regs_struct *, pid_t);
void		print_ret_recvmmsg(struct user_regs_struct *, pid_t);

void		print_args_fanotify_init(struct user_regs_struct *, pid_t);
void		print_ret_fanotify_init(struct user_regs_struct *, pid_t);

void		print_args_fanotify_mark(struct user_regs_struct *, pid_t);
void		print_ret_fanotify_mark(struct user_regs_struct *, pid_t);

void		print_args_prlimit64(struct user_regs_struct *, pid_t);
void		print_ret_prlimit64(struct user_regs_struct *, pid_t);

void		print_args_name_to_handle_at(struct user_regs_struct *, pid_t);
void		print_ret_name_to_handle_at(struct user_regs_struct *, pid_t);

void		print_args_open_by_handle_at(struct user_regs_struct *, pid_t);
void		print_ret_open_by_handle_at(struct user_regs_struct *, pid_t);

void		print_args_clock_adjtime(struct user_regs_struct *, pid_t);
void		print_ret_clock_adjtime(struct user_regs_struct *, pid_t);

void		print_args_syncfs(struct user_regs_struct *, pid_t);
void		print_ret_syncfs(struct user_regs_struct *, pid_t);

void		print_args_sendmmsg(struct user_regs_struct *, pid_t);
void		print_ret_sendmmsg(struct user_regs_struct *, pid_t);

void		print_args_setns(struct user_regs_struct *, pid_t);
void		print_ret_setns(struct user_regs_struct *, pid_t);

void		print_args_getcpu(struct user_regs_struct *, pid_t);
void		print_ret_getcpu(struct user_regs_struct *, pid_t);

void		print_args_process_vm_readv(struct user_regs_struct *, pid_t);
void		print_ret_process_vm_readv(struct user_regs_struct *, pid_t);

void		print_args_process_vm_writev(struct user_regs_struct *, pid_t);
void		print_ret_process_vm_writev(struct user_regs_struct *, pid_t);

#endif		/* AFF_H__ */
