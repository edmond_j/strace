/*
** printer.c for strace in /home/edmond_j//rendu/B4/Advanced Unix System/strace/rendu
** 
** Made by julien edmond
** Login   <edmond_j@epitech.net>
** 
** Started on  Wed May  8 09:39:58 2013 julien edmond
** Last update Mon May 13 11:05:29 2013 julien edmond
*/

#include	<sys/user.h>
#include	<stdio.h>
#include	"strace.h"
#include	"aff.h"

static t_syscall	syscalls[] =
  {
#include "syscall_struct.txt"
  };

int	print_syscall(struct user_regs_struct *regs, int pid)
{
  syscalls[regs->rax].print_arg(regs, pid);
  return (0);
}

int	print_ret(struct user_regs_struct *regs, int pid)
{
  syscalls[regs->orig_rax].print_ret(regs, pid);
  return (0);
}

