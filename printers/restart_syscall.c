/*
** header.c for aff in /home/lequeu_m/
**
** Made by martin lequeux-gruninger
** Login   <lequeu_m@epitech.net>
**
** Started on  Fri May 10 17:38:41 2013 martin lequeux-gruninger
** Last update Wed May 15 11:47:07 2013 julien edmond
*/

/*
**       #include <linux/unistd.h>
**
**       long set_tid_address(int *tidptr);
**
*/

#include <linux/unistd.h>
#include <unistd.h>
#include "aff.h"
#include "strace.h"

void		print_args_restart_syscall(struct user_regs_struct *regs,
					   pid_t pid)
{
  (void)pid;
  fprintf(stderr, "restart_syscall(");
  fprintf(stderr, "%p", (int *)regs->rdi);
  fprintf(stderr, ")\n");
}

void		print_ret_restart_syscall(struct user_regs_struct *regs,
					  pid_t pid)
{
  (void)pid;
  fprintf(stderr, "|Ret----restart_syscall--->(");
  if ((long)regs->rax < 0)
    print_errno((int)regs->rax);
  else
    fprintf(stderr, "%ld", (long)regs->rax);
  fprintf(stderr, ")\n");
}
