/*
** header.c for aff in /home/lequeu_m/
**
** Made by martin lequeux-gruninger
** Login   <lequeu_m@epitech.net>
**
** Started on  Fri May 10 17:38:41 2013 martin lequeux-gruninger
** Last update Mon May 13 18:37:52 2013 julien edmond
*/

/*
**       #include <sys/types.h>
**       #include <unistd.h>     libc[45]
**       #include <ustat.h>      glibc2
**
**       int ustat(dev_t dev, struct ustat *ubuf);
**
*/

#include <sys/types.h>
#include <unistd.h>
#include <ustat.h>
#include <unistd.h>
#include "aff.h"
#include "strace.h"

void		print_args_ustat(struct user_regs_struct *regs, pid_t pid)
{
  (void)pid;
  fprintf(stderr, "ustat(");
  fprintf(stderr, "%lu, %p", (dev_t)regs->rdi, (void *)regs->rsi);
  fprintf(stderr, ")\n");
}

void		print_ret_ustat(struct user_regs_struct *regs, pid_t pid)
{
  (void)pid;
  fprintf(stderr, "|Ret----ustat--->(");
  print_errno((int)regs->rax);
  fprintf(stderr, ")\n");
}
