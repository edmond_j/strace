/*
** header.c for aff in /home/lequeu_m/
**
** Made by martin lequeux-gruninger
** Login   <lequeu_m@epitech.net>
**
** Started on  Fri May 10 17:38:41 2013 martin lequeux-gruninger
** Last update Wed May 15 17:31:02 2013 julien edmond
*/

/*
**       #include <sys/mman.h>
**
**       void *mmap(void *addr, size_t length, int prot, int flags,
**                  int fd, off_t offset);
**       int munmap(void *addr, size_t length);
**
*/

#include <sys/mman.h>
#include <unistd.h>
#include "aff.h"
#include "strace.h"

void		print_args_mmap(struct user_regs_struct *regs, pid_t pid)
{
  (void)pid;
  fprintf(stderr, "mmap(");
  fprintf(stderr, "%p, %lu, %d, %d, %d, 0x%lx", (void *)regs->rdi,
	  (size_t)regs->rsi, (int)regs->rdx, (int)regs->rcx,
	  (int)regs->r8, (off_t)regs->r9);
  fprintf(stderr, ")\n");
}

void		print_ret_mmap(struct user_regs_struct *regs, pid_t pid)
{
  (void)pid;
  fprintf(stderr, "|Ret----mmap--->(");
  if ((long)regs->rax < 0)
    print_errno((int)regs->rax);
  else
    fprintf(stderr, "%p", (void *)regs->rax);
  fprintf(stderr, ")\n");
}
