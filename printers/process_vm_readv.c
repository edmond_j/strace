/*
** header.c for aff in /home/lequeu_m/
**
** Made by martin lequeux-gruninger
** Login   <lequeu_m@epitech.net>
**
** Started on  Fri May 10 17:38:41 2013 martin lequeux-gruninger
** Last update Wed May 15 13:36:48 2013 julien edmond
*/

#include <unistd.h>
#include "aff.h"
#include "strace.h"

void		print_args_process_vm_readv(struct user_regs_struct *regs,
					    pid_t pid)
{
  (void)pid;
  fprintf(stderr, "process_vm_readv(");
  fprintf(stderr, "%d, %p, %lu, %p, %lu, %lu", (int)regs->rdi,
	  (void *)regs->rsi, (unsigned long)regs->rdx, (void *)regs->rcx,
	  (unsigned long)regs->r8, (unsigned long)regs->r9);
  fprintf(stderr, ")\n");
}

void		print_ret_process_vm_readv(struct user_regs_struct *regs,
					   pid_t pid)
{
  (void)pid;
  fprintf(stderr, "|Ret----process_vm_readv--->(");
  if ((ssize_t)regs->rax < 0)
    print_errno((int)regs->rax);
  else
    fprintf(stderr, "%ld", regs->rax);
  fprintf(stderr, ")\n");
}
