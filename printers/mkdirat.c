/*
** header.c for aff in /home/lequeu_m/
**
** Made by martin lequeux-gruninger
** Login   <lequeu_m@epitech.net>
**
** Started on  Fri May 10 17:38:41 2013 martin lequeux-gruninger
** Last update Wed May 15 17:37:35 2013 julien edmond
*/

/*
**       #include <fcntl.h>  Definition of AT_* constants
**       #include <sys/stat.h>
**
**       int mkdirat(int dirfd, const char *pathname, mode_t mode);
**
**   Feature Test Macro Requirements for glibc (see feature_test_macros(7)):
**
**       mkdirat():
**           Since glibc 2.10:
**               _XOPEN_SOURCE >= 700 || _POSIX_C_SOURCE >= 200809L
**           Before glibc 2.10:
**               _ATFILE_SOURCE
**
*/

#include <fcntl.h>
#include <sys/stat.h>
#include <unistd.h>
#include "aff.h"
#include "strace.h"

void		print_args_mkdirat(struct user_regs_struct *regs, pid_t pid)
{
  fprintf(stderr, "mkdirat(");
  fprintf(stderr, "%d, ", (int)regs->rdi);
  print_string(pid, (void *)regs->rsi);
  fprintf(stderr, ", %04o", (mode_t)regs->rdx);
  fprintf(stderr, ")\n");
}

void		print_ret_mkdirat(struct user_regs_struct *regs, pid_t pid)
{
  (void)pid;
  fprintf(stderr, "|Ret----mkdirat--->(");
  print_errno((int)regs->rax);
  fprintf(stderr, ")\n");
}
