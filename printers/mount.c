/*
** header.c for aff in /home/lequeu_m/
**
** Made by martin lequeux-gruninger
** Login   <lequeu_m@epitech.net>
**
** Started on  Fri May 10 17:38:41 2013 martin lequeux-gruninger
** Last update Wed May 15 17:28:05 2013 julien edmond
*/

/*
**       #include <sys/mount.h>
**
**       int mount(const char *source, const char *target,
**                 const char *filesystemtype, unsigned long mountflags,
**                 const void *data);
**
*/

#include <sys/mount.h>
#include <unistd.h>
#include "aff.h"
#include "strace.h"

void		print_args_mount(struct user_regs_struct *regs, pid_t pid)
{
  fprintf(stderr, "mount(");
  print_string(pid, (void *)regs->rdi);
  fputs(", ", stderr);
  print_string(pid, (void *)regs->rsi);
  fputs(", ", stderr);
  print_string(pid, (void *)regs->rdx);
  fprintf(stderr, ", %lu, %p", (unsigned long)regs->rcx, (void *)regs->r8);
  fprintf(stderr, ")\n");
}

void		print_ret_mount(struct user_regs_struct *regs, pid_t pid)
{
  (void)pid;
  fprintf(stderr, "|Ret----mount--->(");
  print_errno((int)regs->rax);
  fprintf(stderr, ")\n");
}
