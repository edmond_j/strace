/*
** header.c for aff in /home/lequeu_m/
**
** Made by martin lequeux-gruninger
** Login   <lequeu_m@epitech.net>
**
** Started on  Fri May 10 17:38:41 2013 martin lequeux-gruninger
** Last update Wed May 15 12:37:01 2013 martin lequeux-gruninger
*/

/*
**       #include <unistd.h>
**
**       pid_t fork(void);
**
*/

#include <unistd.h>
#include <unistd.h>
#include "aff.h"
#include "strace.h"

void		print_args_fork(struct user_regs_struct *regs, pid_t pid)
{
  (void)pid;
  (void)regs;
  fprintf(stderr, "fork(");
  fprintf(stderr, ")\n");
}

void		print_ret_fork(struct user_regs_struct *regs, pid_t pid)
{
  (void)pid;
  fprintf(stderr, "|Ret----fork--->(");
  print_errno((int)regs->rax);
  fprintf(stderr, ")\n");
}
