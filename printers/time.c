/*
** header.c for aff in /home/lequeu_m/
**
** Made by martin lequeux-gruninger
** Login   <lequeu_m@epitech.net>
**
** Started on  Fri May 10 17:38:41 2013 martin lequeux-gruninger
** Last update Tue May 14 11:08:31 2013 julien edmond
*/

/*
**       #include <time.h>
**
**       time_t time(time_t *t);
**
*/

#include <time.h>
#include <unistd.h>
#include "aff.h"
#include "strace.h"

void		print_args_time(struct user_regs_struct *regs, pid_t pid)
{
  (void)pid;
  fprintf(stderr, "time(");
  fprintf(stderr, "%p", (time_t *)regs->rdi);
  fprintf(stderr, ")\n");
}

void		print_ret_time(struct user_regs_struct *regs, pid_t pid)
{
  (void)pid;
  fprintf(stderr, "|Ret----time--->(");
  if ((time_t)regs->rax < (time_t)0)
    print_errno((int)regs->rax);
  else
    fprintf(stderr, "%lu", (time_t)regs->rax);
  fprintf(stderr, ")\n");
}
