/*
** header.c for aff in /home/lequeu_m/
**
** Made by martin lequeux-gruninger
** Login   <lequeu_m@epitech.net>
**
** Started on  Fri May 10 17:38:41 2013 martin lequeux-gruninger
** Last update Tue May 14 15:29:07 2013 julien edmond
*/

/*
**       #include <sched.h>
**
**       int sched_get_priority_max(int policy);
**
**       int sched_get_priority_min(int policy);
**
*/

#include <sched.h>
#include <unistd.h>
#include "aff.h"
#include "strace.h"

void	print_args_sched_get_priority_min(struct user_regs_struct *regs,
					  pid_t pid)
{
  (void)pid;
  fprintf(stderr, "sched_get_priority_min(");
  fprintf(stderr, "%d", (int)regs->rdi);
  fprintf(stderr, ")\n");
}

void	print_ret_sched_get_priority_min(struct user_regs_struct *regs,
					 pid_t pid)
{
  (void)pid;
  fprintf(stderr, "|Ret----sched_get_priority_min--->(");
  print_errno((int)regs->rax);
  fprintf(stderr, ")\n");
}
