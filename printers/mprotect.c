/*
** header.c for aff in /home/lequeu_m/
**
** Made by martin lequeux-gruninger
** Login   <lequeu_m@epitech.net>
**
** Started on  Fri May 10 17:38:41 2013 martin lequeux-gruninger
** Last update Wed May 15 17:24:40 2013 julien edmond
*/

/*
**       #include <sys/mman.h>
**
**       int mprotect(const void *addr, size_t len, int prot);
**
*/

#include <sys/mman.h>
#include <unistd.h>
#include "aff.h"
#include "strace.h"

void		print_args_mprotect(struct user_regs_struct *regs, pid_t pid)
{
  (void)pid;
  fprintf(stderr, "mprotect(");
  fprintf(stderr, "%p, %lu, %d", (void *)regs->rdi, (size_t)regs->rsi,
	  (int)regs->rdx);
  fprintf(stderr, ")\n");
}

void		print_ret_mprotect(struct user_regs_struct *regs, pid_t pid)
{
  (void)pid;
  fprintf(stderr, "|Ret----mprotect--->(");
  print_errno((int)regs->rax);
  fprintf(stderr, ")\n");
}
