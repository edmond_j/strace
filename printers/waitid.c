/*
** header.c for aff in /home/lequeu_m/
**
** Made by martin lequeux-gruninger
** Login   <lequeu_m@epitech.net>
**
** Started on  Fri May 10 17:38:41 2013 martin lequeux-gruninger
** Last update Mon May 13 14:20:44 2013 julien edmond
*/

/*
**       #include <sys/types.h>
**       #include <sys/wait.h>
**
**       pid_t wait(int *status);
**
**       pid_t waitpid(pid_t pid, int *status, int options);
**
**       int waitid(idtype_t idtype, id_t id, siginfo_t *infop, int options);
**
**   Feature Test Macro Requirements for glibc (see feature_test_macros(7)):
**
**       waitid():
**           _SVID_SOURCE || _XOPEN_SOURCE >= 500 ||
**           _XOPEN_SOURCE && _XOPEN_SOURCE_EXTENDED
**           ||  Since glibc 2.12:  _POSIX_C_SOURCE >= 200809L
**
*/

#include <sys/types.h>
#include <sys/wait.h>
#include <unistd.h>
#include "aff.h"
#include "strace.h"

const char			*get_idtype(idtype_t idtype)
{
  int				i;
  static const char * const	types[] = {"P_PID", "P_PGID", "P_ALL", "???"};
  static const idtype_t		ids[] = {P_PID, P_PGID, P_ALL};
  const char			*ret;

  i = -1;
  while (++i < 3 && ids[i] != idtype)
    ;
  if (i >= 3)
    ret = types[3];
  else
    ret = types[i];
  return (ret);
}

void		print_args_waitid(struct user_regs_struct *regs, pid_t pid)
{
  (void)pid;
  fprintf(stderr, "waitid(");
  fprintf(stderr, "%s, %d, %p, %d", get_idtype((idtype_t)regs->rdi),
	  (int)regs->rsi, (void *)regs->rdx, (int)regs->rcx);
  fprintf(stderr, ")\n");
}

void		print_ret_waitid(struct user_regs_struct *regs, pid_t pid)
{
  (void)pid;
  fprintf(stderr, "|Ret----waitid--->(");
  print_errno((int)regs->rax);
  fprintf(stderr, ")\n");
}
