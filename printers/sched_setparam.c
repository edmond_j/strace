/*
** header.c for aff in /home/lequeu_m/
**
** Made by martin lequeux-gruninger
** Login   <lequeu_m@epitech.net>
**
** Started on  Fri May 10 17:38:41 2013 martin lequeux-gruninger
** Last update Tue May 14 14:17:39 2013 julien edmond
*/

/*
**       #include <sched.h>
**
**       int sched_setparam(pid_t pid, const struct sched_param *param);
**
**       int sched_getparam(pid_t pid, struct sched_param *param);
**
**       struct sched_param {
**           ...
**           int sched_priority;
**           ...
**       };
**
*/

#include <sched.h>
#include <unistd.h>
#include "aff.h"
#include "strace.h"

void		print_args_sched_setparam(struct user_regs_struct *regs,
					  pid_t pid)
{
  (void)pid;
  fprintf(stderr, "sched_setparam(");
  fprintf(stderr, "%d, ", (pid_t)regs->rdi);
  print_sched_param(pid, (struct sched_param *)regs->rsi);
  fprintf(stderr, ")\n");
}

void		print_ret_sched_setparam(struct user_regs_struct *regs,
					 pid_t pid)
{
  (void)pid;
  fprintf(stderr, "|Ret----sched_setparam--->(");
  print_errno((int)regs->rax);
  fprintf(stderr, ")\n");
}
