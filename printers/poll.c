/*
** header.c for aff in /home/lequeu_m/
**
** Made by martin lequeux-gruninger
** Login   <lequeu_m@epitech.net>
**
** Started on  Fri May 10 17:38:41 2013 martin lequeux-gruninger
** Last update Wed May 15 13:43:30 2013 julien edmond
*/

/*
**       #include <poll.h>
**
**       int poll(struct pollfd *fds, nfds_t nfds, int timeout);
**
**       #define _GNU_SOURCE          See feature_test_macros(7)
**       #include <poll.h>
**
**       int ppoll(struct pollfd *fds, nfds_t nfds,
**               const struct timespec *timeout_ts, const sigset_t *sigmask);
**
*/

#include <poll.h>
#include <poll.h>
#include <unistd.h>
#include "aff.h"
#include "strace.h"

void		print_args_poll(struct user_regs_struct *regs, pid_t pid)
{
  (void)pid;
  fprintf(stderr, "poll(");
  fprintf(stderr, "%p, %lu, %d", (void *)regs->rdi, (nfds_t)regs->rsi,
	  (int)regs->rdx);
  fprintf(stderr, ")\n");
}

void		print_ret_poll(struct user_regs_struct *regs, pid_t pid)
{
  (void)pid;
  fprintf(stderr, "|Ret----poll--->(");
  print_errno((int)regs->rax);
  fprintf(stderr, ")\n");
}
