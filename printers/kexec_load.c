/*
** header.c for aff in /home/lequeu_m/
**
** Made by martin lequeux-gruninger
** Login   <lequeu_m@epitech.net>
**
** Started on  Fri May 10 17:38:41 2013 martin lequeux-gruninger
** Last update Thu May 16 12:04:24 2013 martin lequeux-gruninger
*/

/*
**       #include <linux/kexec.h>
**       long kexec_load(unsigned long entry, unsigned long nr_segments,
**                       struct kexec_segment *segments, unsigned long flags);
**
*/

#include <unistd.h>
#include "aff.h"
#include "strace.h"

void		print_args_kexec_load(struct user_regs_struct *regs, pid_t pid)
{
  (void)pid;
  fprintf(stderr, "kexec_load(");
  fprintf(stderr, "%lu", (unsigned long)regs->rdi);
  fprintf(stderr, ", %lu", (unsigned long)regs->rsi);
  fprintf(stderr, ", %p", (struct kexec_segment *)regs->rdx);
  fprintf(stderr, ", %lu", (unsigned long)regs->rcx);
  fprintf(stderr, ")\n");
}

void		print_ret_kexec_load(struct user_regs_struct *regs, pid_t pid)
{
  (void)pid;
  fprintf(stderr, "|Ret----kexec_load--->(");
  if ((long)regs->rax < 0)
    print_errno((long)regs->rax);
  fprintf(stderr, ")\n");
}
