/*
** header.c for aff in /home/lequeu_m/
**
** Made by martin lequeux-gruninger
** Login   <lequeu_m@epitech.net>
**
** Started on  Fri May 10 17:38:41 2013 martin lequeux-gruninger
** Last update Thu May 16 13:37:57 2013 julien edmond
*/

/*
**       #include <sys/inotify.h>
**
**       int inotify_init(void);
**       int inotify_init1(int flags);
**
*/

#include <sys/inotify.h>
#include <unistd.h>
#include "aff.h"
#include "strace.h"

void		print_args_inotify_init1(struct user_regs_struct *regs,
					 pid_t pid)
{
  (void)pid;
  fprintf(stderr, "inotify_init1(");
  fprintf(stderr, "%d", (int)regs->rdi);
  fprintf(stderr, ")\n");
}

void		print_ret_inotify_init1(struct user_regs_struct *regs,
					pid_t pid)
{
  (void)pid;
  fprintf(stderr, "|Ret----inotify_init1--->(");
  print_errno((int)regs->rax);
  fprintf(stderr, ")\n");
}
