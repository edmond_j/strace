/*
** header.c for aff in /home/lequeu_m/
**
** Made by martin lequeux-gruninger
** Login   <lequeu_m@epitech.net>
**
** Started on  Fri May 10 17:38:41 2013 martin lequeux-gruninger
** Last update Tue May 14 11:03:27 2013 julien edmond
*/

/*
**       #include <sys/types.h>           See NOTES
**       #include <sys/socket.h>
**
**       int getsockopt(int sockfd, int level, int optname,
**                      void *optval, socklen_t *optlen);
**       int setsockopt(int sockfd, int level, int optname,
**                      const void *optval, socklen_t optlen);
**
*/

#include <sys/types.h>
#include <sys/socket.h>
#include <unistd.h>
#include "aff.h"
#include "strace.h"

void		print_args_setsockopt(struct user_regs_struct *regs, pid_t pid)
{
  (void)pid;
  fprintf(stderr, "setsockopt(");
  fprintf(stderr, "%d, %d, %d, %p, %u",
	  (int)regs->rdi, (int)regs->rsi, (int)regs->rdx,
	  (void *)regs->rcx, (socklen_t)regs->r8);
  fprintf(stderr, ")\n");
}

void		print_ret_setsockopt(struct user_regs_struct *regs, pid_t pid)
{
  (void)pid;
  fprintf(stderr, "|Ret----setsockopt--->(");
  print_errno((int)regs->rax);
  fprintf(stderr, ")\n");
}
