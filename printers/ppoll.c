/*
** header.c for aff in /home/lequeu_m/
**
** Made by martin lequeux-gruninger
** Login   <lequeu_m@epitech.net>
**
** Started on  Fri May 10 17:38:41 2013 martin lequeux-gruninger
** Last update Wed May 15 13:42:40 2013 julien edmond
*/

/*
**       #include <poll.h>
**
**       int poll(struct pollfd *fds, nfds_t nfds, int timeout);
**
**       #define _GNU_SOURCE          See feature_test_macros(7)
**       #include <poll.h>
**
**       int ppoll(struct pollfd *fds, nfds_t nfds,
**               const struct timespec *timeout_ts, const sigset_t *sigmask);
**
*/

#include <poll.h>
#include <poll.h>
#include <unistd.h>
#include "aff.h"
#include "strace.h"

void		print_args_ppoll(struct user_regs_struct *regs, pid_t pid)
{
  (void)pid;
  fprintf(stderr, "ppoll(");
  fprintf(stderr, "%p, %lu, %p, %p", (void *)regs->rdi, (nfds_t)regs->rsi,
	  (void *)regs->rdx, (void *)regs->rcx);
  fprintf(stderr, ")\n");
}

void		print_ret_ppoll(struct user_regs_struct *regs, pid_t pid)
{
  (void)pid;
  fprintf(stderr, "|Ret----ppoll--->(");
  print_errno((int)regs->rax);
  fprintf(stderr, ")\n");
}
