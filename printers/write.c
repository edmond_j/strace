/*
** header.c for aff in /home/lequeu_m/
**
** Made by martin lequeux-gruninger
** Login   <lequeu_m@epitech.net>
**
** Started on  Fri May 10 17:38:41 2013 martin lequeux-gruninger
** Last update Mon May 13 12:05:15 2013 julien edmond
*/

/*
**       #include <unistd.h>
**
**       ssize_t write(int fd, const void *buf, size_t count);
**
*/

#include <unistd.h>
#include <unistd.h>
#include <ctype.h>
#include "aff.h"
#include "strace.h"

void		print_args_write(struct user_regs_struct *regs, pid_t pid)
{
  char		buff[42];
  size_t	to_read;

  to_read = ((size_t)regs->rdx > sizeof(buff) ? sizeof(buff) : regs->rdx);
  fprintf(stderr, "write(");
  read_from_son(pid, (void *)regs->rsi, buff, to_read);
  fprintf(stderr, "%d, ", (int)regs->rdi);
  print_mem(buff, to_read);
  fprintf(stderr, ", %lu", (size_t)regs->rdx);
  fprintf(stderr, ")\n");
}

void		print_ret_write(struct user_regs_struct *regs, pid_t pid)
{
  (void)pid;
  fprintf(stderr, "|Ret----write--->(");
  print_errno(regs->rax);
  fprintf(stderr, ")\n");
}
