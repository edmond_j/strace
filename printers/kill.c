/*
** header.c for aff in /home/lequeu_m/
**
** Made by martin lequeux-gruninger
** Login   <lequeu_m@epitech.net>
**
** Started on  Fri May 10 17:38:41 2013 martin lequeux-gruninger
** Last update Thu May 16 11:59:46 2013 martin lequeux-gruninger
*/

/*
**       #include <sys/types.h>
**       #include <signal.h>
**
**       int kill(pid_t pid, int sig);
**
**   Feature Test Macro Requirements for glibc (see feature_test_macros(7)):
**
**       kill(): _POSIX_C_SOURCE >= 1 || _XOPEN_SOURCE || _POSIX_SOURCE
**
*/

#include <sys/types.h>
#include <signal.h>
#include <unistd.h>
#include "aff.h"
#include "strace.h"

void		print_args_kill(struct user_regs_struct *regs, pid_t pid)
{
  (void)pid;
  fprintf(stderr, "kill(");
  fprintf(stderr, "%d", (int)regs->rdi);
  fprintf(stderr, ", %d", (int)regs->rsi);
  fprintf(stderr, ")\n");
}

void		print_ret_kill(struct user_regs_struct *regs, pid_t pid)
{
  (void)pid;
  fprintf(stderr, "|Ret----kill--->(");
  print_errno((int)regs->rax);
  fprintf(stderr, ")\n");
}
