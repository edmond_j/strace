/*
** header.c for aff in /home/lequeu_m/
**
** Made by martin lequeux-gruninger
** Login   <lequeu_m@epitech.net>
**
** Started on  Fri May 10 17:38:41 2013 martin lequeux-gruninger
** Last update Wed May 15 13:23:01 2013 julien edmond
*/

/*
**       #include <linux/module.h>
**
**       int query_module(const char *name, int which, void *buf,
**                        size_t bufsize, size_t *ret);
**
*/

#include <unistd.h>
#include "aff.h"
#include "strace.h"

void		print_args_query_module(struct user_regs_struct *regs,
					pid_t pid)
{
  fprintf(stderr, "query_module(");
  print_string(pid, (void *)regs->rdi);
  fprintf(stderr, ", %d, %p, %lu, %p", (int)regs->rsi, (void *)regs->rdx,
	  (size_t)regs->rcx, (size_t *)regs->r8);
  fprintf(stderr, ")\n");
}

void		print_ret_query_module(struct user_regs_struct *regs,
				       pid_t pid)
{
  (void)pid;
  fprintf(stderr, "|Ret----query_module--->(");
  print_errno((int)regs->rax);
  fprintf(stderr, ")\n");
}
