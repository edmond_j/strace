/*
** header.c for aff in /home/lequeu_m/
**
** Made by martin lequeux-gruninger
** Login   <lequeu_m@epitech.net>
**
** Started on  Fri May 10 17:38:41 2013 martin lequeux-gruninger
** Last update Wed May 15 11:40:29 2013 julien edmond
*/

/*
**       #include <signal.h>
**
**       int sigpending(sigset_t *set);
**
**   Feature Test Macro Requirements for glibc (see feature_test_macros(7)):
**
**       sigpending(): _POSIX_C_SOURCE >= 1 || _XOPEN_SOURCE || _POSIX_SOURCE
**
*/

#include <signal.h>
#include <unistd.h>
#include "aff.h"
#include "strace.h"

void		print_args_rt_sigpending(struct user_regs_struct *regs,
					 pid_t pid)
{
  (void)pid;
  fprintf(stderr, "rt_sigpending(");
  fprintf(stderr, "%p", (void *)regs->rdi);
  fprintf(stderr, ")\n");
}

void		print_ret_rt_sigpending(struct user_regs_struct *regs,
					pid_t pid)
{
  (void)pid;
  fprintf(stderr, "|Ret----rt_sigpending--->(");
  print_errno((int)regs->rax);
  fprintf(stderr, ")\n");
}
