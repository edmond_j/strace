/*
** header.c for aff in /home/lequeu_m/
**
** Made by martin lequeux-gruninger
** Login   <lequeu_m@epitech.net>
**
** Started on  Fri May 10 17:38:41 2013 martin lequeux-gruninger
** Last update Tue May 14 08:53:15 2013 julien edmond
*/

/*
**       #include <unistd.h>
**
**       int symlink(const char *oldpath, const char *newpath);
**
**   Feature Test Macro Requirements for glibc (see feature_test_macros(7)):
**
**       symlink():
**           _BSD_SOURCE || _XOPEN_SOURCE >= 500 ||
**           _XOPEN_SOURCE && _XOPEN_SOURCE_EXTENDED ||
**           _POSIX_C_SOURCE >= 200112L
**
*/

#include <unistd.h>
#include <unistd.h>
#include "aff.h"
#include "strace.h"

void		print_args_symlink(struct user_regs_struct *regs, pid_t pid)
{
  fprintf(stderr, "symlink(");
  print_string(pid, (void *)regs->rdi);
  fputs(", ", stderr);
  print_string(pid, (void *)regs->rsi);
  fprintf(stderr, ")\n");
}

void		print_ret_symlink(struct user_regs_struct *regs, pid_t pid)
{
  (void)pid;
  fprintf(stderr, "|Ret----symlink--->(");
  print_errno((int)regs->rax);
  fprintf(stderr, ")\n");
}
