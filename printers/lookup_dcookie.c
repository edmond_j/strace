/*
** header.c for aff in /home/lequeu_m/
**
** Made by martin lequeux-gruninger
** Login   <lequeu_m@epitech.net>
**
** Started on  Fri May 10 17:38:41 2013 martin lequeux-gruninger
** Last update Thu May 16 09:32:38 2013 julien edmond
*/

/*
**       int lookup_dcookie(u64 cookie, char *buffer, size_t len);
**
*/

#include <unistd.h>
#include "aff.h"
#include "strace.h"

void		print_args_lookup_dcookie(struct user_regs_struct *regs,
					  pid_t pid)
{
  (void)pid;
  fprintf(stderr, "lookup_dcookie(");
  fprintf(stderr, "%lu, %p, %lu", (unsigned long)regs->rdi, (char *)regs->rsi,
	  (size_t)regs->rdx);
  fprintf(stderr, ")\n");
}

void		print_ret_lookup_dcookie(struct user_regs_struct *regs,
					 pid_t pid)
{
  (void)pid;
  fprintf(stderr, "|Ret----lookup_dcookie--->(");
  print_errno((int)regs->rax);
  fprintf(stderr, ")\n");
}
