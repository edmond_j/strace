/*
** header.c for aff in /home/lequeu_m/
**
** Made by martin lequeux-gruninger
** Login   <lequeu_m@epitech.net>
**
** Started on  Fri May 10 17:38:41 2013 martin lequeux-gruninger
** Last update Tue May 14 10:22:08 2013 julien edmond
*/

/*
**       #include <sys/signalfd.h>
**
**       int signalfd(int fd, const sigset_t *mask, int flags);
**
*/

#include <sys/signalfd.h>
#include <unistd.h>
#include "aff.h"
#include "strace.h"

void		print_args_signalfd(struct user_regs_struct *regs, pid_t pid)
{
  (void)pid;
  fprintf(stderr, "signalfd(");
  fprintf(stderr, "%d, %p, %d", (int)regs->rdi, (void *)regs->rsi,
	  (int)regs->rdx);
  fprintf(stderr, ")\n");
}

void		print_ret_signalfd(struct user_regs_struct *regs, pid_t pid)
{
  (void)pid;
  fprintf(stderr, "|Ret----signalfd--->(");
  print_errno((int)regs->rax);
  fprintf(stderr, ")\n");
}
