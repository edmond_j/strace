/*
** header.c for aff in /home/lequeu_m/
**
** Made by martin lequeux-gruninger
** Login   <lequeu_m@epitech.net>
**
** Started on  Fri May 10 17:38:41 2013 martin lequeux-gruninger
** Last update Tue May 14 08:17:27 2013 julien edmond
*/

/*
**       #include <time.h>
**
**       int timer_delete(timer_t timerid);
**
**       Link with -lrt.
**
**   Feature Test Macro Requirements for glibc (see feature_test_macros(7)):
**
**       timer_delete(): _POSIX_C_SOURCE >= 199309L
**
*/

#include <time.h>
#include <unistd.h>
#include "aff.h"
#include "strace.h"

void		print_args_timer_delete(struct user_regs_struct *regs,
					pid_t pid)
{
  (void)pid;
  fprintf(stderr, "timer_delete(");
  printf("%ld", regs->rdi);
  fprintf(stderr, ")\n");
}

void		print_ret_timer_delete(struct user_regs_struct *regs, pid_t pid)
{
  (void)pid;
  fprintf(stderr, "|Ret----timer_delete--->(");
  print_errno((int)regs->rax);
  fprintf(stderr, ")\n");
}
