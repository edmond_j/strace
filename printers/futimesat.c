/*
** header.c for aff in /home/lequeu_m/
**
** Made by martin lequeux-gruninger
** Login   <lequeu_m@epitech.net>
**
** Started on  Fri May 10 17:38:41 2013 martin lequeux-gruninger
** Last update Thu May 16 10:32:06 2013 martin lequeux-gruninger
*/

/*
**       #include <fcntl.h>  Definition of AT_* constants
**
**       int futimesat(int dirfd, const char *pathname,
**                     const struct timeval times[2]);
**
**   Feature Test Macro Requirements for glibc (see feature_test_macros(7)):
**
**       futimesat():
**           Since glibc 2.10:
**               _XOPEN_SOURCE >= 700 || _POSIX_C_SOURCE >= 200809L
**           Before glibc 2.10:
**               _ATFILE_SOURCE
**
*/

#include <fcntl.h>
#include <unistd.h>
#include "aff.h"
#include "strace.h"

void		print_args_futimesat(struct user_regs_struct *regs, pid_t pid)
{
  (void)pid;
  fprintf(stderr, "futimesat(");
  fprintf(stderr, "%d", (int)regs->rdi);
  fprintf(stderr, ", ");
  print_string(pid, (void *)regs->rsi);
  fprintf(stderr, ", %p", (const struct timeval *)regs->rdx);
  fprintf(stderr, ")\n");
}

void		print_ret_futimesat(struct user_regs_struct *regs, pid_t pid)
{
  (void)pid;
  fprintf(stderr, "|Ret----futimesat--->(");
  print_errno((int)regs->rax);
  fprintf(stderr, ")\n");
}
