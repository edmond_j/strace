/*
** header.c for aff in /home/lequeu_m/
**
** Made by martin lequeux-gruninger
** Login   <lequeu_m@epitech.net>
**
** Started on  Fri May 10 17:38:41 2013 martin lequeux-gruninger
** Last update Tue May 14 08:47:30 2013 julien edmond
*/

/*
**       #include <unistd.h>
**       #include <linux/sysctl.h>
**
**       int _sysctl(struct __sysctl_args *args);
**
*/

#include <unistd.h>
#include <linux/sysctl.h>
#include <unistd.h>
#include "aff.h"
#include "strace.h"

void		print_args__sysctl(struct user_regs_struct *regs, pid_t pid)
{
  (void)pid;
  fprintf(stderr, "_sysctl(");
  fprintf(stderr, "%p", (void *)regs->rdi);
  fprintf(stderr, ")\n");
}

void		print_ret__sysctl(struct user_regs_struct *regs, pid_t pid)
{
  (void)pid;
  fprintf(stderr, "|Ret----_sysctl--->(");
  print_errno((int)regs->rax);
  fprintf(stderr, ")\n");
}
