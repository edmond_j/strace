/*
** header.c for aff in /home/lequeu_m/
**
** Made by martin lequeux-gruninger
** Login   <lequeu_m@epitech.net>
**
** Started on  Fri May 10 17:38:41 2013 martin lequeux-gruninger
** Last update Wed May 15 13:34:01 2013 julien edmond
*/

/*
**       #include <linux/getcpu.h>
**
**       int getcpu(unsigned *cpu, unsigned *node,
**                  struct getcpu_cache *tcache);
**
*/

#include <unistd.h>
#include "aff.h"
#include "strace.h"

void		print_args_process_vm_writev(struct user_regs_struct *regs,
					     pid_t pid)
{
  (void)pid;
  fprintf(stderr, "process_vm_writev(");
  fprintf(stderr, "%p, %p, %p", (unsigned *)regs->rdi, (unsigned *)regs->rsi,
	  (void *)regs->rdx);
  fprintf(stderr, ")\n");
}

void		print_ret_process_vm_writev(struct user_regs_struct *regs,
					    pid_t pid)
{
  (void)pid;
  fprintf(stderr, "|Ret----process_vm_writev--->(");
  print_errno((int)regs->rax);
  fprintf(stderr, ")\n");
}
