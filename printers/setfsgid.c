/*
** header.c for aff in /home/lequeu_m/
**
** Made by martin lequeux-gruninger
** Login   <lequeu_m@epitech.net>
**
** Started on  Fri May 10 17:38:41 2013 martin lequeux-gruninger
** Last update Tue May 14 13:11:29 2013 julien edmond
*/

/*
**       #include <unistd.h>  glibc uses <sys/fsuid.h>
**
**       int setfsgid(uid_t fsgid);
**
*/

#include <unistd.h>
#include <unistd.h>
#include "aff.h"
#include "strace.h"

void		print_args_setfsgid(struct user_regs_struct *regs, pid_t pid)
{
  (void)pid;
  fprintf(stderr, "setfsgid(");
  fprintf(stderr, "%d", (uid_t)regs->rdi);
  fprintf(stderr, ")\n");
}

void		print_ret_setfsgid(struct user_regs_struct *regs, pid_t pid)
{
  (void)pid;
  fprintf(stderr, "|Ret----setfsgid--->(");
  print_errno((int)regs->rax);
  fprintf(stderr, ")\n");
}
