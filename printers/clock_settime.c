/*
** header.c for aff in /home/lequeu_m/
**
** Made by martin lequeux-gruninger
** Login   <lequeu_m@epitech.net>
**
** Started on  Fri May 10 17:38:41 2013 martin lequeux-gruninger
** Last update Thu May 16 10:22:39 2013 martin lequeux-gruninger
*/

/*
**       #include <time.h>
**
**       int clock_getres(clockid_t clk_id, struct timespec *res);
**
**       int clock_gettime(clockid_t clk_id, struct timespec *tp);
**
**       int clock_settime(clockid_t clk_id, const struct timespec *tp);
**
**       Link with -lrt.
**
**   Feature Test Macro Requirements for glibc (see feature_test_macros(7)):
**
**       clock_getres(), clock_gettime(), clock_settime():
**              _POSIX_C_SOURCE >= 199309L
**
*/

#include <time.h>
#include <unistd.h>
#include "aff.h"
#include "strace.h"

void		print_args_clock_settime(struct user_regs_struct *regs, pid_t pid)
{
  (void)pid;
  fprintf(stderr, "clock_settime(");
  fprintf(stderr, "%d", (clockid_t)regs->rdi);
  fprintf(stderr, ", %p", (const struct timespec *)regs->rsi);
  fprintf(stderr, ")\n");
}

void		print_ret_clock_settime(struct user_regs_struct *regs, pid_t pid)
{
  (void)pid;
  fprintf(stderr, "|Ret----clock_settime--->(");
  print_errno((int)regs->rax);
  fprintf(stderr, ")\n");
}
