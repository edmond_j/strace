/*
** header.c for aff in /home/lequeu_m/
**
** Made by martin lequeux-gruninger
** Login   <lequeu_m@epitech.net>
**
** Started on  Fri May 10 17:38:41 2013 martin lequeux-gruninger
** Last update Wed May 15 13:04:17 2013 martin lequeux-gruninger
*/

/*
**       #include <unistd.h>
**
**       int setpgid(pid_t pid, pid_t pgid);
**       pid_t getpgid(pid_t pid);
**
**       pid_t getpgrp(void);                  POSIX.1 version
**       pid_t getpgrp(pid_t pid);             BSD version
**
**       int setpgrp(void);                    System V version
**       int setpgrp(pid_t pid, pid_t pgid);   BSD version
**
**   Feature Test Macro Requirements for glibc (see feature_test_macros(7)):
**
**       getpgid():
**           _XOPEN_SOURCE >= 500 || _XOPEN_SOURCE && _XOPEN_SOURCE_EXTENDED
**           ||  Since glibc 2.12:  _POSIX_C_SOURCE >= 200809L
**
**       setpgrp() (POSIX.1):
**           _SVID_SOURCE || _XOPEN_SOURCE >= 500 ||
**           _XOPEN_SOURCE && _XOPEN_SOURCE_EXTENDED
**
**       setpgrp() (BSD), getpgrp() (BSD):
**           _BSD_SOURCE &&
**               ! (_POSIX_SOURCE || _POSIX_C_SOURCE || _XOPEN_SOURCE ||
**                  _XOPEN_SOURCE_EXTENDED || _GNU_SOURCE || _SVID_SOURCE)
**
*/

#include <unistd.h>
#include <unistd.h>
#include "aff.h"
#include "strace.h"

void		print_args_getpgrp(struct user_regs_struct *regs, pid_t pid)
{
  (void)pid;
  (void)regs;
  fprintf(stderr, "getpgrp(");
  fprintf(stderr, ")\n");
}

void		print_ret_getpgrp(struct user_regs_struct *regs, pid_t pid)
{
  (void)pid;
  fprintf(stderr, "|Ret----getpgrp--->(");
  print_errno((pid_t)regs->rax);
  fprintf(stderr, ")\n");
}
