/*
** header.c for aff in /home/lequeu_m/
**
** Made by martin lequeux-gruninger
** Login   <lequeu_m@epitech.net>
**
** Started on  Fri May 10 17:38:41 2013 martin lequeux-gruninger
** Last update Thu May 16 10:32:26 2013 martin lequeux-gruninger
*/

/*
**       int getdents(unsigned int fd, struct linux_dirent *dirp,
**                    unsigned int count);
**
*/

#include <unistd.h>
#include "aff.h"
#include "strace.h"

void		print_args_getdents(struct user_regs_struct *regs, pid_t pid)
{
  (void)pid;
  (void)regs;
  fprintf(stderr, "getdents(");
  fprintf(stderr, "%u", (unsigned int)regs->rdi);
  fprintf(stderr, ", %p", (struct linux_dirent *)regs->rsi);
  fprintf(stderr, ", %u", (unsigned int)regs->rdx);
  fprintf(stderr, ")\n");
}

void		print_ret_getdents(struct user_regs_struct *regs, pid_t pid)
{
  (void)pid;
  (void)regs;
  fprintf(stderr, "|Ret----getdents--->(");
  print_errno((int)regs->rax);
  fprintf(stderr, ")\n");
}
