/*
** header.c for aff in /home/lequeu_m/
**
** Made by martin lequeux-gruninger
** Login   <lequeu_m@epitech.net>
**
** Started on  Fri May 10 17:38:41 2013 martin lequeux-gruninger
** Last update Thu May 16 11:48:18 2013 martin lequeux-gruninger
*/

/*
**       #include <unistd.h>  for libc5
**       #include <sys/io.h>  for glibc
**
**       int ioperm(unsigned long from, unsigned long num, int turn_on);
**
*/

#include <unistd.h>
#include <unistd.h>
#include "aff.h"
#include "strace.h"

void		print_args_ioperm(struct user_regs_struct *regs, pid_t pid)
{
  (void)pid;
  fprintf(stderr, "ioperm(");
  fprintf(stderr, "%lu", (unsigned long)regs->rdi);
  fprintf(stderr, ", %lu", (unsigned long)regs->rsi);
  fprintf(stderr, ", %d", (int)regs->rdx);
  fprintf(stderr, ")\n");
}

void		print_ret_ioperm(struct user_regs_struct *regs, pid_t pid)
{
  (void)pid;
  fprintf(stderr, "|Ret----ioperm--->(");
  print_errno((int)regs->rax);
  fprintf(stderr, ")\n");
}
