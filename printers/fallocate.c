/*
** header.c for aff in /home/lequeu_m/
**
** Made by martin lequeux-gruninger
** Login   <lequeu_m@epitech.net>
**
** Started on  Fri May 10 17:38:41 2013 martin lequeux-gruninger
** Last update Thu May 16 10:26:37 2013 martin lequeux-gruninger
*/

/*
**       #define _GNU_SOURCE              See feature_test_macros(7)
**       #include <fcntl.h>
**
**       int fallocate(int fd, int mode, off_t offset, off_t len);
**
*/

#include <fcntl.h>
#include <unistd.h>
#include "aff.h"
#include "strace.h"

void		print_args_fallocate(struct user_regs_struct *regs, pid_t pid)
{
  (void)pid;
  fprintf(stderr, "fallocate(");
  fprintf(stderr, "%d", (int)regs->rdi);
  fprintf(stderr, ", %d", (int)regs->rsi);
  fprintf(stderr, ", %ld", (off_t)regs->rdx);
  fprintf(stderr, ", %ld", (off_t)regs->rcx);
  fprintf(stderr, ")\n");
}

void		print_ret_fallocate(struct user_regs_struct *regs, pid_t pid)
{
  (void)pid;
  fprintf(stderr, "|Ret----fallocate--->(");
  print_errno((int)regs->rax);
  fprintf(stderr, ")\n");
}
