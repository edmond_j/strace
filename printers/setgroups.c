/*
** header.c for aff in /home/lequeu_m/
**
** Made by martin lequeux-gruninger
** Login   <lequeu_m@epitech.net>
**
** Started on  Fri May 10 17:38:41 2013 martin lequeux-gruninger
** Last update Thu May 16 09:57:38 2013 martin lequeux-gruninger
*/

/*
**       #include <sys/types.h>
**       #include <unistd.h>
**
**       int getgroups(int size, gid_t list[]);
**
**       #include <grp.h>
**
**       int setgroups(size_t size, const gid_t *list);
**
**   Feature Test Macro Requirements for glibc (see feature_test_macros(7)):
**
**       setgroups(): _BSD_SOURCE
**
*/

#include <sys/types.h>
#include <unistd.h>
#include <grp.h>
#include <unistd.h>
#include "aff.h"
#include "strace.h"

void		print_args_setgroups(struct user_regs_struct *regs, pid_t pid)
{
  (void)pid;
  fprintf(stderr, "setgroups(");
  fprintf(stderr, "%d, %p", (int)regs->rdi, (gid_t *)regs->rsi);
  fprintf(stderr, ")\n");
}

void		print_ret_setgroups(struct user_regs_struct *regs, pid_t pid)
{
  (void)pid;
  fprintf(stderr, "|Ret----setgroups--->(");
  print_errno((int)regs->rax);
  fprintf(stderr, ")\n");
}
