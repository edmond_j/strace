/*
** header.c for aff in /home/lequeu_m/
**
** Made by martin lequeux-gruninger
** Login   <lequeu_m@epitech.net>
**
** Started on  Fri May 10 17:38:41 2013 martin lequeux-gruninger
** Last update Thu May 16 10:30:25 2013 martin lequeux-gruninger
*/

/*
**       #include <sys/file.h>
**
**       int flock(int fd, int operation);
**
*/

#include <sys/file.h>
#include <unistd.h>
#include "aff.h"
#include "strace.h"

void		print_args_flock(struct user_regs_struct *regs, pid_t pid)
{
  (void)pid;
  fprintf(stderr, "flock(");
  fprintf(stderr, "%d", (int)regs->rdi);
  fprintf(stderr, ", %d", (int)regs->rsi);
  fprintf(stderr, ")\n");
}

void		print_ret_flock(struct user_regs_struct *regs, pid_t pid)
{
  (void)pid;
  fprintf(stderr, "|Ret----flock--->(");
  print_errno((int)regs->rax);
  fprintf(stderr, ")\n");
}
