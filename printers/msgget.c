/*
** header.c for aff in /home/lequeu_m/
**
** Made by martin lequeux-gruninger
** Login   <lequeu_m@epitech.net>
**
** Started on  Fri May 10 17:38:41 2013 martin lequeux-gruninger
** Last update Wed May 15 17:04:00 2013 julien edmond
*/

/*
**       #include <sys/types.h>
**       #include <sys/ipc.h>
**       #include <sys/msg.h>
**
**       int msgget(key_t key, int msgflg);
**
*/

#include <sys/types.h>
#include <sys/ipc.h>
#include <sys/msg.h>
#include <unistd.h>
#include "aff.h"
#include "strace.h"

void		print_args_msgget(struct user_regs_struct *regs, pid_t pid)
{
  (void)pid;
  fprintf(stderr, "msgget(");
  fprintf(stderr, "%d, %d", (key_t)regs->rdi, (int)regs->rsi);
  fprintf(stderr, ")\n");
}

void		print_ret_msgget(struct user_regs_struct *regs, pid_t pid)
{
  (void)pid;
  fprintf(stderr, "|Ret----msgget--->(");
  print_errno((int)regs->rax);
  fprintf(stderr, ")\n");
}
