/*
** header.c for aff in /home/lequeu_m/
**
** Made by martin lequeux-gruninger
** Login   <lequeu_m@epitech.net>
**
** Started on  Fri May 10 17:38:41 2013 martin lequeux-gruninger
** Last update Wed May 15 14:31:55 2013 julien edmond
*/

/*
**       #include <fcntl.h>  Definition of AT_* constants
**
**       int futimesat(int dirfd, const char *pathname,
**                     const struct timeval times[2]);
**
**   Feature Test Macro Requirements for glibc (see feature_test_macros(7)):
**
**       futimesat():
**           Since glibc 2.10:
**               _XOPEN_SOURCE >= 700 || _POSIX_C_SOURCE >= 200809L
**           Before glibc 2.10:
**               _ATFILE_SOURCE
**
*/

#include <fcntl.h>
#include <unistd.h>
#include "aff.h"
#include "strace.h"

void		print_args_newfstatat(struct user_regs_struct *regs, pid_t pid)
{
  fprintf(stderr, "newfstatat(");
  fprintf(stderr, "%d, ", (int)regs->rdi);
  print_string(pid, (void *)regs->rsi);
  fprintf(stderr, ", %p, %d", (void *)regs->rdx, (int)regs->rcx);
  fprintf(stderr, ")\n");
}

void		print_ret_newfstatat(struct user_regs_struct *regs, pid_t pid)
{
  (void)pid;
  fprintf(stderr, "|Ret----newfstatat--->(");
  print_errno((int)regs->rax);
  fprintf(stderr, ")\n");
}
