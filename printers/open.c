/*
** header.c for aff in /home/lequeu_m/
**
** Made by martin lequeux-gruninger
** Login   <lequeu_m@epitech.net>
**
** Started on  Fri May 10 17:38:41 2013 martin lequeux-gruninger
** Last update Wed May 15 14:19:45 2013 julien edmond
*/

/*
**       #include <sys/types.h>
**       #include <sys/stat.h>
**       #include <fcntl.h>
**
**       int open(const char *pathname, int flags);
**       int open(const char *pathname, int flags, mode_t mode);
**
**       int creat(const char *pathname, mode_t mode);
**
*/

#include <sys/types.h>
#include <sys/stat.h>
#include <fcntl.h>
#include <unistd.h>
#include "aff.h"
#include "strace.h"

const char			*open_flags_string(int index)
{
  static const char * const	str[] = {"O_RDONLY", "O_WRONLY", "O_RDWR",
					 "O_APPEND", "O_ASYNC", "O_CLOEXEC",
					 "O_CREAT", "O_DIRECT", "O_DIRECTORY",
					 "O_EXCL", "O_LARGEFILE", "O_NOATIME",
					 "O_NOCTTY", "O_NOFOLLOW",
					 "O_NONBLOCK", "O_NDELAY", "O_SYNC",
					 "O_TRUNC"};

  return (str[index]);
}

void			print_open_flags(int flags)
{
  static const int	masks[] = {O_RDONLY, O_WRONLY, O_RDWR, O_APPEND,
				   O_ASYNC, O_CLOEXEC, O_CREAT, O_DIRECT,
				   O_DIRECTORY, O_EXCL, O_LARGEFILE,
				   O_NOATIME, O_NOCTTY, O_NOFOLLOW,
				   O_NONBLOCK, O_NDELAY, O_SYNC, O_TRUNC};
  int			i;
  unsigned		count;

  count = 0;
  i = -1;
  while ((unsigned)++i < sizeof(masks) / sizeof(const int))
    if ((masks[i] & flags) == masks[i])
      {
	if (count)
	  fputc('|', stderr);
	fputs(open_flags_string(i), stderr);
	flags &= ~masks[i];
	++count;
      }
  if (flags)
    {
      if (count)
	fputc('|', stderr);
      fprintf(stderr, "0x%x", flags);
    }
}

void		print_args_open(struct user_regs_struct *regs, pid_t pid)
{
  fprintf(stderr, "open(");
  print_string(pid, (void *)regs->rdi);
  fputs(", ", stderr);
  print_open_flags((int)regs->rsi);
  if (((int)regs->rsi & O_CREAT) == O_CREAT)
    fprintf(stderr, ", %04o", (mode_t)regs->rdx);
  fprintf(stderr, ")\n");
}

void		print_ret_open(struct user_regs_struct *regs, pid_t pid)
{
  (void)pid;
  fprintf(stderr, "|Ret----open--->(");
  print_errno((int)regs->rax);
  fprintf(stderr, ")\n");
}
