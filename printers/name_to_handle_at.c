/*
** header.c for aff in /home/lequeu_m/
**
** Made by martin lequeux-gruninger
** Login   <lequeu_m@epitech.net>
**
** Started on  Fri May 10 17:38:41 2013 martin lequeux-gruninger
** Last update Wed May 15 14:59:14 2013 julien edmond
*/

#include <sys/socket.h>
#include <unistd.h>
#include "aff.h"
#include "strace.h"

void		print_args_name_to_handle_at(struct user_regs_struct *regs,
					     pid_t pid)
{
  (void)pid;
  (void)regs;
  fprintf(stderr, "name_to_handle_at(");
  fprintf(stderr, ")\n");
}

void		print_ret_name_to_handle_at(struct user_regs_struct *regs,
					    pid_t pid)
{
  (void)pid;
  (void)regs;
  fprintf(stderr, "|Ret----name_to_handle_at--->(");
  fprintf(stderr, ")\n");
}
