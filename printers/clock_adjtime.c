/*
** header.c for aff in /home/lequeu_m/
**
** Made by martin lequeux-gruninger
** Login   <lequeu_m@epitech.net>
**
** Started on  Fri May 10 17:38:41 2013 martin lequeux-gruninger
** Last update Wed May 15 10:36:16 2013 martin lequeux-gruninger
*/

/*
**       #define _GNU_SOURCE
**       #include <sys/socket.h>
**
**       int recvmmsg(int sockfd, struct mmsghdr *msgvec, unsigned int vlen,
**                    unsigned int flags, struct timespec *timeout);
**
*/

#include <sys/socket.h>
#include <unistd.h>
#include "aff.h"
#include "strace.h"

void		print_args_clock_adjtime(struct user_regs_struct *regs, pid_t pid)
{
  (void)pid;
  (void)regs;
  fprintf(stderr, "clock_adjtime(");
  fprintf(stderr, "UNIMPLEMENTED SYSCALL");
  fprintf(stderr, ")\n");
}

void		print_ret_clock_adjtime(struct user_regs_struct *regs, pid_t pid)
{
  (void)pid;
  (void)regs;
  fprintf(stderr, "|Ret----clock_adjtime--->(");
  fprintf(stderr, "UNIMPLEMENTED SYSCALL");
  fprintf(stderr, ")\n");
}
