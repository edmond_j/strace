/*
** header.c for aff in /home/lequeu_m/
**
** Made by martin lequeux-gruninger
** Login   <lequeu_m@epitech.net>
**
** Started on  Fri May 10 17:38:41 2013 martin lequeux-gruninger
** Last update Wed May 15 11:29:10 2013 julien edmond
*/

/*
**       long  rt_sigtimedwait(const sigset_t *uthese, siginfo_t *uinfo,
**                             const struct timespec uts, size_t sigsetsize);
**
**       #include <signal.h>
**
**       int sigwaitinfo(const sigset_t *set, siginfo_t *info);
**
**       int sigtimedwait(const sigset_t *set, siginfo_t *info,
**                        const struct timespec *timeout);
**
**   Feature Test Macro Requirements for glibc (see feature_test_macros(7)):
**
**       sigwaitinfo(), sigtimedwait(): _POSIX_C_SOURCE >= 199309L
**
*/

#include <signal.h>
#include <unistd.h>
#include "aff.h"
#include "strace.h"

void		print_args_rt_sigtimedwait(struct user_regs_struct *regs,
					   pid_t pid)
{
  (void)pid;
  fprintf(stderr, "rt_sigtimedwait(");
  fprintf(stderr, "%p, %p, %lx, %lu", (void *)regs->rdi, (void *)regs->rsi,
	  regs->rdx, (size_t)regs->rcx);
  fprintf(stderr, ")\n");
}

void		print_ret_rt_sigtimedwait(struct user_regs_struct *regs,
					  pid_t pid)
{
  (void)pid;
  fprintf(stderr, "|Ret----rt_sigtimedwait--->(");
  if ((long)regs->rax < 0)
    print_errno((int)regs->rax);
  else
    fprintf(stderr, "%ld", (long)regs->rax);
  fprintf(stderr, ")\n");
}
