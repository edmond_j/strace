/*
** header.c for aff in /home/lequeu_m/
**
** Made by martin lequeux-gruninger
** Login   <lequeu_m@epitech.net>
**
** Started on  Fri May 10 17:38:41 2013 martin lequeux-gruninger
** Last update Wed May 15 11:50:47 2013 martin lequeux-gruninger
*/

/*
**       #include <sys/epoll.h>
**
**       int epoll_create(int size);
**       int epoll_create1(int flags);
**
*/

#include <sys/epoll.h>
#include <unistd.h>
#include "aff.h"
#include "strace.h"

void		print_args_epoll_wait_old(struct user_regs_struct *regs, pid_t pid)
{
  (void)pid;
  (void)regs;
  fprintf(stderr, "epoll_wait_old(");
  fprintf(stderr, "UNIMPLEMENTED SYSCALL");
  fprintf(stderr, ")\n");
}

void		print_ret_epoll_wait_old(struct user_regs_struct *regs, pid_t pid)
{
  (void)pid;
  (void)regs;
  fprintf(stderr, "|Ret----epoll_wait_old--->(");
  fprintf(stderr, "UNIMPLEMENTED SYSCALL");
  fprintf(stderr, ")\n");
}
