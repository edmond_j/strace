/*
** header.c for aff in /home/lequeu_m/
**
** Made by martin lequeux-gruninger
** Login   <lequeu_m@epitech.net>
**
** Started on  Fri May 10 17:38:41 2013 martin lequeux-gruninger
** Last update Tue May 14 14:15:55 2013 julien edmond
*/

/*
**       #include <sched.h>
**
**       int sched_setscheduler(pid_t pid, int policy,
**                              const struct sched_param *param);
**
**       int sched_getscheduler(pid_t pid);
**
**       struct sched_param {
**           ...
**           int sched_priority;
**           ...
**       };
**
*/

#include <sched.h>
#include <unistd.h>
#include "aff.h"
#include "strace.h"

void			print_sched_param(pid_t pid, struct sched_param *from)
{
  struct sched_param	param;

  read_from_son(pid, from, &param, sizeof(param));
  fprintf(stderr, "{sched_priority=%d}", param.sched_priority);
}

void		print_args_sched_setscheduler(struct user_regs_struct *regs,
					      pid_t pid)
{
  (void)pid;
  fprintf(stderr, "sched_setscheduler(");
  fprintf(stderr, "%d, %d, ", (pid_t)regs->rdi, (int)regs->rsi);
  print_sched_param(pid, (struct sched_param *)regs->rdx);
  fprintf(stderr, ")\n");
}

void		print_ret_sched_setscheduler(struct user_regs_struct *regs,
					     pid_t pid)
{
  (void)pid;
  fprintf(stderr, "|Ret----sched_setscheduler--->(");
  print_errno((int)regs->rax);
  fprintf(stderr, ")\n");
}
