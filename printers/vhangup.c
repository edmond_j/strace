/*
** header.c for aff in /home/lequeu_m/
**
** Made by martin lequeux-gruninger
** Login   <lequeu_m@epitech.net>
**
** Started on  Fri May 10 17:38:41 2013 martin lequeux-gruninger
** Last update Mon May 13 14:33:13 2013 julien edmond
*/

/*
**       #include <unistd.h>
**
**       int vhangup(void);
**
**   Feature Test Macro Requirements for glibc (see feature_test_macros(7)):
**
**       vhangup(): _BSD_SOURCE || (_XOPEN_SOURCE && _XOPEN_SOURCE < 500)
**
*/

#include <unistd.h>
#include <unistd.h>
#include "aff.h"
#include "strace.h"

void		print_args_vhangup(struct user_regs_struct *regs, pid_t pid)
{
  (void)pid;
  (void)regs;
  fprintf(stderr, "vhangup(");
  fprintf(stderr, ")\n");
}

void		print_ret_vhangup(struct user_regs_struct *regs, pid_t pid)
{
  (void)pid;
  fprintf(stderr, "|Ret----vhangup--->(");
  print_errno((int)regs->rax);
  fprintf(stderr, ")\n");
}
