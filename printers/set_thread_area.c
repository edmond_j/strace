/*
** header.c for aff in /home/lequeu_m/
**
** Made by martin lequeux-gruninger
** Login   <lequeu_m@epitech.net>
**
** Started on  Fri May 10 17:38:41 2013 martin lequeux-gruninger
** Last update Tue May 14 11:01:06 2013 julien edmond
*/

/*
**       #include <linux/unistd.h>
**       #include <asm/ldt.h>
**
**       int set_thread_area(struct user_desc *u_info);
**
*/

#include <linux/unistd.h>
#include <asm/ldt.h>
#include <unistd.h>
#include "aff.h"
#include "strace.h"

void		print_args_set_thread_area(struct user_regs_struct *regs,
					   pid_t pid)
{
  (void)pid;
  fprintf(stderr, "set_thread_area(");
  fprintf(stderr, "%p", (void *)regs->rdi);
  fprintf(stderr, ")\n");
}

void		print_ret_set_thread_area(struct user_regs_struct *regs,
					  pid_t pid)
{
  (void)pid;
  fprintf(stderr, "|Ret----set_thread_area--->(");
  print_errno((int)regs->rax);
  fprintf(stderr, ")\n");
}
