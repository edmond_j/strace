/*
** header.c for aff in /home/lequeu_m/
**
** Made by martin lequeux-gruninger
** Login   <lequeu_m@epitech.net>
**
** Started on  Fri May 10 17:38:41 2013 martin lequeux-gruninger
** Last update Thu May 16 10:25:03 2013 martin lequeux-gruninger
*/

/*
**       #include <sys/epoll.h>
**
**       int epoll_wait(int epfd, struct epoll_event *events,
**                      int maxevents, int timeout);
**       int epoll_pwait(int epfd, struct epoll_event *events,
**                      int maxevents, int timeout,
**                      const sigset_t *sigmask);
**
*/

#include <sys/epoll.h>
#include <unistd.h>
#include "aff.h"
#include "strace.h"

void		print_args_epoll_pwait(struct user_regs_struct *regs, pid_t pid)
{
  (void)pid;
  fprintf(stderr, "epoll_pwait(");
  fprintf(stderr, "%u", (int)regs->rdi);
  fprintf(stderr, ", %p", (struct epoll_event *)regs->rsi);
  fprintf(stderr, ", %d", (int)regs->rdx);
  fprintf(stderr, ", %d", (int)regs->rcx);
  fprintf(stderr, ", %p", (const sigset_t *)regs->r8);
  fprintf(stderr, ")\n");
}

void		print_ret_epoll_pwait(struct user_regs_struct *regs, pid_t pid)
{
  (void)pid;
  fprintf(stderr, "|Ret----epoll_pwait--->(");
  print_errno((int)regs->rax);
  fprintf(stderr, ")\n");
}
