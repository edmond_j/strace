/*
** header.c for aff in /home/lequeu_m/
**
** Made by martin lequeux-gruninger
** Login   <lequeu_m@epitech.net>
**
** Started on  Fri May 10 17:38:41 2013 martin lequeux-gruninger
** Last update Tue May 14 11:09:08 2013 julien edmond
*/

/*
**       #include <sys/types.h>
**       #include <sys/shm.h>
**
**       void *shmat(int shmid, const void *shmaddr, int shmflg);
**
**       int shmdt(const void *shmaddr);
**
*/

#include <sys/types.h>
#include <sys/shm.h>
#include <unistd.h>
#include "aff.h"
#include "strace.h"

void		print_args_shmat(struct user_regs_struct *regs, pid_t pid)
{
  (void)pid;
  fprintf(stderr, "shmat(");
  fprintf(stderr, "%d, %p, %d", (int)regs->rdi, (void *)regs->rsi,
	  (int)regs->rdx);
  fprintf(stderr, ")\n");
}

void		print_ret_shmat(struct user_regs_struct *regs, pid_t pid)
{
  (void)pid;
  fprintf(stderr, "|Ret----shmat--->(");
  if ((void *)regs->rax < (void *)-1)
    print_errno((int)regs->rax);
  else
    fprintf(stderr, "%p", (void *)regs->rax);
  fprintf(stderr, ")\n");
}
