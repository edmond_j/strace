/*
** header.c for aff in /home/lequeu_m/
**
** Made by martin lequeux-gruninger
** Login   <lequeu_m@epitech.net>
**
** Started on  Fri May 10 17:38:41 2013 martin lequeux-gruninger
** Last update Wed May 15 17:38:18 2013 julien edmond
*/

/*
**       #include <unistd.h>
**       #include <sys/mman.h>
**
**       int mincore(void *addr, size_t length, unsigned char *vec);
**
**   Feature Test Macro Requirements for glibc (see feature_test_macros(7)):
**
**       mincore(): _BSD_SOURCE || _SVID_SOURCE
**
*/

#include <unistd.h>
#include <sys/mman.h>
#include <unistd.h>
#include "aff.h"
#include "strace.h"

void		print_args_mincore(struct user_regs_struct *regs, pid_t pid)
{
  (void)pid;
  fprintf(stderr, "mincore(");
  fprintf(stderr, "%p, %lu, %p", (void *)regs->rdi, (size_t)regs->rsi,
	  (unsigned char *)regs->rdx);
  fprintf(stderr, ")\n");
}

void		print_ret_mincore(struct user_regs_struct *regs, pid_t pid)
{
  (void)pid;
  fprintf(stderr, "|Ret----mincore--->(");
  print_errno((int)regs->rax);
  fprintf(stderr, ")\n");
}
