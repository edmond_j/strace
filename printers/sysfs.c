/*
** header.c for aff in /home/lequeu_m/
**
** Made by martin lequeux-gruninger
** Login   <lequeu_m@epitech.net>
**
** Started on  Fri May 10 17:38:41 2013 martin lequeux-gruninger
** Last update Tue May 14 08:45:12 2013 julien edmond
*/

/*
**       int sysfs(int option, const char *fsname);
**
**       int sysfs(int option, unsigned int fs_index, char *buf);
**
**       int sysfs(int option);
**
*/

#include <unistd.h>
#include "aff.h"
#include "strace.h"

void		print_args_sysfs(struct user_regs_struct *regs, pid_t pid)
{
  (void)pid;
  fprintf(stderr, "sysfs(");
  fprintf(stderr, "%d", (int)regs->rdi);
  if ((int)regs->rdi == 1)
    fprintf(stderr, "%s", (const char *)regs->rsi);
  else if ((int)regs->rsi == 2)
    fprintf(stderr, "%u, %p", (unsigned)regs->rsi, (void *)regs->rdx);
  fprintf(stderr, ")\n");
}

void		print_ret_sysfs(struct user_regs_struct *regs, pid_t pid)
{
  (void)pid;
  fprintf(stderr, "|Ret----sysfs--->(");
  print_errno((int)regs->rax);
  fprintf(stderr, ")\n");
}
