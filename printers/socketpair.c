/*
** header.c for aff in /home/lequeu_m/
**
** Made by martin lequeux-gruninger
** Login   <lequeu_m@epitech.net>
**
** Started on  Fri May 10 17:38:41 2013 martin lequeux-gruninger
** Last update Tue May 14 10:18:59 2013 julien edmond
*/

/*
**       #include <sys/types.h>           See NOTES
**       #include <sys/socket.h>
**
**       int socketpair(int domain, int type, int protocol, int sv[2]);
**
*/

#include <sys/types.h>
#include <sys/socket.h>
#include <unistd.h>
#include "aff.h"
#include "strace.h"

void		print_args_socketpair(struct user_regs_struct *regs, pid_t pid)
{
  (void)pid;
  fprintf(stderr, "socketpair(");
  fprintf(stderr, "%d, %d, %d, %p", (int)regs->rdi, (int)regs->rsi,
	  (int)regs->rdx, (int *)regs->rcx);
  fprintf(stderr, ")\n");
}

void		print_ret_socketpair(struct user_regs_struct *regs, pid_t pid)
{
  (void)pid;
  fprintf(stderr, "|Ret----socketpair--->(");
  print_errno((int)regs->rax);
  fprintf(stderr, ")\n");
}
