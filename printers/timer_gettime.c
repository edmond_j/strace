/*
** header.c for aff in /home/lequeu_m/
**
** Made by martin lequeux-gruninger
** Login   <lequeu_m@epitech.net>
**
** Started on  Fri May 10 17:38:41 2013 martin lequeux-gruninger
** Last update Tue May 14 08:13:27 2013 julien edmond
*/

/*
**       #include <time.h>
**
**       int timer_settime(timer_t timerid, int flags,
**                         const struct itimerspec *new_value,
**                         struct itimerspec * old_value);
**       int timer_gettime(timer_t timerid, struct itimerspec *curr_value);
**
**       Link with -lrt.
**
**   Feature Test Macro Requirements for glibc (see feature_test_macros(7)):
**
**       timer_settime(), timer_gettime(): _POSIX_C_SOURCE >= 199309L
**
*/

#include <time.h>
#include <unistd.h>
#include "aff.h"
#include "strace.h"

void		print_args_timer_gettime(struct user_regs_struct *regs,
					 pid_t pid)
{
  (void)pid;
  fprintf(stderr, "timer_gettime(");
  fprintf(stderr, "%ld, %p", regs->rdi, (void *)regs->rsi);
  fprintf(stderr, ")\n");
}

void		print_ret_timer_gettime(struct user_regs_struct *regs,
					pid_t pid)
{
  (void)pid;
  fprintf(stderr, "|Ret----timer_gettime--->(");
  print_errno((int)regs->rax);
  fprintf(stderr, ")\n");
}
