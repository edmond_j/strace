/*
** header.c for aff in /home/lequeu_m/
**
** Made by martin lequeux-gruninger
** Login   <lequeu_m@epitech.net>
**
** Started on  Fri May 10 17:38:41 2013 martin lequeux-gruninger
** Last update Wed May 15 13:08:44 2013 martin lequeux-gruninger
*/

/*
**       #include <unistd.h>
**       #include <sys/types.h>
**
**       uid_t getuid(void);
**       uid_t geteuid(void);
**
*/

#include <unistd.h>
#include <sys/types.h>
#include <unistd.h>
#include "aff.h"
#include "strace.h"

void		print_args_geteuid(struct user_regs_struct *regs, pid_t pid)
{
  (void)pid;
  (void)regs;
  fprintf(stderr, "geteuid(");
  fprintf(stderr, ")\n");
}

void		print_ret_geteuid(struct user_regs_struct *regs, pid_t pid)
{
  (void)pid;
  fprintf(stderr, "|Ret----geteuid--->(");
  print_errno((uid_t)regs->rax);
  fprintf(stderr, ")\n");
}
