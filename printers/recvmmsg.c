/*
** header.c for aff in /home/lequeu_m/
**
** Made by martin lequeux-gruninger
** Login   <lequeu_m@epitech.net>
**
** Started on  Fri May 10 17:38:41 2013 martin lequeux-gruninger
** Last update Wed May 15 12:29:25 2013 julien edmond
*/

/*
**       #define _GNU_SOURCE
**       #include <sys/socket.h>
**
**       int recvmmsg(int sockfd, struct mmsghdr *msgvec, unsigned int vlen,
**                    unsigned int flags, struct timespec *timeout);
**
*/

#include <sys/socket.h>
#include <unistd.h>
#include "aff.h"
#include "strace.h"

void		print_args_recvmmsg(struct user_regs_struct *regs, pid_t pid)
{
  (void)pid;
  fprintf(stderr, "recvmmsg(");
  fprintf(stderr, "%d, %p, %u, %u, %p", (int)regs->rdi, (void *)regs->rsi,
	  (unsigned)regs->rdx, (unsigned)regs->rcx, (void *)regs->r8);
  fprintf(stderr, ")\n");
}

void		print_ret_recvmmsg(struct user_regs_struct *regs, pid_t pid)
{
  (void)pid;
  fprintf(stderr, "|Ret----recvmmsg--->(");
  print_errno((int)regs->rax);
  fprintf(stderr, ")\n");
}
