/*
** header.c for aff in /home/lequeu_m/
**
** Made by martin lequeux-gruninger
** Login   <lequeu_m@epitech.net>
**
** Started on  Fri May 10 17:38:41 2013 martin lequeux-gruninger
** Last update Wed May 15 17:51:39 2013 julien edmond
*/

/*
**       #include <numaif.h>
**
**       int mbind(void *addr, unsigned long len, int mode,
**                 unsigned long *nodemask, unsigned long maxnode,
**                 unsigned flags);
**
**       Link with -lnuma.
**
*/

#include <unistd.h>
#include "aff.h"
#include "strace.h"

void		print_args_mbind(struct user_regs_struct *regs, pid_t pid)
{
  (void)pid;
  fprintf(stderr, "mbind(");
  fprintf(stderr, "%p, %lu, %d, %p, %lu, %u", (void *)regs->rdi,
	  (unsigned long)regs->rsi, (int)regs->rdx, (void *)regs->rcx,
	  (unsigned long)regs->r8, (unsigned)regs->r9);
  fprintf(stderr, ")\n");
}

void		print_ret_mbind(struct user_regs_struct *regs, pid_t pid)
{
  (void)pid;
  fprintf(stderr, "|Ret----mbind--->(");
  print_errno((int)regs->rax);
  fprintf(stderr, ")\n");
}
