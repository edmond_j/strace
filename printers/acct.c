/*
** header.c for aff in /home/lequeu_m/
**
** Made by martin lequeux-gruninger
** Login   <lequeu_m@epitech.net>
**
** Started on  Fri May 10 17:38:41 2013 martin lequeux-gruninger
** Last update Tue May 14 12:18:04 2013 martin lequeux-gruninger
*/

/*
**       #include <unistd.h>
**
**       int acct(const char *filename);
**
**   Feature Test Macro Requirements for glibc (see feature_test_macros(7)):
**
**       acct(): _BSD_SOURCE || (_XOPEN_SOURCE && _XOPEN_SOURCE < 500)
**
*/

#include <unistd.h>
#include <unistd.h>
#include "aff.h"
#include "strace.h"

void		print_args_acct(struct user_regs_struct *regs, pid_t pid)
{
  (void)pid;
  fprintf(stderr, "acct(");
  print_string(pid, (void *)regs->rdi);
  fprintf(stderr, ")\n");
}

void		print_ret_acct(struct user_regs_struct *regs, pid_t pid)
{
  (void)pid;
  fprintf(stderr, "|Ret----acct--->(");
  print_errno((int)regs->rax);
  fprintf(stderr, ")\n");
}
