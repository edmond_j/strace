/*
** header.c for aff in /home/lequeu_m/
**
** Made by martin lequeux-gruninger
** Login   <lequeu_m@epitech.net>
**
** Started on  Fri May 10 17:38:41 2013 martin lequeux-gruninger
** Last update Wed May 15 16:59:46 2013 julien edmond
*/

/*
**       #include <sys/types.h>
**       #include <sys/ipc.h>
**       #include <sys/msg.h>
**
**       int msgsnd(int msqid, const void *msgp, size_t msgsz, int msgflg);
**
**       ssize_t msgrcv(int msqid, void *msgp, size_t msgsz, long msgtyp,
**                      int msgflg);
**
*/

#include <sys/types.h>
#include <sys/ipc.h>
#include <sys/msg.h>
#include <unistd.h>
#include "aff.h"
#include "strace.h"

void		print_args_msgsnd(struct user_regs_struct *regs, pid_t pid)
{
  (void)pid;
  fprintf(stderr, "msgsnd(");
  fprintf(stderr, "%d, %p, %lu, %d", (int)regs->rdi, (const void *)regs->rsi,
	  (size_t)regs->rdx, (int)regs->rcx);
  fprintf(stderr, ")\n");
}

void		print_ret_msgsnd(struct user_regs_struct *regs, pid_t pid)
{
  (void)pid;
  fprintf(stderr, "|Ret----msgsnd--->(");
  print_errno((int)regs->rax);
  fprintf(stderr, ")\n");
}
