/*
** header.c for aff in /home/lequeu_m/
**
** Made by martin lequeux-gruninger
** Login   <lequeu_m@epitech.net>
**
** Started on  Fri May 10 17:38:41 2013 martin lequeux-gruninger
** Last update Wed May 15 13:12:36 2013 martin lequeux-gruninger
*/

/*
**       #include <unistd.h>
**       #include <sys/types.h>
**
**       gid_t getgid(void);
**       gid_t getegid(void);
**
*/

#include <unistd.h>
#include <sys/types.h>
#include "aff.h"
#include "strace.h"

void		print_args_getegid(struct user_regs_struct *regs, pid_t pid)
{
  (void)pid;
  (void)regs;
  fprintf(stderr, "getegid(");
  fprintf(stderr, ")\n");
}

void		print_ret_getegid(struct user_regs_struct *regs, pid_t pid)
{
  (void)pid;
  fprintf(stderr, "|Ret----getegid--->(");
  print_errno((gid_t)regs->rax);
  fprintf(stderr, ")\n");
}
