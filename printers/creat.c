/*
** header.c for aff in /home/lequeu_m/
**
** Made by martin lequeux-gruninger
** Login   <lequeu_m@epitech.net>
**
** Started on  Fri May 10 17:38:41 2013 martin lequeux-gruninger
** Last update Thu May 16 10:23:14 2013 martin lequeux-gruninger
*/

/*
**       #include <sys/types.h>
**       #include <sys/stat.h>
**       #include <fcntl.h>
**
**       int open(const char *pathname, int flags);
**       int open(const char *pathname, int flags, mode_t mode);
**
**       int creat(const char *pathname, mode_t mode);
**
*/

#include <sys/types.h>
#include <sys/stat.h>
#include <fcntl.h>
#include <unistd.h>
#include "aff.h"
#include "strace.h"

void		print_args_creat(struct user_regs_struct *regs, pid_t pid)
{
  fprintf(stderr, "creat(");
  print_string(pid, (void *)regs->rdi);
  fprintf(stderr, ", %d", (int)regs->rsi);
  fprintf(stderr, ")\n");
}

void		print_ret_creat(struct user_regs_struct *regs, pid_t pid)
{
  (void)pid;
  fprintf(stderr, "|Ret----creat--->(");
  print_errno((int)regs->rax);
  fprintf(stderr, ")\n");
}
