/*
** header.c for aff in /home/lequeu_m/
**
** Made by martin lequeux-gruninger
** Login   <lequeu_m@epitech.net>
**
** Started on  Fri May 10 17:38:41 2013 martin lequeux-gruninger
** Last update Tue May 14 11:07:53 2013 julien edmond
*/

/*
**       #define _GNU_SOURCE              See feature_test_macros(7)
**       #include <sched.h>
**
**       int unshare(int flags);
**
*/

#include <sched.h>
#include <unistd.h>
#include "aff.h"
#include "strace.h"

void		print_args_set_robust_list(struct user_regs_struct *regs,
					   pid_t pid)
{
  (void)pid;
  fprintf(stderr, "set_robust_list(");
  fprintf(stderr, "%p, %lu", (void *)regs->rdi, (size_t)regs->rsi);
  fprintf(stderr, ")\n");
}

void		print_ret_set_robust_list(struct user_regs_struct *regs,
					  pid_t pid)
{
  (void)pid;
  fprintf(stderr, "|Ret----set_robust_list--->(");
  if ((long)regs->rax < 0)
    print_errno((int)regs->rax);
  else
    fprintf(stderr, "%ld", regs->rax);
  fprintf(stderr, ")\n");
}
