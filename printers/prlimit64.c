/*
** header.c for aff in /home/lequeu_m/
**
** Made by martin lequeux-gruninger
** Login   <lequeu_m@epitech.net>
**
** Started on  Fri May 10 17:38:41 2013 martin lequeux-gruninger
** Last update Wed May 15 13:38:14 2013 julien edmond
*/

#include <sys/socket.h>
#include <unistd.h>
#include "aff.h"
#include "strace.h"

void		print_args_prlimit64(struct user_regs_struct *regs, pid_t pid)
{
  (void)pid;
  fprintf(stderr, "prlimit64(");
  fprintf(stderr, "%d, %d, %p, %p", (pid_t)regs->rdi, (int)regs->rsi,
	  (void *)regs->rdx, (void *)regs->rcx);
  fprintf(stderr, ")\n");
}

void		print_ret_prlimit64(struct user_regs_struct *regs, pid_t pid)
{
  (void)pid;
  fprintf(stderr, "|Ret----prlimit64--->(");
  print_errno((int)regs->rax);
  fprintf(stderr, ")\n");
}
