/*
** header.c for aff in /home/lequeu_m/
**
** Made by martin lequeux-gruninger
** Login   <lequeu_m@epitech.net>
**
** Started on  Fri May 10 17:38:41 2013 martin lequeux-gruninger
** Last update Thu May 16 10:25:43 2013 martin lequeux-gruninger
*/

/*
**       #include <linux/unistd.h>
**
**       void exit_group(int status);
**
*/

#include <linux/unistd.h>
#include <unistd.h>
#include "aff.h"
#include "strace.h"

void		print_args_exit_group(struct user_regs_struct *regs, pid_t pid)
{
  (void)pid;
  fprintf(stderr, "exit_group(");
  fprintf(stderr, "%d", (int)regs->rdi);
  fprintf(stderr, ")\n");
}

void		print_ret_exit_group(struct user_regs_struct *regs, pid_t pid)
{
  (void)pid;
  fprintf(stderr, "|Ret----exit_group--->(");
  print_errno((int)regs->rax);
  fprintf(stderr, ")\n");
}
