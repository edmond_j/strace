/*
** header.c for aff in /home/lequeu_m/
**
** Made by martin lequeux-gruninger
** Login   <lequeu_m@epitech.net>
**
** Started on  Fri May 10 17:38:41 2013 martin lequeux-gruninger
** Last update Thu May 16 09:28:49 2013 julien edmond
*/

/*
**       #include <sys/types.h>
**       #include <unistd.h>
**
**       off_t lseek(int fd, off_t offset, int whence);
**
*/

#include <sys/types.h>
#include <unistd.h>
#include <unistd.h>
#include "aff.h"
#include "strace.h"

void		print_args_lseek(struct user_regs_struct *regs, pid_t pid)
{
  (void)pid;
  fprintf(stderr, "lseek(");
  fprintf(stderr, "%d, 0x%lx, %d", (int)regs->rdi, (off_t)regs->rsi,
	  (int)regs->rdx);
  fprintf(stderr, ")\n");
}

void		print_ret_lseek(struct user_regs_struct *regs, pid_t pid)
{
  (void)pid;
  fprintf(stderr, "|Ret----lseek--->(");
  if ((off_t)regs->rax < 0)
    print_errno((int)regs->rax);
  else
    fprintf(stderr, "0x%lx", regs->rax);
  fprintf(stderr, ")\n");
}
