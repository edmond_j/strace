/*
** header.c for aff in /home/lequeu_m/
**
** Made by martin lequeux-gruninger
** Login   <lequeu_m@epitech.net>
**
** Started on  Fri May 10 17:38:41 2013 martin lequeux-gruninger
** Last update Wed May 15 17:19:22 2013 julien edmond
*/

/*
**       #include <mqueue.h>
**
**       ssize_t mq_receive(mqd_t mqdes, char *msg_ptr,
**                          size_t msg_len, unsigned *msg_prio);
**
**       #include <time.h>
**       #include <mqueue.h>
**
**       ssize_t mq_timedreceive(mqd_t mqdes, char *msg_ptr,
**                          size_t msg_len, unsigned *msg_prio,
**                          const struct timespec *abs_timeout);
**
**       Link with -lrt.
**
**   Feature Test Macro Requirements for glibc (see feature_test_macros(7)):
**
**       mq_timedreceive():
**           _XOPEN_SOURCE >= 600 || _POSIX_C_SOURCE >= 200112L
**
*/

#include <mqueue.h>
#include <time.h>
#include <mqueue.h>
#include <unistd.h>
#include "aff.h"
#include "strace.h"

void		print_args_mq_timedreceive(struct user_regs_struct *regs,
					   pid_t pid)
{
  (void)pid;
  fprintf(stderr, "mq_timedreceive(");
  fprintf(stderr, "%d, %p, %lu, %p, %p", (mqd_t)regs->rdi, (char *)regs->rsi,
	  (size_t)regs->rdx, (unsigned *)regs->rcx, (void *)regs->r8);
  fprintf(stderr, ")\n");
}

void		print_ret_mq_timedreceive(struct user_regs_struct *regs,
					  pid_t pid)
{
  (void)pid;
  fprintf(stderr, "|Ret----mq_timedreceive--->(");
  if ((ssize_t)regs->rax < 0)
    print_errno((int)regs->rax);
  else
    fprintf(stderr, "%ld", (ssize_t)regs->rax);
  fprintf(stderr, ")\n");
}
