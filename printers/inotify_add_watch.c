/*
** header.c for aff in /home/lequeu_m/
**
** Made by martin lequeux-gruninger
** Login   <lequeu_m@epitech.net>
**
** Started on  Fri May 10 17:38:41 2013 martin lequeux-gruninger
** Last update Thu May 16 13:37:39 2013 julien edmond
*/

/*
**       #include <sys/inotify.h>
**
**       int inotify_add_watch(int fd, const char *pathname, uint32_t mask);
**
*/

#include <sys/inotify.h>
#include <unistd.h>
#include "aff.h"
#include "strace.h"

void		print_args_inotify_add_watch(struct user_regs_struct *regs,
					     pid_t pid)
{
  fprintf(stderr, "inotify_add_watch(");
  fprintf(stderr, "%d", (int)regs->rdi);
  fprintf(stderr, ", ");
  print_string(pid, (void *)regs->rsi);
  fprintf(stderr, ", %u", (uint32_t)regs->rdx);
  fprintf(stderr, ")\n");
}

void		print_ret_inotify_add_watch(struct user_regs_struct *regs,
					    pid_t pid)
{
  (void)pid;
  fprintf(stderr, "|Ret----inotify_add_watch--->(");
  print_errno((int)regs->rax);
  fprintf(stderr, ")\n");
}
