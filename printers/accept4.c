/*
** header.c for aff in /home/lequeu_m/
**
** Made by martin lequeux-gruninger
** Login   <lequeu_m@epitech.net>
**
** Started on  Fri May 10 17:38:41 2013 martin lequeux-gruninger
** Last update Tue May 14 12:13:51 2013 martin lequeux-gruninger
*/

/*
**       #include <sys/types.h>           See NOTES
**       #include <sys/socket.h>
**
**       int accept(int sockfd, struct sockaddr *addr, socklen_t *addrlen);
**
**       #define _GNU_SOURCE              See feature_test_macros(7)
**       #include <sys/socket.h>
**
**       int accept4(int sockfd, struct sockaddr *addr,
**                   socklen_t *addrlen, int flags);
**
*/

#include <sys/types.h>
#include <sys/socket.h>
#include <unistd.h>
#include <unistd.h>
#include <string.h>
#include <netinet/in.h>
#include <arpa/inet.h>
#include "aff.h"
#include "strace.h"

void		*get_in_addr2(struct sockaddr *sa)
{
  if (sa->sa_family == AF_INET)
    return &(((struct sockaddr_in*)sa)->sin_addr);
  return &(((struct sockaddr_in6*)sa)->sin6_addr);
}

void		print_args_accept4(struct user_regs_struct *regs, pid_t pid)
{
  unsigned int		addrlen;
  int			flags;
  int			nb;

  flags = (int)regs->rcx;
  nb = 0;
  if (flags | SOCK_NONBLOCK)
    ++nb;
  if (flags | SOCK_CLOEXEC)
    ++nb;
  fprintf(stderr, "accept(");
  read_from_son(pid, (unsigned int *)regs->rdx, &addrlen, sizeof(addrlen));
  fprintf(stderr, "%d", (int)regs->rdi);
  fprintf(stderr, ", %p", (struct sockaddr *)regs->rsi);
  fprintf(stderr, ", %u", addrlen);
  fprintf(stderr, ", {");
  if (nb == 2)
    fprintf(stderr, "SOCK_NONBLOCK | SOCK_CLOEXEC");
  else if (nb == 1)
    fprintf(stderr, ((flags | SOCK_NONBLOCK) ? ("SOCK_NONBLOCK") :
		     ("SOCK_CLOEXEC")));
  else
    fprintf(stderr, "0");
  fprintf(stderr, "})\n");
}

void		print_ret_accept4(struct user_regs_struct *regs, pid_t pid)
{
  struct sockaddr	addr;
  unsigned int		addrlen;
  static char		*families[13] = { "AF_UNSPEC", "AF_UNIX", "AF_INET",
					  "AF_AX25", "AF_IPX", "AF_APPLETALK",
					  "AF_NETROM", "AF_BRIDGE", "AF_AAL5",
					  "AF_X25", "AF_INET6", "AF_MAX",
					  NULL };
  int			i;
  char			client_ip[INET6_ADDRSTRLEN];

  (void)pid;
  fprintf(stderr, "|Ret----accept4--->(");
  print_errno((int)regs->rax);
  read_from_son(pid, (struct sockaddr *)regs->rsi, &addr, sizeof(addr));
  read_from_son(pid, (unsigned int *)regs->rdx, &addrlen, sizeof(addrlen));
  i = addr.sa_family;
  if (inet_ntop(i, get_in_addr2(&addr),
		client_ip, addrlen) == NULL)
    return ;
  fprintf(stderr, ", {%s, %s}", ((i >= 0 && i <= 11) ? (families[i]) :
				 ("UNKNOWN")), client_ip);
  fprintf(stderr, ")\n");
}
