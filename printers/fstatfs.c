/*
** header.c for aff in /home/lequeu_m/
**
** Made by martin lequeux-gruninger
** Login   <lequeu_m@epitech.net>
**
** Started on  Fri May 10 17:38:41 2013 martin lequeux-gruninger
** Last update Thu May 16 10:31:04 2013 martin lequeux-gruninger
*/

/*
**       #include <sys/vfs.h>     or <sys/statfs.h>
**
**       int statfs(const char *path, struct statfs *buf);
**       int fstatfs(int fd, struct statfs *buf);
**
*/

#include <sys/vfs.h>
#include <unistd.h>
#include "aff.h"
#include "strace.h"

void		print_args_fstatfs(struct user_regs_struct *regs, pid_t pid)
{
  (void)pid;
  fprintf(stderr, "fstatfs(");
  fprintf(stderr, "%d", (int)regs->rdi);
  fprintf(stderr, ", %p", (struct statfs *)regs->rsi);
  fprintf(stderr, ")\n");
}

void		print_ret_fstatfs(struct user_regs_struct *regs, pid_t pid)
{
  (void)pid;
  fprintf(stderr, "|Ret----fstatfs--->(");
  print_errno((int)regs->rax);
  fprintf(stderr, ")\n");
}
