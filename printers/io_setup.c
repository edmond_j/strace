/*
** header.c for aff in /home/lequeu_m/
**
** Made by martin lequeux-gruninger
** Login   <lequeu_m@epitech.net>
**
** Started on  Fri May 10 17:38:41 2013 martin lequeux-gruninger
** Last update Thu May 16 12:04:11 2013 martin lequeux-gruninger
*/

/*
**       #include <libaio.h>
**
**       int io_setup(unsigned nr_events, aio_context_t *ctxp);
**
**       Link with -laio.
**
*/

#include <unistd.h>
#include "aff.h"
#include "strace.h"

void		print_args_io_setup(struct user_regs_struct *regs, pid_t pid)
{
  (void)pid;
  fprintf(stderr, "io_setup(");
  fprintf(stderr, "%u", (unsigned)regs->rdi);
  fprintf(stderr, ", %p", (long unsigned *)regs->rsi);
  fprintf(stderr, ")\n");
}

void		print_ret_io_setup(struct user_regs_struct *regs, pid_t pid)
{
  (void)pid;
  fprintf(stderr, "|Ret----io_setup--->(");
  print_errno((int)regs->rax);
  fprintf(stderr, ")\n");
}
