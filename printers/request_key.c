/*
** header.c for aff in /home/lequeu_m/
**
** Made by martin lequeux-gruninger
** Login   <lequeu_m@epitech.net>
**
** Started on  Fri May 10 17:38:41 2013 martin lequeux-gruninger
** Last update Wed May 15 12:16:03 2013 julien edmond
*/

/*
**       #include <keyutils.h>
**
**       key_serial_t request_key(const char *type, const char *description,
**       const char *callout_info, key_serial_t keyring);
**
*/

#include <unistd.h>
#include <keyutils.h>
#include "aff.h"
#include "strace.h"

void		print_args_request_key(struct user_regs_struct *regs,
				       pid_t pid)
{
  fprintf(stderr, "request_key(");
  print_string(pid, (void *)regs->rdi);
  fputs(", ", stderr);
  print_string(pid, (void *)regs->rsi);
  fputs(", ", stderr);
  print_string(pid, (void *)regs->rdx);
  fputs(", ", stderr);
  fprintf(stderr, "%d", (key_serial_t)regs->rcx);
  fprintf(stderr, ")\n");
}

void		print_ret_request_key(struct user_regs_struct *regs, pid_t pid)
{
  (void)pid;
  fprintf(stderr, "|Ret----request_key--->(");
  print_errno((key_serial_t)regs->rax);
  fprintf(stderr, ")\n");
}
