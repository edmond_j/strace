/*
** header.c for aff in /home/lequeu_m/
**
** Made by martin lequeux-gruninger
** Login   <lequeu_m@epitech.net>
**
** Started on  Fri May 10 17:38:41 2013 martin lequeux-gruninger
** Last update Wed May 15 13:39:19 2013 julien edmond
*/

/*
**       #include <sys/uio.h>
**
**       ssize_t readv(int fd, const struct iovec *iov, int iovcnt);
**
**       ssize_t writev(int fd, const struct iovec *iov, int iovcnt);
**
**       ssize_t preadv(int fd, const struct iovec *iov, int iovcnt,
**                      off_t offset);
**
**       ssize_t pwritev(int fd, const struct iovec *iov, int iovcnt,
**                       off_t offset);
**
**   Feature Test Macro Requirements for glibc (see feature_test_macros(7)):
**
**       preadv(), pwritev(): _BSD_SOURCE
**
*/

#include <sys/uio.h>
#include <unistd.h>
#include "aff.h"
#include "strace.h"

void		print_args_preadv(struct user_regs_struct *regs, pid_t pid)
{
  (void)pid;
  fprintf(stderr, "preadv(");
  fprintf(stderr, "%d, %p, %d, 0x%lx", (int)regs->rdi, (void *)regs->rsi,
	  (int)regs->rdx, (off_t)regs->rcx);
  fprintf(stderr, ")\n");
}

void		print_ret_preadv(struct user_regs_struct *regs, pid_t pid)
{
  (void)pid;
  fprintf(stderr, "|Ret----preadv--->(");
  if ((ssize_t)regs->rax < 0)
    print_errno((int)regs->rax);
  else
    fprintf(stderr, "%ld", regs->rax);
  fprintf(stderr, ")\n");
}
