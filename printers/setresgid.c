/*
** header.c for aff in /home/lequeu_m/
**
** Made by martin lequeux-gruninger
** Login   <lequeu_m@epitech.net>
**
** Started on  Fri May 10 17:38:41 2013 martin lequeux-gruninger
** Last update Tue May 14 11:19:55 2013 julien edmond
*/

/*
**       #define _GNU_SOURCE          See feature_test_macros(7)
**       #include <unistd.h>
**
**       int setresuid(uid_t ruid, uid_t euid, uid_t suid);
**       int setresgid(gid_t rgid, gid_t egid, gid_t sgid);
**
*/

#include <unistd.h>
#include <unistd.h>
#include "aff.h"
#include "strace.h"

void		print_args_setresgid(struct user_regs_struct *regs, pid_t pid)
{
  (void)pid;
  fprintf(stderr, "setresgid(");
  fprintf(stderr, "%d, %d, %d", (gid_t)regs->rdi, (gid_t)regs->rsi,
	  (gid_t)regs->rdx);
  fprintf(stderr, ")\n");
}

void		print_ret_setresgid(struct user_regs_struct *regs, pid_t pid)
{
  (void)pid;
  fprintf(stderr, "|Ret----setresgid--->(");
  print_errno((int)regs->rax);
  fprintf(stderr, ")\n");
}
