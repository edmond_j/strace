/*
** header.c for aff in /home/lequeu_m/
**
** Made by martin lequeux-gruninger
** Login   <lequeu_m@epitech.net>
**
** Started on  Fri May 10 17:38:41 2013 martin lequeux-gruninger
** Last update Wed May 15 12:54:38 2013 martin lequeux-gruninger
*/

/*
**       #include <sys/types.h>
**
**       pid_t gettid(void);
**
*/

#include <sys/types.h>
#include <unistd.h>
#include "aff.h"
#include "strace.h"

void		print_args_gettid(struct user_regs_struct *regs, pid_t pid)
{
  (void)pid;
  (void)regs;
  fprintf(stderr, "gettid(");
  fprintf(stderr, ")\n");
}

void		print_ret_gettid(struct user_regs_struct *regs, pid_t pid)
{
  (void)pid;
  fprintf(stderr, "|Ret----gettid--->(");
  print_errno((pid_t)regs->rax);
  fprintf(stderr, ")\n");
}
