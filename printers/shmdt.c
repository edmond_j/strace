/*
** header.c for aff in /home/lequeu_m/
**
** Made by martin lequeux-gruninger
** Login   <lequeu_m@epitech.net>
**
** Started on  Fri May 10 17:38:41 2013 martin lequeux-gruninger
** Last update Tue May 14 10:30:22 2013 julien edmond
*/

/*
**       #include <sys/types.h>
**       #include <sys/shm.h>
**
**       void *shmat(int shmid, const void *shmaddr, int shmflg);
**
**       int shmdt(const void *shmaddr);
**
*/

#include <sys/types.h>
#include <sys/shm.h>
#include <unistd.h>
#include "aff.h"
#include "strace.h"

void		print_args_shmdt(struct user_regs_struct *regs, pid_t pid)
{
  (void)pid;
  fprintf(stderr, "shmdt(");
  fprintf(stderr, "%p", (void *)regs->rdi);
  fprintf(stderr, ")\n");
}

void		print_ret_shmdt(struct user_regs_struct *regs, pid_t pid)
{
  (void)pid;
  fprintf(stderr, "|Ret----shmdt--->(");
  print_errno((int)regs->rax);
  fprintf(stderr, ")\n");
}
