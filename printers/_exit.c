/*
** header.c for aff in /home/lequeu_m/
**
** Made by martin lequeux-gruninger
** Login   <lequeu_m@epitech.net>
**
** Started on  Fri May 10 17:38:41 2013 martin lequeux-gruninger
** Last update Thu May 16 10:25:37 2013 martin lequeux-gruninger
*/

/*
**       #include <unistd.h>
**
**       void _exit(int status);
**
**       #include <stdlib.h>
**
**       void _Exit(int status);
**
**   Feature Test Macro Requirements for glibc (see feature_test_macros(7)):
**
**       _Exit():
**           _XOPEN_SOURCE >= 600 || _ISOC99_SOURCE ||
**           _POSIX_C_SOURCE >= 200112L;
**           or cc -std=c99
**
*/

#include <unistd.h>
#include <stdlib.h>
#include <unistd.h>
#include "aff.h"
#include "strace.h"

void		print_args__exit(struct user_regs_struct *regs, pid_t pid)
{
  (void)pid;
  fprintf(stderr, "_exit(");
  fprintf(stderr, "%d", (int)regs->rdi);
  fprintf(stderr, ")\n");
}

void		print_ret__exit(struct user_regs_struct *regs, pid_t pid)
{
  (void)pid;
  fprintf(stderr, "|Ret----_exit--->(");
  print_errno((int)regs->rax);
  fprintf(stderr, ")\n");
}
