/*
** header.c for aff in /home/lequeu_m/
**
** Made by martin lequeux-gruninger
** Login   <lequeu_m@epitech.net>
**
** Started on  Fri May 10 17:38:41 2013 martin lequeux-gruninger
** Last update Thu May 16 10:30:13 2013 martin lequeux-gruninger
*/

/*
**       #include <sys/types.h>
**       #include <attr/xattr.h>
**
**       ssize_t getxattr(const char *path, const char *name,
**                        void *value, size_t size);
**       ssize_t lgetxattr(const char *path, const char *name,
**                        void *value, size_t size);
**       ssize_t fgetxattr(int fd, const char *name,
**                        void *value, size_t size);
**
*/

#include <sys/types.h>
#include <unistd.h>
#include "aff.h"
#include "strace.h"

void		print_args_fgetxattr(struct user_regs_struct *regs, pid_t pid)
{
  fprintf(stderr, "fgetxattr(");
  print_string(pid, (void *)regs->rdi);
  fprintf(stderr, ", ");
  print_string(pid, (void *)regs->rsi);
  fprintf(stderr, ", %p", (void *)regs->rdx);
  fprintf(stderr, ",  %lu", (size_t)regs->rcx);
  fprintf(stderr, ")\n");
}

void		print_ret_fgetxattr(struct user_regs_struct *regs, pid_t pid)
{
  (void)pid;
  fprintf(stderr, "|Ret----fgetxattr--->(");
  print_errno((int)regs->rax);
  fprintf(stderr, ")\n");
}
