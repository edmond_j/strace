/*
** header.c for aff in /home/lequeu_m/
**
** Made by martin lequeux-gruninger
** Login   <lequeu_m@epitech.net>
**
** Started on  Fri May 10 17:38:41 2013 martin lequeux-gruninger
** Last update Thu May 16 10:33:07 2013 martin lequeux-gruninger
*/

/*
**       #include <unistd.h>
**
**       pid_t getsid(pid_t pid);
**
**   Feature Test Macro Requirements for glibc (see feature_test_macros(7)):
**
**       getsid():
**           _XOPEN_SOURCE >= 500 || _XOPEN_SOURCE && _XOPEN_SOURCE_EXTENDED
**           ||  Since glibc 2.12:  _POSIX_C_SOURCE >= 200809L
**
*/

#include <unistd.h>
#include <unistd.h>
#include "aff.h"
#include "strace.h"

void		print_args_getsid(struct user_regs_struct *regs, pid_t pid)
{
  (void)pid;
  fprintf(stderr, "getsid(");
  fprintf(stderr, ", %d", (pid_t)regs->rdi);
  fprintf(stderr, ")\n");
}

void		print_ret_getsid(struct user_regs_struct *regs, pid_t pid)
{
  (void)pid;
  fprintf(stderr, "|Ret----getsid--->(");
  print_errno((int)regs->rax);
  fprintf(stderr, ")\n");
}
