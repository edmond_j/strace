/*
** header.c for aff in /home/lequeu_m/
**
** Made by martin lequeux-gruninger
** Login   <lequeu_m@epitech.net>
**
** Started on  Fri May 10 17:38:41 2013 martin lequeux-gruninger
** Last update Tue May 14 08:41:16 2013 julien edmond
*/

/*
**       int syslog(int type, char *bufp, int len);
**                        No wrapper provided in glibc
**
**        The glibc interface
**       #include <sys/klog.h>
**
**       int klogctl(int type, char *bufp, int len);
**
*/

#include <sys/klog.h>
#include <unistd.h>
#include "aff.h"
#include "strace.h"

void		print_args_syslog(struct user_regs_struct *regs, pid_t pid)
{
  (void)pid;
  fprintf(stderr, "syslog(");
  fprintf(stderr, "%d, %p, %d", (int)regs->rdi, (char *)regs->rsi,
	  (int)regs->rdx);
  fprintf(stderr, ")\n");
}

void		print_ret_syslog(struct user_regs_struct *regs, pid_t pid)
{
  (void)pid;
  fprintf(stderr, "|Ret----syslog--->(");
  print_errno((int)regs->rax);
  fprintf(stderr, ")\n");
}
