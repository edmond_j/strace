/*
** header.c for aff in /home/lequeu_m/
**
** Made by martin lequeux-gruninger
** Login   <lequeu_m@epitech.net>
**
** Started on  Fri May 10 17:38:41 2013 martin lequeux-gruninger
** Last update Thu May 16 11:48:11 2013 julien edmond
*/

/*
**       #include <sys/ioctl.h>
**
**       int ioctl(int d, int request, ...);
**
*/

#include <sys/ioctl.h>
#include <unistd.h>
#include "aff.h"
#include "strace.h"

void		print_args_ioctl(struct user_regs_struct *regs, pid_t pid)
{
  (void)pid;
  fprintf(stderr, "ioctl(");
  fprintf(stderr, "%d, %d, ...", (int)regs->rdi, (int)regs->rsi);
  fprintf(stderr, ")\n");
}

void		print_ret_ioctl(struct user_regs_struct *regs, pid_t pid)
{
  (void)pid;
  fprintf(stderr, "|Ret----ioctl--->(");
  print_errno((int)regs->rax);
  fprintf(stderr, ")\n");
}
