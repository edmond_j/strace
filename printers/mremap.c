/*
** header.c for aff in /home/lequeu_m/
**
** Made by martin lequeux-gruninger
** Login   <lequeu_m@epitech.net>
**
** Started on  Fri May 10 17:38:41 2013 martin lequeux-gruninger
** Last update Wed May 15 17:07:31 2013 julien edmond
*/

/*
**       #define _GNU_SOURCE          See feature_test_macros(7)
**       #include <sys/mman.h>
**
**       void *mremap(void *old_address, size_t old_size,
**                    size_t new_size, int flags, ...  void *new_address );
**
*/

#include <sys/mman.h>
#include <unistd.h>
#include "aff.h"
#include "strace.h"

void		print_args_mremap(struct user_regs_struct *regs, pid_t pid)
{
  (void)pid;
  fprintf(stderr, "mremap(");
  fprintf(stderr, "%p, %lu, %lu, %d", (void *)regs->rdi, (size_t)regs->rsi,
	  (size_t)regs->rdx, (int)regs->rcx);
  fprintf(stderr, ")\n");
}

void		print_ret_mremap(struct user_regs_struct *regs, pid_t pid)
{
  (void)pid;
  fprintf(stderr, "|Ret----mremap--->(");
  if ((long)regs->rax < 0)
    print_errno((int)regs->rax);
  else
    fprintf(stderr, "%p", (void *)regs->rax);
  fprintf(stderr, ")\n");
}
