/*
** header.c for aff in /home/lequeu_m/
**
** Made by martin lequeux-gruninger
** Login   <lequeu_m@epitech.net>
**
** Started on  Fri May 10 17:38:41 2013 martin lequeux-gruninger
** Last update Mon May 13 18:09:55 2013 julien edmond
*/

/*
**       Unimplemented system calls.
**
*/

#include <unistd.h>
#include "aff.h"
#include "strace.h"

void		print_args_tuxcall(struct user_regs_struct *regs, pid_t pid)
{
  (void)pid;
  (void)regs;
  fprintf(stderr, "tuxcall(");
  fprintf(stderr, ")\n");
}

void		print_ret_tuxcall(struct user_regs_struct *regs, pid_t pid)
{
  (void)pid;
  fprintf(stderr, "|Ret----tuxcall--->(");
  print_errno((int)regs->rax);
  fprintf(stderr, ")\n");
}
