/*
** header.c for aff in /home/lequeu_m/
**
** Made by martin lequeux-gruninger
** Login   <lequeu_m@epitech.net>
**
** Started on  Fri May 10 17:38:41 2013 martin lequeux-gruninger
** Last update Tue May 14 11:48:50 2013 julien edmond
*/

/*
**       #include <sys/types.h>
**       #include <unistd.h>
**
**       int setgid(gid_t gid);
**
*/

#include <sys/types.h>
#include <unistd.h>
#include <unistd.h>
#include "aff.h"
#include "strace.h"

void		print_args_setgid(struct user_regs_struct *regs, pid_t pid)
{
  (void)pid;
  fprintf(stderr, "setgid(");
  fprintf(stderr, "%d", (gid_t)regs->rdi);
  fprintf(stderr, ")\n");
}

void		print_ret_setgid(struct user_regs_struct *regs, pid_t pid)
{
  (void)pid;
  fprintf(stderr, "|Ret----setgid--->(");
  print_errno((int)regs->rax);
  fprintf(stderr, ")\n");
}
