/*
** header.c for aff in /home/lequeu_m/
**
** Made by martin lequeux-gruninger
** Login   <lequeu_m@epitech.net>
**
** Started on  Fri May 10 17:38:41 2013 martin lequeux-gruninger
** Last update Tue May 14 11:03:51 2013 julien edmond
*/

/*
**       #include <unistd.h>
**
**       pid_t setsid(void);
**
*/

#include <unistd.h>
#include <unistd.h>
#include "aff.h"
#include "strace.h"

void		print_args_setsid(struct user_regs_struct *regs, pid_t pid)
{
  (void)pid;
  (void)regs;
  fprintf(stderr, "setsid(");
  fprintf(stderr, ")\n");
}

void		print_ret_setsid(struct user_regs_struct *regs, pid_t pid)
{
  (void)pid;
  fprintf(stderr, "|Ret----setsid--->(");
  print_errno((int)regs->rax);
  fprintf(stderr, ")\n");
}
