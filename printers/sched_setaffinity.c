/*
** header.c for aff in /home/lequeu_m/
**
** Made by martin lequeux-gruninger
** Login   <lequeu_m@epitech.net>
**
** Started on  Fri May 10 17:38:41 2013 martin lequeux-gruninger
** Last update Tue May 14 15:21:16 2013 julien edmond
*/

/*
**       #define _GNU_SOURCE              See feature_test_macros(7)
**       #include <sched.h>
**
**       int sched_setaffinity(pid_t pid, size_t cpusetsize,
**                             cpu_set_t *mask);
**
**       int sched_getaffinity(pid_t pid, size_t cpusetsize,
**                             cpu_set_t *mask);
**
*/

#include <sched.h>
#include <unistd.h>
#include "aff.h"
#include "strace.h"

void		print_args_sched_setaffinity(struct user_regs_struct *regs,
					     pid_t pid)
{
  (void)pid;
  fprintf(stderr, "sched_setaffinity(");
  fprintf(stderr, "%d, %lu, %p", (pid_t)regs->rdi, (size_t)regs->rsi,
	  (cpu_set_t *)regs->rdx);
  fprintf(stderr, ")\n");
}

void		print_ret_sched_setaffinity(struct user_regs_struct *regs,
					    pid_t pid)
{
  (void)pid;
  fprintf(stderr, "|Ret----sched_setaffinity--->(");
  print_errno((int)regs->rax);
  fprintf(stderr, ")\n");
}
