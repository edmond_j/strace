/*
** header.c for aff in /home/lequeu_m/
**
** Made by martin lequeux-gruninger
** Login   <lequeu_m@epitech.net>
**
** Started on  Fri May 10 17:38:41 2013 martin lequeux-gruninger
** Last update Tue May 14 11:49:26 2013 julien edmond
*/

/*
**       #include <unistd.h>  glibc uses <sys/fsuid.h>
**
**       int setfsuid(uid_t fsuid);
**
*/

#include <unistd.h>
#include <unistd.h>
#include "aff.h"
#include "strace.h"

void		print_args_setfsuid(struct user_regs_struct *regs, pid_t pid)
{
  (void)pid;
  fprintf(stderr, "setfsuid(");
  fprintf(stderr, "%d", (uid_t)regs->rdi);
  fprintf(stderr, ")\n");
}

void		print_ret_setfsuid(struct user_regs_struct *regs, pid_t pid)
{
  (void)pid;
  fprintf(stderr, "|Ret----setfsuid--->(");
  print_errno((int)regs->rax);
  fprintf(stderr, ")\n");
}
