/*
** header.c for aff in /home/lequeu_m/
**
** Made by martin lequeux-gruninger
** Login   <lequeu_m@epitech.net>
**
** Started on  Fri May 10 17:38:41 2013 martin lequeux-gruninger
** Last update Thu May 16 09:39:19 2013 julien edmond
*/

/*
**       #include <sys/types.h>
**       #include <attr/xattr.h>
**
**       ssize_t getxattr(const char *path, const char *name,
**                        void *value, size_t size);
**       ssize_t lgetxattr(const char *path, const char *name,
**                        void *value, size_t size);
**       ssize_t fgetxattr(int fd, const char *name,
**                        void *value, size_t size);
**
*/

#include <sys/types.h>
#include <unistd.h>
#include "aff.h"
#include "strace.h"

void		print_args_lgetxattr(struct user_regs_struct *regs, pid_t pid)
{
  fprintf(stderr, "lgetxattr(");
  print_string(pid, (void *)regs->rdi);
  fputs(", ", stderr);
  print_string(pid, (void *)regs->rsi);
  fprintf(stderr, ", %p, %lu", (void *)regs->rdx, (size_t)regs->rcx);
  fprintf(stderr, ")\n");
}

void		print_ret_lgetxattr(struct user_regs_struct *regs, pid_t pid)
{
  (void)pid;
  fprintf(stderr, "|Ret----lgetxattr--->(");
  if ((ssize_t)regs->rax < 0)
    print_errno((int)regs->rax);
  else
    fprintf(stderr, "%ld", (ssize_t)regs->rax);
  fprintf(stderr, ")\n");
}
