/*
** header.c for aff in /home/lequeu_m/
**
** Made by martin lequeux-gruninger
** Login   <lequeu_m@epitech.net>
**
** Started on  Fri May 10 17:38:41 2013 martin lequeux-gruninger
** Last update Wed May 15 13:45:46 2013 julien edmond
*/

/*
**       #include <unistd.h>
**
**       int pipe(int pipefd[2]);
**
**       #define _GNU_SOURCE              See feature_test_macros(7)
**       #include <unistd.h>
**
**       int pipe2(int pipefd[2], int flags);
**
*/

#include <unistd.h>
#include <unistd.h>
#include <unistd.h>
#include "aff.h"
#include "strace.h"

void		print_args_pipe2(struct user_regs_struct *regs, pid_t pid)
{
  (void)pid;
  fprintf(stderr, "pipe2(");
  fprintf(stderr, "%p, %d", (int *)regs->rdi, (int)regs->rsi);
  fprintf(stderr, ")\n");
}

void		print_ret_pipe2(struct user_regs_struct *regs, pid_t pid)
{
  (void)pid;
  fprintf(stderr, "|Ret----pipe2--->(");
  print_errno((int)regs->rax);
  fprintf(stderr, ")\n");
}
