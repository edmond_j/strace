/*
** header.c for aff in /home/lequeu_m/
**
** Made by martin lequeux-gruninger
** Login   <lequeu_m@epitech.net>
**
** Started on  Fri May 10 17:38:41 2013 martin lequeux-gruninger
** Last update Wed May 15 12:22:06 2013 martin lequeux-gruninger
*/

/*
**       #include <sys/types.h>
**       #include <attr/xattr.h>
**
**       int setxattr(const char *path, const char *name,
**                     const void *value, size_t size, int flags);
**       int lsetxattr(const char *path, const char *name,
**                     const void *value, size_t size, int flags);
**       int fsetxattr(int fd, const char *name,
**                     const void *value, size_t size, int flags);
**
*/

#include <sys/types.h>
#include <unistd.h>
#include "aff.h"
#include "strace.h"

void		print_args_setxattr(struct user_regs_struct *regs, pid_t pid)
{
  (void)pid;
  fprintf(stderr, "setxattr(");
  print_string(pid, (void *)regs->rdi);
  fputs(", ", stderr);
  print_string(pid, (void *)regs->rsi);
  fprintf(stderr, ", %p, ", (void *)regs->rdx);
  print_string(pid, (void *)regs->rcx);
  fprintf(stderr, ")\n");
}

void		print_ret_setxattr(struct user_regs_struct *regs, pid_t pid)
{
  (void)pid;
  fprintf(stderr, "|Ret----setxattr--->(");
  print_errno((int)regs->rax);
  fprintf(stderr, ")\n");
}
