/*
** header.c for aff in /home/lequeu_m/
**
** Made by martin lequeux-gruninger
** Login   <lequeu_m@epitech.net>
**
** Started on  Fri May 10 17:38:41 2013 martin lequeux-gruninger
** Last update Wed May 15 17:40:59 2013 julien edmond
*/

/*
**       #include <numaif.h>
**
**       long migrate_pages(int pid, unsigned long maxnode,
**                          const unsigned long *old_nodes,
**                          const unsigned long *new_nodes);
**
**       Link with -lnuma.
**
*/

#include <unistd.h>
#include "aff.h"
#include "strace.h"

void		print_args_migrate_pages(struct user_regs_struct *regs,
					 pid_t pid)
{
  (void)pid;
  fprintf(stderr, "migrate_pages(");
  fprintf(stderr, "%d, %lu, %p, %p", (int)regs->rdi, (unsigned long)regs->rsi,
	  (const unsigned long *)regs->rdx, (const unsigned long *)regs->rcx);
  fprintf(stderr, ")\n");
}

void		print_ret_migrate_pages(struct user_regs_struct *regs,
					pid_t pid)
{
  (void)pid;
  fprintf(stderr, "|Ret----migrate_pages--->(");
  if ((long)regs->rax < 0)
    print_errno((int)regs->rax);
  else
    fprintf(stderr, "%ld", (long)regs->rax);
  fprintf(stderr, ")\n");
}
