/*
** header.c for aff in /home/lequeu_m/
**
** Made by martin lequeux-gruninger
** Login   <lequeu_m@epitech.net>
**
** Started on  Fri May 10 17:38:41 2013 martin lequeux-gruninger
** Last update Thu May 16 10:22:51 2013 martin lequeux-gruninger
*/

/*
**       #define _GNU_SOURCE              See feature_test_macros(7)
**       #include <sched.h>
**
**       int clone(int (*fn)(void *), void *child_stack,
**                 int flags, void *arg, ...
**                  pid_t *ptid, struct user_desc *tls, pid_t *ctid  );
**
*/

#include <sched.h>
#include <unistd.h>
#include "aff.h"
#include "strace.h"

void		print_args_clone(struct user_regs_struct *regs, pid_t pid)
{
  (void)pid;
  fprintf(stderr, "clone(");
  fprintf(stderr, "%p", (int (*)(void *))regs->rdi);
  fprintf(stderr, ", %p", (void *)regs->rsi);
  fprintf(stderr, ", %d", (int)regs->rdx);
  fprintf(stderr, ", %p", (void *)regs->rcx);
  fprintf(stderr, ", ...");
  fprintf(stderr, ")\n");
}

void		print_ret_clone(struct user_regs_struct *regs, pid_t pid)
{
  (void)pid;
  fprintf(stderr, "|Ret----clone--->(");
  print_errno((int)regs->rax);
  fprintf(stderr, ")\n");
}
