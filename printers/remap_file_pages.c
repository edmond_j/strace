/*
** header.c for aff in /home/lequeu_m/
**
** Made by martin lequeux-gruninger
** Login   <lequeu_m@epitech.net>
**
** Started on  Fri May 10 17:38:41 2013 martin lequeux-gruninger
** Last update Wed May 15 12:24:23 2013 julien edmond
*/

/*
**       #define _GNU_SOURCE          See feature_test_macros(7)
**       #include <sys/mman.h>
**
**       int remap_file_pages(void *addr, size_t size, int prot,
**                            ssize_t pgoff, int flags);
**
*/

#include <sys/mman.h>
#include <unistd.h>
#include "aff.h"
#include "strace.h"

void		print_args_remap_file_pages(struct user_regs_struct *regs,
					    pid_t pid)
{
  (void)pid;
  fprintf(stderr, "remap_file_pages(");
  fprintf(stderr, "%p, %lu, %d, %ld, %d", (void *)regs->rdi, (size_t)regs->rsi,
	  (int)regs->rdx, (ssize_t)regs->rcx, (int)regs->r8);
  fprintf(stderr, ")\n");
}

void		print_ret_remap_file_pages(struct user_regs_struct *regs,
					   pid_t pid)
{
  (void)pid;
  fprintf(stderr, "|Ret----remap_file_pages--->(");
  print_errno((int)regs->rax);
  fprintf(stderr, ")\n");
}
