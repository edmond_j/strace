/*
** header.c for aff in /home/lequeu_m/
**
** Made by martin lequeux-gruninger
** Login   <lequeu_m@epitech.net>
**
** Started on  Fri May 10 17:38:41 2013 martin lequeux-gruninger
** Last update Wed May 15 17:54:00 2013 julien edmond
*/

/*
**       #include <sys/mman.h>
**
**       int madvise(void *addr, size_t length, int advice);
**
**   Feature Test Macro Requirements for glibc (see feature_test_macros(7)):
**
**       madvise(): _BSD_SOURCE
**
*/

#include <sys/mman.h>
#include <unistd.h>
#include "aff.h"
#include "strace.h"

void		print_args_madvise(struct user_regs_struct *regs, pid_t pid)
{
  (void)pid;
  fprintf(stderr, "madvise(");
  fprintf(stderr, "%p, %lu, %d", (void *)regs->rdi, (size_t)regs->rsi,
	  (int)regs->rdx);
  fprintf(stderr, ")\n");
}

void		print_ret_madvise(struct user_regs_struct *regs, pid_t pid)
{
  (void)pid;
  fprintf(stderr, "|Ret----madvise--->(");
  print_errno((int)regs->rax);
  fprintf(stderr, ")\n");
}
