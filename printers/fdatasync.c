/*
** header.c for aff in /home/lequeu_m/
**
** Made by martin lequeux-gruninger
** Login   <lequeu_m@epitech.net>
**
** Started on  Fri May 10 17:38:41 2013 martin lequeux-gruninger
** Last update Thu May 16 10:28:53 2013 martin lequeux-gruninger
*/

/*
**       #include <unistd.h>
**
**       int fsync(int fd);
**
**       int fdatasync(int fd);
**
**   Feature Test Macro Requirements for glibc (see feature_test_macros(7)):
**
**       fsync(): _BSD_SOURCE || _XOPEN_SOURCE
**                ||  since glibc 2.8:  _POSIX_C_SOURCE >= 200112L
**       fdatasync(): _POSIX_C_SOURCE >= 199309L || _XOPEN_SOURCE >= 500
**
*/

#include <unistd.h>
#include <unistd.h>
#include "aff.h"
#include "strace.h"

void		print_args_fdatasync(struct user_regs_struct *regs, pid_t pid)
{
  (void)pid;
  fprintf(stderr, "fdatasync(");
  fprintf(stderr, "%d", (int)regs->rdi);
  fprintf(stderr, ")\n");
}

void		print_ret_fdatasync(struct user_regs_struct *regs, pid_t pid)
{
  (void)pid;
  fprintf(stderr, "|Ret----fdatasync--->(");
  print_errno((int)regs->rax);
  fprintf(stderr, ")\n");
}
