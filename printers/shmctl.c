/*
** header.c for aff in /home/lequeu_m/
**
** Made by martin lequeux-gruninger
** Login   <lequeu_m@epitech.net>
**
** Started on  Fri May 10 17:38:41 2013 martin lequeux-gruninger
** Last update Tue May 14 10:31:46 2013 julien edmond
*/

/*
**       #include <sys/ipc.h>
**       #include <sys/shm.h>
**
**       int shmctl(int shmid, int cmd, struct shmid_ds *buf);
**
*/

#include <sys/ipc.h>
#include <sys/shm.h>
#include <unistd.h>
#include "aff.h"
#include "strace.h"

void		print_args_shmctl(struct user_regs_struct *regs, pid_t pid)
{
  (void)pid;
  fprintf(stderr, "shmctl(");
  fprintf(stderr, "%d, %d, %p", (int)regs->rdi, (int)regs->rsi,
	  (void *)regs->rdx);
  fprintf(stderr, ")\n");
}

void		print_ret_shmctl(struct user_regs_struct *regs, pid_t pid)
{
  (void)pid;
  fprintf(stderr, "|Ret----shmctl--->(");
  print_errno((int)regs->rax);
  fprintf(stderr, ")\n");
}
