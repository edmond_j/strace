/*
** header.c for aff in /home/lequeu_m/
**
** Made by martin lequeux-gruninger
** Login   <lequeu_m@epitech.net>
**
** Started on  Fri May 10 17:38:41 2013 martin lequeux-gruninger
** Last update Thu May 16 10:33:02 2013 martin lequeux-gruninger
*/

/*
**       #include <sys/time.h>
**       #include <sys/resource.h>
**
**       int getrusage(int who, struct rusage *usage);
**
*/

#include <sys/time.h>
#include <sys/resource.h>
#include <unistd.h>
#include "aff.h"
#include "strace.h"

void		print_args_getrusage(struct user_regs_struct *regs, pid_t pid)
{
  (void)pid;
  fprintf(stderr, "getrusage(");
  fprintf(stderr, "%d", (int)regs->rdi);
  fprintf(stderr, ", %p", (struct rusage *)regs->rsi);
  fprintf(stderr, ")\n");
}

void		print_ret_getrusage(struct user_regs_struct *regs, pid_t pid)
{
  (void)pid;
  fprintf(stderr, "|Ret----getrusage--->(");
  print_errno((int)regs->rax);
  fprintf(stderr, ")\n");
}
