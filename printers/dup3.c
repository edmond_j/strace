/*
** header.c for aff in /home/lequeu_m/
**
** Made by martin lequeux-gruninger
** Login   <lequeu_m@epitech.net>
**
** Started on  Fri May 10 17:38:41 2013 martin lequeux-gruninger
** Last update Thu May 16 10:23:47 2013 martin lequeux-gruninger
*/

/*
**       #include <unistd.h>
**
**       int dup(int oldfd);
**       int dup2(int oldfd, int newfd);
**
**       #define _GNU_SOURCE              See feature_test_macros(7)
**       #include <unistd.h>
**
**       int dup3(int oldfd, int newfd, int flags);
**
*/

#include <unistd.h>
#include <unistd.h>
#include <unistd.h>
#include "aff.h"
#include "strace.h"

void		print_args_dup3(struct user_regs_struct *regs, pid_t pid)
{
  (void)pid;
  fprintf(stderr, "dup3(");
  fprintf(stderr, "%d", (int)regs->rdi);
  fprintf(stderr, ", %d", (int)regs->rsi);
  fprintf(stderr, ", %d", (int)regs->rdx);
  fprintf(stderr, ")\n");
}

void		print_ret_dup3(struct user_regs_struct *regs, pid_t pid)
{
  (void)pid;
  fprintf(stderr, "|Ret----dup3--->(");
  print_errno((int)regs->rax);
  fprintf(stderr, ")\n");
}
