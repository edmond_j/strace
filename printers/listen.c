/*
** header.c for aff in /home/lequeu_m/
**
** Made by martin lequeux-gruninger
** Login   <lequeu_m@epitech.net>
**
** Started on  Fri May 10 17:38:41 2013 martin lequeux-gruninger
** Last update Thu May 16 09:34:41 2013 julien edmond
*/

/*
**       #include <sys/types.h>           See NOTES
**       #include <sys/socket.h>
**
**       int listen(int sockfd, int backlog);
**
*/

#include <sys/types.h>
#include <sys/socket.h>
#include <unistd.h>
#include "aff.h"
#include "strace.h"

void		print_args_listen(struct user_regs_struct *regs, pid_t pid)
{
  (void)pid;
  fprintf(stderr, "listen(");
  fprintf(stderr, "%d, %d", (int)regs->rdi, (int)regs->rsi);
  fprintf(stderr, ")\n");
}

void		print_ret_listen(struct user_regs_struct *regs, pid_t pid)
{
  (void)pid;
  fprintf(stderr, "|Ret----listen--->(");
  print_errno((int)regs->rax);
  fprintf(stderr, ")\n");
}
