/*
** header.c for aff in /home/lequeu_m/
**
** Made by martin lequeux-gruninger
** Login   <lequeu_m@epitech.net>
**
** Started on  Fri May 10 17:38:41 2013 martin lequeux-gruninger
** Last update Wed May 15 13:44:37 2013 julien edmond
*/

/*
**       int pivot_root(const char *new_root, const char *put_old);
**
*/

#include <unistd.h>
#include "aff.h"
#include "strace.h"

void		print_args_pivot_root(struct user_regs_struct *regs, pid_t pid)
{
  fprintf(stderr, "pivot_root(");
  print_string(pid, (void *)regs->rdi);
  fputs(", ", stderr);
  print_string(pid, (void *)regs->rsi);
  fprintf(stderr, ")\n");
}

void		print_ret_pivot_root(struct user_regs_struct *regs, pid_t pid)
{
  (void)pid;
  fprintf(stderr, "|Ret----pivot_root--->(");
  print_errno((int)regs->rax);
  fprintf(stderr, ")\n");
}
