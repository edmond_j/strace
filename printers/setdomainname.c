/*
** header.c for aff in /home/lequeu_m/
**
** Made by martin lequeux-gruninger
** Login   <lequeu_m@epitech.net>
**
** Started on  Fri May 10 17:38:41 2013 martin lequeux-gruninger
** Last update Tue May 14 13:15:41 2013 julien edmond
*/

/*
**       #include <unistd.h>
**
**       int getdomainname(char *name, size_t len);
**       int setdomainname(const char *name, size_t len);
**
**   Feature Test Macro Requirements for glibc (see feature_test_macros(7)):
**
**       getdomainname(), setdomainname():
**           _BSD_SOURCE || (_XOPEN_SOURCE && _XOPEN_SOURCE < 500)
**
*/

#include <stdlib.h>
#include <unistd.h>
#include <unistd.h>
#include "aff.h"
#include "strace.h"

void		print_sized_string(int pid, void *from, unsigned size)
{
  char		*buff;

  if ((buff = malloc(sizeof(char) * (size + 1))))
    {
      read_from_son(pid, from, buff, size);
      buff[size] = 0;
      fputs(buff, stderr);
      free(buff);
    }
}

void		print_args_setdomainname(struct user_regs_struct *regs,
					 pid_t pid)
{
  fprintf(stderr, "setdomainname(");
  print_sized_string(pid, (void *)regs->rdi, ((size_t)regs->rsi < 42 ?
					      regs->rsi : 42));
  fprintf(stderr, ", %lu", (size_t)regs->rsi);
  fprintf(stderr, ")\n");
}

void		print_ret_setdomainname(struct user_regs_struct *regs, pid_t pid)
{
  (void)pid;
  fprintf(stderr, "|Ret----setdomainname--->(");
  print_errno((int)regs->rax);
  fprintf(stderr, ")\n");
}
