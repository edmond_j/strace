/*
** header.c for aff in /home/lequeu_m/
**
** Made by martin lequeux-gruninger
** Login   <lequeu_m@epitech.net>
**
** Started on  Fri May 10 17:38:41 2013 martin lequeux-gruninger
** Last update Wed May 15 12:19:12 2013 julien edmond
*/

/*
**       #include <stdio.h>
**
**       int rename(const char *oldpath, const char *newpath);
**
*/

#include <stdio.h>
#include <unistd.h>
#include "aff.h"
#include "strace.h"

void		print_args_rename(struct user_regs_struct *regs, pid_t pid)
{
  fprintf(stderr, "rename(");
  print_string(pid, (void *)regs->rdi);
  fputs(", ", stderr);
  print_string(pid, (void *)regs->rsi);
  fprintf(stderr, ")\n");
}

void		print_ret_rename(struct user_regs_struct *regs, pid_t pid)
{
  (void)pid;
  fprintf(stderr, "|Ret----rename--->(");
  print_errno((int)regs->rax);
  fprintf(stderr, ")\n");
}
