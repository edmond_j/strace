/*
** header.c for aff in /home/lequeu_m/
**
** Made by martin lequeux-gruninger
** Login   <lequeu_m@epitech.net>
**
** Started on  Fri May 10 17:38:41 2013 martin lequeux-gruninger
** Last update Thu May 16 09:37:32 2013 julien edmond
*/

/*
**       #include <fcntl.h>  Definition of AT_* constants
**       #include <unistd.h>
**
**       int linkat(int olddirfd, const char *oldpath,
**                  int newdirfd, const char *newpath, int flags);
**
**   Feature Test Macro Requirements for glibc (see feature_test_macros(7)):
**
**       linkat():
**           Since glibc 2.10:
**               _XOPEN_SOURCE >= 700 || _POSIX_C_SOURCE >= 200809L
**           Before glibc 2.10:
**               _ATFILE_SOURCE
**
*/

#include <fcntl.h>
#include <unistd.h>
#include <unistd.h>
#include "aff.h"
#include "strace.h"

void		print_args_linkat(struct user_regs_struct *regs, pid_t pid)
{
  fprintf(stderr, "linkat(");
  fprintf(stderr, "%d, ", (int)regs->rdi);
  print_string(pid, (void *)regs->rsi);
  fprintf(stderr, ", %d, ", (int)regs->rdx);
  print_string(pid, (void *)regs->rcx);
  fprintf(stderr, ", %d", (int)regs->r8);
  fprintf(stderr, ")\n");
}

void		print_ret_linkat(struct user_regs_struct *regs, pid_t pid)
{
  (void)pid;
  fprintf(stderr, "|Ret----linkat--->(");
  print_errno((int)regs->rax);
  fprintf(stderr, ")\n");
}
