/*
** header.c for aff in /home/lequeu_m/
**
** Made by martin lequeux-gruninger
** Login   <lequeu_m@epitech.net>
**
** Started on  Fri May 10 17:38:41 2013 martin lequeux-gruninger
** Last update Thu May 16 10:31:55 2013 martin lequeux-gruninger
*/

/*
**       #include <linux/futex.h>
**       #include <sys/time.h>
**
**       int futex(int *uaddr, int op, int val, const struct timespec *timeout,
**                 int *uaddr2, int val3);
**
*/

#include <linux/futex.h>
#include <sys/time.h>
#include <unistd.h>
#include "aff.h"
#include "strace.h"

void		print_args_futex(struct user_regs_struct *regs, pid_t pid)
{
  (void)pid;
  fprintf(stderr, "futex(");
  fprintf(stderr, "%p", (int *)regs->rdi);
  fprintf(stderr, ", %d", (int)regs->rsi);
  fprintf(stderr, ", %d", (int)regs->rdx);
  fprintf(stderr, ", %p", (const struct timespec *)regs->rcx);
  fprintf(stderr, ", %p", (int *)regs->r8);
  fprintf(stderr, ", %d", (int)regs->r9);
  fprintf(stderr, ")\n");
}

void		print_ret_futex(struct user_regs_struct *regs, pid_t pid)
{
  (void)pid;
  fprintf(stderr, "|Ret----futex--->(");
  print_errno((int)regs->rax);
  fprintf(stderr, ")\n");
}
