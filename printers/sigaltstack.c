/*
** header.c for aff in /home/lequeu_m/
**
** Made by martin lequeux-gruninger
** Login   <lequeu_m@epitech.net>
**
** Started on  Fri May 10 17:38:41 2013 martin lequeux-gruninger
** Last update Tue May 14 10:24:52 2013 julien edmond
*/

/*
**       #include <signal.h>
**
**       int sigaltstack(const stack_t *ss, stack_t *oss);
**
**   Feature Test Macro Requirements for glibc (see feature_test_macros(7)):
**
**       sigaltstack():
**           _BSD_SOURCE || _XOPEN_SOURCE >= 500 ||
**           _XOPEN_SOURCE && _XOPEN_SOURCE_EXTENDED
**           ||  Since glibc 2.12:  _POSIX_C_SOURCE >= 200809L
**
*/

#include <signal.h>
#include <unistd.h>
#include "aff.h"
#include "strace.h"

void		print_stack_t(int pid, stack_t *ptr)
{
  stack_t	ss;

  read_from_son(pid, ptr, &ss, sizeof(ss));
  fprintf(stderr, "%p, %d, %lu", ss.ss_sp, ss.ss_flags, ss.ss_size);
}

void		print_args_sigaltstack(struct user_regs_struct *regs,
				       pid_t pid)
{
  fprintf(stderr, "sigaltstack(");
  print_stack_t(pid, (stack_t *)regs->rdi);
  fprintf(stderr, ", %p", (void *)regs->rsi);
  fprintf(stderr, ")\n");
}

void		print_ret_sigaltstack(struct user_regs_struct *regs, pid_t pid)
{
  (void)pid;
  fprintf(stderr, "|Ret----sigaltstack--->(");
  print_errno((int)regs->rax);
  fprintf(stderr, ")\n");
}
