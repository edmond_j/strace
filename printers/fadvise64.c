/*
** header.c for aff in /home/lequeu_m/
**
** Made by martin lequeux-gruninger
** Login   <lequeu_m@epitech.net>
**
** Started on  Fri May 10 17:38:41 2013 martin lequeux-gruninger
** Last update Thu May 16 10:26:25 2013 martin lequeux-gruninger
*/

/*
**       #include <fcntl.h>
**
**       int posix_fadvise(int fd, off_t offset, off_t len, int advice);
**
**   Feature Test Macro Requirements for glibc (see feature_test_macros(7)):
**
**       posix_fadvise():
**           _XOPEN_SOURCE >= 600 || _POSIX_C_SOURCE >= 200112L
**
*/

#include <fcntl.h>
#include <unistd.h>
#include "aff.h"
#include "strace.h"

void		print_args_fadvise64(struct user_regs_struct *regs, pid_t pid)
{
  (void)pid;
  (void)regs;
  fprintf(stderr, "fadvise64(");
  fprintf(stderr, "%d", (int)regs->rdi);
  fprintf(stderr, ", %ld", (off_t)regs->rsi);
  fprintf(stderr, ", %ld", (off_t)regs->rdx);
  fprintf(stderr, ", %d", (int)regs->rcx);
  fprintf(stderr, ")\n");
}

void		print_ret_fadvise64(struct user_regs_struct *regs, pid_t pid)
{
  (void)pid;
  fprintf(stderr, "|Ret----fadvise64--->(");
  print_errno((int)regs->rax);
  fprintf(stderr, ")\n");
}
