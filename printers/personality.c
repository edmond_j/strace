/*
** header.c for aff in /home/lequeu_m/
**
** Made by martin lequeux-gruninger
** Login   <lequeu_m@epitech.net>
**
** Started on  Fri May 10 17:38:41 2013 martin lequeux-gruninger
** Last update Wed May 15 13:46:09 2013 julien edmond
*/

/*
**       #include <sys/personality.h>
**
**       int personality(unsigned long persona);
**
*/

#include <sys/personality.h>
#include <unistd.h>
#include "aff.h"
#include "strace.h"

void		print_args_personality(struct user_regs_struct *regs, pid_t pid)
{
  (void)pid;
  fprintf(stderr, "personality(");
  fprintf(stderr, "%lu", (unsigned long)regs->rdi);
  fprintf(stderr, ")\n");
}

void		print_ret_personality(struct user_regs_struct *regs, pid_t pid)
{
  (void)pid;
  fprintf(stderr, "|Ret----personality--->(");
  print_errno((int)regs->rax);
  fprintf(stderr, ")\n");
}
