/*
** header.c for aff in /home/lequeu_m/
**
** Made by martin lequeux-gruninger
** Login   <lequeu_m@epitech.net>
**
** Started on  Fri May 10 17:38:41 2013 martin lequeux-gruninger
** Last update Tue May 14 14:04:31 2013 julien edmond
*/

/*
**       #include <sys/types.h>
**       #include <sys/ipc.h>
**       #include <sys/sem.h>
**
**       int semget(key_t key, int nsems, int semflg);
**
*/

#include <sys/types.h>
#include <sys/ipc.h>
#include <sys/sem.h>
#include <unistd.h>
#include "aff.h"
#include "strace.h"

void		print_args_semget(struct user_regs_struct *regs, pid_t pid)
{
  (void)pid;
  fprintf(stderr, "semget(");
  fprintf(stderr, "%d, %d, %d,", (key_t)regs->rdi, (int)regs->rsi,
	  (int)regs->rdx);
  fprintf(stderr, ")\n");
}

void		print_ret_semget(struct user_regs_struct *regs, pid_t pid)
{
  (void)pid;
  fprintf(stderr, "|Ret----semget--->(");
  print_errno((int)regs->rax);
  fprintf(stderr, ")\n");
}
