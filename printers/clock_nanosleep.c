/*
** header.c for aff in /home/lequeu_m/
**
** Made by martin lequeux-gruninger
** Login   <lequeu_m@epitech.net>
**
** Started on  Fri May 10 17:38:41 2013 martin lequeux-gruninger
** Last update Thu May 16 13:36:47 2013 julien edmond
*/

/*
**       #include <time.h>
**
**       int clock_nanosleep(clockid_t clock_id, int flags,
**                           const struct timespec *request,
**                           struct timespec *remain);
**
**       Link with -lrt.
**
**   Feature Test Macro Requirements for glibc (see feature_test_macros(7)):
**
**       clock_nanosleep():
**           _XOPEN_SOURCE >= 600 || _POSIX_C_SOURCE >= 200112L
**
*/

#include <time.h>
#include <unistd.h>
#include "aff.h"
#include "strace.h"

void		print_args_clock_nanosleep(struct user_regs_struct *regs,
					   pid_t pid)
{
  (void)pid;
  fprintf(stderr, "clock_nanosleep(");
  fprintf(stderr, "%d", (clockid_t)regs->rdi);
  fprintf(stderr, ", %d", (int)regs->rsi);
  fprintf(stderr, ", %p", (const struct timespec *)regs->rdx);
  fprintf(stderr, ", %p", (struct timespec *)regs->rcx);
  fprintf(stderr, ")\n");
}

void		print_ret_clock_nanosleep(struct user_regs_struct *regs,
					  pid_t pid)
{
  (void)pid;
  fprintf(stderr, "|Ret----clock_nanosleep--->(");
  print_errno((int)regs->rax);
  fprintf(stderr, ")\n");
}
