/*
** header.c for aff in /home/lequeu_m/
**
** Made by martin lequeux-gruninger
** Login   <lequeu_m@epitech.net>
**
** Started on  Fri May 10 17:38:41 2013 martin lequeux-gruninger
** Last update Tue May 14 08:54:58 2013 julien edmond
*/

/*
**       #include <fcntl.h>  Definition of AT_* constants
**       #include <stdio.h>
**
**       int symlinkat(const char *oldpath, int newdirfd, const char *newpath);
**
**   Feature Test Macro Requirements for glibc (see feature_test_macros(7)):
**
**       symlinkat():
**           Since glibc 2.10:
**               _XOPEN_SOURCE >= 700 || _POSIX_C_SOURCE >= 200809L
**           Before glibc 2.10:
**               _ATFILE_SOURCE
**
*/

#include <fcntl.h>
#include <stdio.h>
#include <unistd.h>
#include "aff.h"
#include "strace.h"

void		print_args_symlinkat(struct user_regs_struct *regs, pid_t pid)
{
  fprintf(stderr, "symlinkat(");
  print_string(pid, (void *)regs->rdi);
  fprintf(stderr, ", %d, ", (int)regs->rsi);
  print_string(pid, (void *)regs->rdx);
  fprintf(stderr, ")\n");
}

void		print_ret_symlinkat(struct user_regs_struct *regs, pid_t pid)
{
  (void)pid;
  fprintf(stderr, "|Ret----symlinkat--->(");
  print_errno((int)regs->rax);
  fprintf(stderr, ")\n");
}
