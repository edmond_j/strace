/*
** header.c for aff in /home/lequeu_m/
**
** Made by martin lequeux-gruninger
** Login   <lequeu_m@epitech.net>
**
** Started on  Fri May 10 17:38:41 2013 martin lequeux-gruninger
** Last update Thu May 16 10:20:35 2013 martin lequeux-gruninger
*/

/*
**       #include <asm/prctl.h>
**       #include <sys/prctl.h>
**
**       int arch_prctl(int code, unsigned long addr);
**       int arch_prctl(int code, unsigned long *addr);
**
*/

#include <asm/prctl.h>
#include <sys/prctl.h>
#include <unistd.h>
#include "aff.h"
#include "strace.h"

void		print_args_arch_prctl(struct user_regs_struct *regs, pid_t pid)
{
  (void)pid;
  fprintf(stderr, "arch_prctl(");
  fprintf(stderr, "%d", (int)regs->rdi);
  fprintf(stderr, ", %lx", (unsigned long)regs->rsi);
  fprintf(stderr, ")\n");
}

void		print_ret_arch_prctl(struct user_regs_struct *regs, pid_t pid)
{
  (void)pid;
  fprintf(stderr, "|Ret----arch_prctl--->(");
  print_errno((int)regs->rax);
  fprintf(stderr, ")\n");
}
