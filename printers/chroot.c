/*
** header.c for aff in /home/lequeu_m/
**
** Made by martin lequeux-gruninger
** Login   <lequeu_m@epitech.net>
**
** Started on  Fri May 10 17:38:41 2013 martin lequeux-gruninger
** Last update Thu May 16 10:22:07 2013 martin lequeux-gruninger
*/

/*
**       #include <unistd.h>
**
**       int chroot(const char *path);
**
**   Feature Test Macro Requirements for glibc (see feature_test_macros(7)):
**
**       chroot():
**           Since glibc 2.2.2:
**               _BSD_SOURCE ||
**                   (_XOPEN_SOURCE >= 500 ||
**                       _XOPEN_SOURCE && _XOPEN_SOURCE_EXTENDED) &&
**                   !(_POSIX_C_SOURCE >= 200112L || _XOPEN_SOURCE >= 600)
**           Before glibc 2.2.2: none
**
*/

#include <unistd.h>
#include <unistd.h>
#include "aff.h"
#include "strace.h"

void		print_args_chroot(struct user_regs_struct *regs, pid_t pid)
{
  fprintf(stderr, "chroot(");
  print_string(pid, (void *)regs->rdi);
  fprintf(stderr, ")\n");
}

void		print_ret_chroot(struct user_regs_struct *regs, pid_t pid)
{
  (void)pid;
  fprintf(stderr, "|Ret----chroot--->(");
  print_errno((int)regs->rax);
  fprintf(stderr, ")\n");
}
