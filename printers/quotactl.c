/*
** header.c for aff in /home/lequeu_m/
**
** Made by martin lequeux-gruninger
** Login   <lequeu_m@epitech.net>
**
** Started on  Fri May 10 17:38:41 2013 martin lequeux-gruninger
** Last update Wed May 15 13:21:17 2013 julien edmond
*/

/*
**       #include <sys/quota.h>
**       #include <xfs/xqm.h>
**
**       int quotactl(int cmd, const char *special, int id, caddr_t addr);
**
*/

#include <sys/quota.h>
#include <unistd.h>
#include "aff.h"
#include "strace.h"

void		print_args_quotactl(struct user_regs_struct *regs, pid_t pid)
{
  fprintf(stderr, "quotactl(");
  fprintf(stderr, "%d, ", (int)regs->rdi);
  print_string(pid, (void *)regs->rsi);
  fprintf(stderr, ", %d, %p", (int)regs->rdx, (caddr_t)regs->rcx);
  fprintf(stderr, ")\n");
}

void		print_ret_quotactl(struct user_regs_struct *regs, pid_t pid)
{
  (void)pid;
  fprintf(stderr, "|Ret----quotactl--->(");
  print_errno((int)regs->rax);
  fprintf(stderr, ")\n");
}
