/*
** header.c for aff in /home/lequeu_m/
**
** Made by martin lequeux-gruninger
** Login   <lequeu_m@epitech.net>
**
** Started on  Fri May 10 17:38:41 2013 martin lequeux-gruninger
** Last update Mon May 13 15:18:28 2013 julien edmond
*/

/*
**       #include <sys/mount.h>
**
**       int umount(const char *target);
**
**       int umount2(const char *target, int flags);
**
*/

#include <sys/mount.h>
#include <unistd.h>
#include "aff.h"
#include "strace.h"

void		print_args_umount(struct user_regs_struct *regs, pid_t pid)
{
  (void)pid;
  fprintf(stderr, "umount(");
  print_string(pid, (void *)regs->rdi);
  fprintf(stderr, ")\n");
}

void		print_ret_umount(struct user_regs_struct *regs, pid_t pid)
{
  (void)pid;
  fprintf(stderr, "|Ret----umount--->(");
  print_errno((int)regs->rax);
  fprintf(stderr, ")\n");
}
