/*
** header.c for aff in /home/lequeu_m/
**
** Made by martin lequeux-gruninger
** Login   <lequeu_m@epitech.net>
**
** Started on  Fri May 10 17:38:41 2013 martin lequeux-gruninger
** Last update Wed May 15 13:32:27 2013 julien edmond
*/

/*
**        According to POSIX.1-2001
**       #include <sys/select.h>
**
**        According to earlier standards
**       #include <sys/time.h>
**       #include <sys/types.h>
**       #include <unistd.h>
**
**       int select(int nfds, fd_set *readfds, fd_set *writefds,
**                  fd_set *exceptfds, struct timeval *timeout);
**
**       void FD_CLR(int fd, fd_set *set);
**       int  FD_ISSET(int fd, fd_set *set);
**       void FD_SET(int fd, fd_set *set);
**       void FD_ZERO(fd_set *set);
**
**       #include <sys/select.h>
**
**       int pselect(int nfds, fd_set *readfds, fd_set *writefds,
**                   fd_set *exceptfds, const struct timespec *timeout,
**                   const sigset_t *sigmask);
**
**   Feature Test Macro Requirements for glibc (see feature_test_macros(7)):
**
**       pselect(): _POSIX_C_SOURCE >= 200112L || _XOPEN_SOURCE >= 600
*/

#include <sys/select.h>
#include <sys/time.h>
#include <sys/types.h>
#include <unistd.h>
#include <sys/select.h>
#include <stdio.h>
#include <stdlib.h>
#include <sys/time.h>
#include <sys/types.h>
#include <unistd.h>
#include <unistd.h>
#include "aff.h"
#include "strace.h"

void		print_args_pselect6(struct user_regs_struct *regs, pid_t pid)
{
  (void)pid;
  fprintf(stderr, "pselect6(");
  fprintf(stderr, "%d, %p, %p, %p, %p, %p", (int)regs->rdi, (void *)regs->rsi,
	  (void *)regs->rdx, (void *)regs->rcx, (void *)regs->r8,
	  (void *)regs->r9);
  fprintf(stderr, ")\n");
}

void		print_ret_pselect6(struct user_regs_struct *regs, pid_t pid)
{
  (void)pid;
  fprintf(stderr, "|Ret----pselect6--->(");
  print_errno((int)regs->rax);
  fprintf(stderr, ")\n");
}
