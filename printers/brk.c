/*
** header.c for aff in /home/lequeu_m/
**
** Made by martin lequeux-gruninger
** Login   <lequeu_m@epitech.net>
**
** Started on  Fri May 10 17:38:41 2013 martin lequeux-gruninger
** Last update Wed May 15 10:13:43 2013 martin lequeux-gruninger
*/

/*
**       #include <unistd.h>
**
**       int brk(void *addr);
**
**       void *sbrk(intptr_t increment);
**
**   Feature Test Macro Requirements for glibc (see feature_test_macros(7)):
**
**       brk(), sbrk():
**           Since glibc 2.12:
**               _BSD_SOURCE || _SVID_SOURCE ||
**                   (_XOPEN_SOURCE >= 500 ||
**                       _XOPEN_SOURCE && _XOPEN_SOURCE_EXTENDED) &&
**                   !(_POSIX_C_SOURCE >= 200112L || _XOPEN_SOURCE >= 600)
**           Before glibc 2.12:
**               _BSD_SOURCE || _SVID_SOURCE || _XOPEN_SOURCE >= 500 ||
**               _XOPEN_SOURCE && _XOPEN_SOURCE_EXTENDED
**
*/

#include <unistd.h>
#include <unistd.h>
#include "aff.h"
#include "strace.h"

void		print_args_brk(struct user_regs_struct *regs, pid_t pid)
{
  (void)pid;
  fprintf(stderr, "brk(");
  fprintf(stderr, "%p", (void *)regs->rdi);
  fprintf(stderr, ")\n");
}

void		print_ret_brk(struct user_regs_struct *regs, pid_t pid)
{
  (void)pid;
  fprintf(stderr, "|Ret----brk--->(");
  print_errno((int)regs->rax);
  fprintf(stderr, ")\n");
}
