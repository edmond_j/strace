/*
** header.c for aff in /home/lequeu_m/
**
** Made by martin lequeux-gruninger
** Login   <lequeu_m@epitech.net>
**
** Started on  Fri May 10 17:38:41 2013 martin lequeux-gruninger
** Last update Tue May 14 11:22:49 2013 julien edmond
*/

/*
**       #include <sys/time.h>
**       #include <sys/resource.h>
**
**       int getpriority(int which, int who);
**       int setpriority(int which, int who, int prio);
**
*/

#include <sys/time.h>
#include <sys/resource.h>
#include <unistd.h>
#include "aff.h"
#include "strace.h"

void		print_args_setpriority(struct user_regs_struct *regs, pid_t pid)
{
  (void)pid;
  fprintf(stderr, "setpriority(");
  fprintf(stderr, "%d, %d", (int)regs->rdi, (int)regs->rsi);
  fprintf(stderr, ")\n");
}

void		print_ret_setpriority(struct user_regs_struct *regs, pid_t pid)
{
  (void)pid;
  fprintf(stderr, "|Ret----setpriority--->(");
  print_errno((int)regs->rax);
  fprintf(stderr, ")\n");
}
