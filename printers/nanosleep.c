/*
** header.c for aff in /home/lequeu_m/
**
** Made by martin lequeux-gruninger
** Login   <lequeu_m@epitech.net>
**
** Started on  Fri May 10 17:38:41 2013 martin lequeux-gruninger
** Last update Wed May 15 14:42:29 2013 julien edmond
*/

/*
**       #include <time.h>
**
**       int nanosleep(const struct timespec *req, struct timespec *rem);
**
**   Feature Test Macro Requirements for glibc (see feature_test_macros(7)):
**
**       nanosleep(): _POSIX_C_SOURCE >= 199309L
**
*/

#include <time.h>
#include <unistd.h>
#include "aff.h"
#include "strace.h"

void		print_args_nanosleep(struct user_regs_struct *regs, pid_t pid)
{
  (void)pid;
  fprintf(stderr, "nanosleep(");
  fprintf(stderr, "%p, %p", (void *)regs->rdi, (void *)regs->rsi);
  fprintf(stderr, ")\n");
}

void		print_ret_nanosleep(struct user_regs_struct *regs, pid_t pid)
{
  (void)pid;
  fprintf(stderr, "|Ret----nanosleep--->(");
  print_errno((int)regs->rax);
  fprintf(stderr, ")\n");
}
