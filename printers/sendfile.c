/*
** header.c for aff in /home/lequeu_m/
**
** Made by martin lequeux-gruninger
** Login   <lequeu_m@epitech.net>
**
** Started on  Fri May 10 17:38:41 2013 martin lequeux-gruninger
** Last update Tue May 14 14:00:46 2013 julien edmond
*/

/*
**       #include <sys/sendfile.h>
**
**       ssize_t sendfile(int out_fd, int in_fd, off_t *offset, size_t count);
**
*/

#include <sys/sendfile.h>
#include <unistd.h>
#include "aff.h"
#include "strace.h"

void		print_args_sendfile(struct user_regs_struct *regs, pid_t pid)
{
  (void)pid;
  fprintf(stderr, "sendfile(");
  fprintf(stderr, "%d, %d, %p, %lu", (int)regs->rdi, (int)regs->rsi,
	  (off_t *)regs->rdx, (size_t)regs->rcx);
  fprintf(stderr, ")\n");
}

void		print_ret_sendfile(struct user_regs_struct *regs, pid_t pid)
{
  (void)pid;
  fprintf(stderr, "|Ret----sendfile--->(");
  if ((ssize_t)regs->rax < 0)
    print_errno((int)regs->rax);
  else
    fprintf(stderr, "%ld", regs->rax);
  fprintf(stderr, ")\n");
}
