/*
** header.c for aff in /home/lequeu_m/
**
** Made by martin lequeux-gruninger
** Login   <lequeu_m@epitech.net>
**
** Started on  Fri May 10 17:38:41 2013 martin lequeux-gruninger
** Last update Thu May 16 09:37:36 2013 martin lequeux-gruninger
*/

/*
**       #include <sys/time.h>
**
**       int getitimer(int which, struct itimerval *curr_value);
**       int setitimer(int which, const struct itimerval *new_value,
**                     struct itimerval *old_value);
**
*/

#include <sys/time.h>
#include <unistd.h>
#include "aff.h"
#include "strace.h"

void			print_itimerval_get(int pid, struct itimerval *from)
{
  struct itimerval	to;

  read_from_son(pid, from, &to, sizeof(to));
  fprintf(stderr, "{ {%lu, %lu}, {%lu, %lu} }",
	  to.it_interval.tv_sec, to.it_interval.tv_usec,
	  to.it_value.tv_sec, to.it_value.tv_usec);
}

void		print_args_getitimer(struct user_regs_struct *regs, pid_t pid)
{
  (void)pid;
  fprintf(stderr, "getitimer(");
  fprintf(stderr, "%d", (int)regs->rdi);
  fprintf(stderr, "%p", (struct itimerval *)regs->rsi);
  fprintf(stderr, ")\n");
}

void		print_ret_getitimer(struct user_regs_struct *regs, pid_t pid)
{
  fprintf(stderr, "|Ret----getitimer--->(");
  print_errno((int)regs->rax);
  fprintf(stderr, ", ");
  print_itimerval_get(pid, (struct itimerval *)regs->rsi);
  fprintf(stderr, ")\n");
}
