/*
** header.c for aff in /home/lequeu_m/
**
** Made by martin lequeux-gruninger
** Login   <lequeu_m@epitech.net>
**
** Started on  Fri May 10 17:38:41 2013 martin lequeux-gruninger
** Last update Wed May 15 17:04:37 2013 julien edmond
*/

/*
**       #include <sys/types.h>
**       #include <sys/ipc.h>
**       #include <sys/msg.h>
**
**       int msgctl(int msqid, int cmd, struct msqid_ds *buf);
**
*/

#include <sys/types.h>
#include <sys/ipc.h>
#include <sys/msg.h>
#include <unistd.h>
#include "aff.h"
#include "strace.h"

void		print_args_msgctl(struct user_regs_struct *regs, pid_t pid)
{
  (void)pid;
  fprintf(stderr, "msgctl(");
  fprintf(stderr, "%d, %d, %p", (int)regs->rdi, (int)regs->rsi,
	  (void *)regs->rdx);
  fprintf(stderr, ")\n");
}

void		print_ret_msgctl(struct user_regs_struct *regs, pid_t pid)
{
  (void)pid;
  fprintf(stderr, "|Ret----msgctl--->(");
  print_errno((int)regs->rax);
  fprintf(stderr, ")\n");
}
