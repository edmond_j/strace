/*
** header.c for aff in /home/lequeu_m/
**
** Made by martin lequeux-gruninger
** Login   <lequeu_m@epitech.net>
**
** Started on  Fri May 10 17:38:41 2013 martin lequeux-gruninger
** Last update Wed May 15 16:56:57 2013 julien edmond
*/

/*
**       #include <sys/mman.h>
**
**       int mlock(const void *addr, size_t len);
**       int munlock(const void *addr, size_t len);
**
**       int mlockall(int flags);
**       int munlockall(void);
**
*/

#include <sys/mman.h>
#include <unistd.h>
#include "aff.h"
#include "strace.h"

void		print_args_munlock(struct user_regs_struct *regs, pid_t pid)
{
  (void)pid;
  fprintf(stderr, "munlock(");
  fprintf(stderr, "%p, %lu", (void *)regs->rdi, (size_t)regs->rsi);
  fprintf(stderr, ")\n");
}

void		print_ret_munlock(struct user_regs_struct *regs, pid_t pid)
{
  (void)pid;
  fprintf(stderr, "|Ret----munlock--->(");
  print_errno((int)regs->rax);
  fprintf(stderr, ")\n");
}
