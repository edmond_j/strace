/*
** header.c for aff in /home/lequeu_m/
**
** Made by martin lequeux-gruninger
** Login   <lequeu_m@epitech.net>
**
** Started on  Fri May 10 17:38:41 2013 martin lequeux-gruninger
** Last update Tue May 14 14:02:33 2013 julien edmond
*/

/*
**       #include <sys/types.h>
**       #include <sys/ipc.h>
**       #include <sys/sem.h>
**
**       int semop(int semid, struct sembuf *sops, unsigned nsops);
**
**       int semtimedop(int semid, struct sembuf *sops, unsigned nsops,
**                      struct timespec *timeout);
**
**   Feature Test Macro Requirements for glibc (see feature_test_macros(7)):
**
**       semtimedop(): _GNU_SOURCE
**
*/

#include <sys/types.h>
#include <sys/ipc.h>
#include <sys/sem.h>
#include <unistd.h>
#include "aff.h"
#include "strace.h"

void		print_args_semtimedop(struct user_regs_struct *regs, pid_t pid)
{
  (void)pid;
  fprintf(stderr, "semtimedop(");
  fprintf(stderr, "%d, %p, %u, %p", (int)regs->rdi, (void *)regs->rsi,
	  (unsigned)regs->rdx, (void *)regs->rcx);
  fprintf(stderr, ")\n");
}

void		print_ret_semtimedop(struct user_regs_struct *regs, pid_t pid)
{
  (void)pid;
  fprintf(stderr, "|Ret----semtimedop--->(");
  print_errno((int)regs->rax);
  fprintf(stderr, ")\n");
}
