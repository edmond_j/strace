/*
** header.c for aff in /home/lequeu_m/
**
** Made by martin lequeux-gruninger
** Login   <lequeu_m@epitech.net>
**
** Started on  Fri May 10 17:38:41 2013 martin lequeux-gruninger
** Last update Wed May 15 17:26:17 2013 julien edmond
*/

/*
**       #include <numaif.h>
**
**       long move_pages(int pid, unsigned long count, void **pages,
**                       const int *nodes, int *status, int flags);
**
**       Link with -lnuma.
**
*/

#include <unistd.h>
#include "aff.h"
#include "strace.h"

void		print_args_move_pages(struct user_regs_struct *regs, pid_t pid)
{
  (void)pid;
  fprintf(stderr, "move_pages(");
  fprintf(stderr, "%d, %lu, %p, %p, %p, %d", (int)regs->rdi,
	  (unsigned long)regs->rsi, (void **)regs->rdx, (const int *)regs->rcx,
	  (int *)regs->r8, (int)regs->r9);
  fprintf(stderr, ")\n");
}

void		print_ret_move_pages(struct user_regs_struct *regs, pid_t pid)
{
  (void)pid;
  fprintf(stderr, "|Ret----move_pages--->(");
  if ((long)regs->rax < 0)
    print_errno((int)regs->rax);
  else
    fprintf(stderr, "%ld", regs->rax);
  fprintf(stderr, ")\n");
}
