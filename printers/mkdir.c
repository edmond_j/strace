/*
** header.c for aff in /home/lequeu_m/
**
** Made by martin lequeux-gruninger
** Login   <lequeu_m@epitech.net>
**
** Started on  Fri May 10 17:38:41 2013 martin lequeux-gruninger
** Last update Wed May 15 17:36:20 2013 julien edmond
*/

/*
**       #include <sys/stat.h>
**       #include <sys/types.h>
**
**       int mkdir(const char *pathname, mode_t mode);
**
*/

#include <sys/stat.h>
#include <sys/types.h>
#include <unistd.h>
#include "aff.h"
#include "strace.h"

void		print_args_mkdir(struct user_regs_struct *regs, pid_t pid)
{
  fprintf(stderr, "mkdir(");
  print_string(pid, (void *)regs->rdi);
  fprintf(stderr, ", %04o", (mode_t)regs->rsi);
  fprintf(stderr, ")\n");
}

void		print_ret_mkdir(struct user_regs_struct *regs, pid_t pid)
{
  (void)pid;
  fprintf(stderr, "|Ret----mkdir--->(");
  print_errno((int)regs->rax);
  fprintf(stderr, ")\n");
}
