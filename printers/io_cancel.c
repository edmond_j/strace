/*
** header.c for aff in /home/lequeu_m/
**
** Made by martin lequeux-gruninger
** Login   <lequeu_m@epitech.net>
**
** Started on  Fri May 10 17:38:41 2013 martin lequeux-gruninger
** Last update Thu May 16 12:02:16 2013 martin lequeux-gruninger
*/

/*
**       #include <libaio.h>
**
**       int io_cancel(aio_context_t ctx_id, struct iocb *iocb,
**                     struct io_event *result);
**
**       Link with -laio.
**
*/

#include <unistd.h>
#include "aff.h"
#include "strace.h"

void		print_args_io_cancel(struct user_regs_struct *regs, pid_t pid)
{
  (void)pid;
  fprintf(stderr, "io_cancel(");
  fprintf(stderr, "%lu", (unsigned long)regs->rdi);
  fprintf(stderr, ", %p", (struct iocb *)regs->rsi);
  fprintf(stderr, ", %p", (struct io_event *)regs->rdx);
  fprintf(stderr, ")\n");
}

void		print_ret_io_cancel(struct user_regs_struct *regs, pid_t pid)
{
  (void)pid;
  fprintf(stderr, "|Ret----io_cancel--->(");
  print_errno((int)regs->rax);
  fprintf(stderr, ")\n");
}
