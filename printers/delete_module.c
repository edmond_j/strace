/*
** header.c for aff in /home/lequeu_m/
**
** Made by martin lequeux-gruninger
** Login   <lequeu_m@epitech.net>
**
** Started on  Fri May 10 17:38:41 2013 martin lequeux-gruninger
** Last update Thu May 16 10:23:25 2013 martin lequeux-gruninger
*/

/*
**       #include <linux/module.h>
**
**       int delete_module(const char *name);
**
*/

#include <unistd.h>
#include "aff.h"
#include "strace.h"

void		print_args_delete_module(struct user_regs_struct *regs, pid_t pid)
{
  fprintf(stderr, "delete_module(");
  print_string(pid, (void *)regs->rdi);
  fprintf(stderr, ")\n");
}

void		print_ret_delete_module(struct user_regs_struct *regs, pid_t pid)
{
  (void)pid;
  fprintf(stderr, "|Ret----delete_module--->(");
  print_errno((int)regs->rax);
  fprintf(stderr, ")\n");
}
