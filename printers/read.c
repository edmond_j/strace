/*
** header.c for aff in /home/lequeu_m/
**
** Made by martin lequeux-gruninger
** Login   <lequeu_m@epitech.net>
**
** Started on  Fri May 10 17:38:41 2013 martin lequeux-gruninger
** Last update Wed May 15 12:43:50 2013 julien edmond
*/

/*
**       #include <unistd.h>
**
**       ssize_t read(int fd, void *buf, size_t count);
**
*/

#include <unistd.h>
#include <unistd.h>
#include "aff.h"
#include "strace.h"

void		print_args_read(struct user_regs_struct *regs, pid_t pid)
{
  (void)pid;
  fprintf(stderr, "read(");
  fprintf(stderr, "%d, %p, %lu", (int)regs->rdi, (void *)regs->rsi,
	  (size_t)regs->rdx);
  fprintf(stderr, ")\n");
}

void		print_ret_read(struct user_regs_struct *regs, pid_t pid)
{
  (void)pid;
  fprintf(stderr, "|Ret----read--->(");
  if ((ssize_t)regs->rax < 0)
    print_errno((int)regs->rax);
  else
    fprintf(stderr, "%ld", regs->rax);
  fprintf(stderr, ")\n");
}
