/*
** header.c for aff in /home/lequeu_m/
**
** Made by martin lequeux-gruninger
** Login   <lequeu_m@epitech.net>
**
** Started on  Fri May 10 17:38:41 2013 martin lequeux-gruninger
** Last update Thu May 16 09:35:29 2013 julien edmond
*/

/*
**       #include <unistd.h>
**
**       int link(const char *oldpath, const char *newpath);
**
*/

#include <unistd.h>
#include <unistd.h>
#include "aff.h"
#include "strace.h"

void		print_args_link(struct user_regs_struct *regs, pid_t pid)
{
  (void)pid;
  fprintf(stderr, "link(");
  print_string(pid, (void *)regs->rdi);
  fputs(", ", stderr);
  print_string(pid, (void *)regs->rsi);
  fprintf(stderr, ")\n");
}

void		print_ret_link(struct user_regs_struct *regs, pid_t pid)
{
  (void)pid;
  fprintf(stderr, "|Ret----link--->(");
  print_errno((int)regs->rax);
  fprintf(stderr, ")\n");
}
