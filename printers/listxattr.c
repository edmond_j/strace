/*
** header.c for aff in /home/lequeu_m/
**
** Made by martin lequeux-gruninger
** Login   <lequeu_m@epitech.net>
**
** Started on  Fri May 10 17:38:41 2013 martin lequeux-gruninger
** Last update Thu May 16 09:34:14 2013 julien edmond
*/

/*
**       #include <sys/types.h>
**       #include <attr/xattr.h>
**
**       ssize_t listxattr(const char *path, char *list, size_t size);
**       ssize_t llistxattr(const char *path, char *list, size_t size);
**       ssize_t flistxattr(int fd, char *list, size_t size);
**
*/

#include <sys/types.h>
#include <unistd.h>
#include "aff.h"
#include "strace.h"

void		print_args_listxattr(struct user_regs_struct *regs, pid_t pid)
{
  fprintf(stderr, "listxattr(");
  print_string(pid, (void *)regs->rdi);
  fprintf(stderr, "%p, %lu", (char *)regs->rsi, (size_t)regs->rdx);
  fprintf(stderr, ")\n");
}

void		print_ret_listxattr(struct user_regs_struct *regs, pid_t pid)
{
  (void)pid;
  fprintf(stderr, "|Ret----listxattr--->(");
  if ((ssize_t)regs->rax < 0)
    print_errno((int)regs->rax);
  else
    fprintf(stderr, "%ld", (ssize_t)regs->rax);
  fprintf(stderr, ")\n");
}
