/*
** header.c for aff in /home/lequeu_m/
**
** Made by martin lequeux-gruninger
** Login   <lequeu_m@epitech.net>
**
** Started on  Fri May 10 17:38:41 2013 martin lequeux-gruninger
** Last update Mon May 13 15:00:31 2013 julien edmond
*/

/*
**       #include <sys/types.h>
**       #include <utime.h>
**
**       int utime(const char *filename, const struct utimbuf *times);
**
**       #include <sys/time.h>
**
**       int utimes(const char *filename, const struct timeval times[2]);
**
*/

#include <sys/types.h>
#include <utime.h>
#include <sys/time.h>
#include <unistd.h>
#include "aff.h"
#include "strace.h"

void			print_args_utime(struct user_regs_struct *regs,
					 pid_t pid)
{
  struct utimbuf	buf;

  if (regs->rsi)
    read_from_son(pid, (void *)regs->rsi, &buf, sizeof(buf));
  fprintf(stderr, "utime(");
  print_string(pid, (void *)regs->rdi);
  if (regs->rsi)
    fprintf(stderr, ", { %lu, %lu }", buf.actime, buf.modtime);
  else
    fprintf(stderr, ", NULL");
  fprintf(stderr, ")\n");
}

void		print_ret_utime(struct user_regs_struct *regs, pid_t pid)
{
  (void)pid;
  fprintf(stderr, "|Ret----utime--->(");
  print_errno((int)regs->rax);
  fprintf(stderr, ")\n");
}
