/*
** header.c for aff in /home/lequeu_m/
**
** Made by martin lequeux-gruninger
** Login   <lequeu_m@epitech.net>
**
** Started on  Fri May 10 17:38:41 2013 martin lequeux-gruninger
** Last update Mon May 13 15:17:20 2013 julien edmond
*/

/*
**       #include <sys/utsname.h>
**
**       int uname(struct utsname *buf);
**
*/

#include <sys/utsname.h>
#include <unistd.h>
#include "aff.h"
#include "strace.h"

void		print_args_uname(struct user_regs_struct *regs, pid_t pid)
{
  (void)pid;
  fprintf(stderr, "uname(");
  fprintf(stderr, "%p", (void *)regs->rdi);
  fprintf(stderr, ")\n");
}

void		print_ret_uname(struct user_regs_struct *regs, pid_t pid)
{
  (void)pid;
  fprintf(stderr, "|Ret----uname--->(");
  print_errno((int)regs->rax);
  fprintf(stderr, ")\n");
}
