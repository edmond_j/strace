/*
** header.c for aff in /home/lequeu_m/
**
** Made by martin lequeux-gruninger
** Login   <lequeu_m@epitech.net>
**
** Started on  Fri May 10 17:38:41 2013 martin lequeux-gruninger
** Last update Wed May 15 12:31:41 2013 julien edmond
*/

/*
**       #include <sys/types.h>
**       #include <sys/socket.h>
**
**       ssize_t recv(int sockfd, void *buf, size_t len, int flags);
**
**       ssize_t recvfrom(int sockfd, void *buf, size_t len, int flags,
**                        struct sockaddr *src_addr, socklen_t *addrlen);
**
**       ssize_t recvmsg(int sockfd, struct msghdr *msg, int flags);
**
*/

#include <sys/types.h>
#include <sys/socket.h>
#include <unistd.h>
#include "aff.h"
#include "strace.h"

void		print_args_recvfrom(struct user_regs_struct *regs, pid_t pid)
{
  (void)pid;
  fprintf(stderr, "recvfrom(");
  fprintf(stderr, "%d, %p, %lu, %d, %p, %p", (int)regs->rdi, (void *)regs->rsi,
	  (size_t)regs->rdx, (int)regs->rcx, (void *)regs->r8,
	  (void *)regs->r9);
  fprintf(stderr, ")\n");
}

void		print_ret_recvfrom(struct user_regs_struct *regs, pid_t pid)
{
  (void)pid;
  fprintf(stderr, "|Ret----recvfrom--->(");
  if ((ssize_t)regs->rax < 0)
    print_errno((int)regs->rax);
  else
    fprintf(stderr, "%ld", (ssize_t)regs->rax);
  fprintf(stderr, ")\n");
}
