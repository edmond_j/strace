/*
** header.c for aff in /home/lequeu_m/
**
** Made by martin lequeux-gruninger
** Login   <lequeu_m@epitech.net>
**
** Started on  Fri May 10 17:38:41 2013 martin lequeux-gruninger
** Last update Wed May 15 12:43:07 2013 julien edmond
*/

/*
**       #include <fcntl.h>  Definition of AT_* constants
**       #include <unistd.h>
**
**       int readlinkat(int dirfd, const char *pathname,
**                      char *buf, size_t bufsiz);
**
**   Feature Test Macro Requirements for glibc (see feature_test_macros(7)):
**
**       readlinkat():
**           Since glibc 2.10:
**               _XOPEN_SOURCE >= 700 || _POSIX_C_SOURCE >= 200809L
**           Before glibc 2.10:
**               _ATFILE_SOURCE
**
*/

#include <fcntl.h>
#include <unistd.h>
#include <unistd.h>
#include "aff.h"
#include "strace.h"

void		print_args_readlinkat(struct user_regs_struct *regs, pid_t pid)
{
  fprintf(stderr, "readlinkat(");
  fprintf(stderr, "%d, ", (int)regs->rdi);
  print_string(pid, (void *)regs->rsi);
  fprintf(stderr, ", %p, %lu", (char *)regs->rdx, (size_t)regs->rcx);
  fprintf(stderr, ")\n");
}

void		print_ret_readlinkat(struct user_regs_struct *regs, pid_t pid)
{
  (void)pid;
  fprintf(stderr, "|Ret----readlinkat--->(");
  print_errno((int)regs->rax);
  fprintf(stderr, ")\n");
}
