/*
** header.c for aff in /home/lequeu_m/
**
** Made by martin lequeux-gruninger
** Login   <lequeu_m@epitech.net>
**
** Started on  Fri May 10 17:38:41 2013 martin lequeux-gruninger
** Last update Wed May 15 17:33:46 2013 julien edmond
*/

/*
**       #include <sys/types.h>
**       #include <sys/stat.h>
**       #include <fcntl.h>
**       #include <unistd.h>
**
**       int mknod(const char *pathname, mode_t mode, dev_t dev);
**
**   Feature Test Macro Requirements for glibc (see feature_test_macros(7)):
**
**       mknod():
**           _BSD_SOURCE || _SVID_SOURCE || _XOPEN_SOURCE >= 500 ||
**           _XOPEN_SOURCE && _XOPEN_SOURCE_EXTENDED
**
*/

#include <sys/types.h>
#include <sys/stat.h>
#include <fcntl.h>
#include <unistd.h>
#include <unistd.h>
#include "aff.h"
#include "strace.h"

void		print_args_mknod(struct user_regs_struct *regs, pid_t pid)
{
  fprintf(stderr, "mknod(");
  print_string(pid, (void *)regs->rdi);
  fprintf(stderr, ", %04o, %lu", (mode_t)regs->rsi, (dev_t)regs->rdx);
  fprintf(stderr, ")\n");
}

void		print_ret_mknod(struct user_regs_struct *regs, pid_t pid)
{
  (void)pid;
  fprintf(stderr, "|Ret----mknod--->(");
  print_errno((int)regs->rax);
  fprintf(stderr, ")\n");
}
