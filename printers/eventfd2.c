/*
** header.c for aff in /home/lequeu_m/
**
** Made by martin lequeux-gruninger
** Login   <lequeu_m@epitech.net>
**
** Started on  Fri May 10 17:38:41 2013 martin lequeux-gruninger
** Last update Thu May 16 10:25:17 2013 martin lequeux-gruninger
*/

/*
**       #include <sys/eventfd.h>
**
**       int eventfd(unsigned int initval, int flags);
**
*/

#include <sys/eventfd.h>
#include <unistd.h>
#include "aff.h"
#include "strace.h"

void		print_args_eventfd2(struct user_regs_struct *regs, pid_t pid)
{
  (void)pid;
  fprintf(stderr, "eventfd2(");
  fprintf(stderr, "%u", (unsigned)regs->rdi);
  fprintf(stderr, ", %d", (int)regs->rsi);
  fprintf(stderr, ")\n");
}

void		print_ret_eventfd2(struct user_regs_struct *regs, pid_t pid)
{
  (void)pid;
  fprintf(stderr, "|Ret----eventfd2--->(");
  print_errno((int)regs->rax);
  fprintf(stderr, ")\n");
}
