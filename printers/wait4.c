/*
** header.c for aff in /home/lequeu_m/
**
** Made by martin lequeux-gruninger
** Login   <lequeu_m@epitech.net>
**
** Started on  Fri May 10 17:38:41 2013 martin lequeux-gruninger
** Last update Mon May 13 18:14:31 2013 julien edmond
*/

/*
**       #include <sys/types.h>
**       #include <sys/time.h>
**       #include <sys/resource.h>
**       #include <sys/wait.h>
**
**       pid_t wait3(int *status, int options,
**                   struct rusage *rusage);
**
**       pid_t wait4(pid_t pid, int *status, int options,
**                   struct rusage *rusage);
**
**   Feature Test Macro Requirements for glibc (see feature_test_macros(7)):
**
**       wait3():
**           _BSD_SOURCE || _XOPEN_SOURCE >= 500 ||
**           _XOPEN_SOURCE && _XOPEN_SOURCE_EXTENDED
**       wait4():
**           _BSD_SOURCE
**
*/

#include <sys/types.h>
#include <sys/time.h>
#include <sys/resource.h>
#include <sys/wait.h>
#include <unistd.h>
#include "aff.h"
#include "strace.h"

void		print_args_wait4(struct user_regs_struct *regs, pid_t pid)
{
  (void)pid;
  fprintf(stderr, "wait4(");
  fprintf(stderr, "%d, %p, %d, %p",
	  (pid_t)regs->rdi, (int *)regs->rsi, (int)regs->rdx,
	  (struct rusage *)regs->rcx);
  fprintf(stderr, ")\n");
}

void		print_ret_wait4(struct user_regs_struct *regs, pid_t pid)
{
  (void)pid;
  fprintf(stderr, "|Ret----wait4--->(");
  print_errno((int)regs->rax);
  fprintf(stderr, ")\n");
}
