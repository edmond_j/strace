/*
** header.c for aff in /home/lequeu_m/
**
** Made by martin lequeux-gruninger
** Login   <lequeu_m@epitech.net>
**
** Started on  Fri May 10 17:38:41 2013 martin lequeux-gruninger
** Last update Tue May 14 08:15:23 2013 julien edmond
*/

/*
**       #include <sys/timerfd.h>
**
**       int timerfd_create(int clockid, int flags);
**
**       int timerfd_settime(int fd, int flags,
**                           const struct itimerspec *new_value,
**                           struct itimerspec *old_value);
**
**       int timerfd_gettime(int fd, struct itimerspec *curr_value);
**
*/

#include <sys/timerfd.h>
#include <unistd.h>
#include "aff.h"
#include "strace.h"

void		print_args_timerfd_settime(struct user_regs_struct *regs,
					   pid_t pid)
{
  struct itimerspec	new_value;

  if (regs->rdx)
    read_from_son(pid, (void *)regs->rdx, &new_value, sizeof(new_value));
  fprintf(stderr, "timerfd_settime(");
  fprintf(stderr, "%d, %d, ", (int)regs->rdi, (int)regs->rsi);
  if (regs->rdx)
    fprintf(stderr, "{ {%lu, %ld}, {%lu, %ld} }",
	    new_value.it_interval.tv_sec, new_value.it_interval.tv_nsec,
	    new_value.it_value.tv_sec, new_value.it_value.tv_nsec);
  else
    fprintf(stderr, "NULL");
  fprintf(stderr, ", %p", (void *)regs->rcx);
  fprintf(stderr, ")\n");
}

void		print_ret_timerfd_settime(struct user_regs_struct *regs,
					  pid_t pid)
{
  (void)pid;
  fprintf(stderr, "|Ret----timerfd_settime--->(");
  print_errno((int)regs->rax);
  fprintf(stderr, ")\n");
}
