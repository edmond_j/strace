/*
** header.c for aff in /home/lequeu_m/
**
** Made by martin lequeux-gruninger
** Login   <lequeu_m@epitech.net>
**
** Started on  Fri May 10 17:38:41 2013 martin lequeux-gruninger
** Last update Tue May 14 08:25:29 2013 julien edmond
*/

/*
**       #include <signal.h>
**       #include <time.h>
**
**       int timer_create(clockid_t clockid, struct sigevent *sevp,
**                        timer_t *timerid);
**
**       Link with -lrt.
**
**   Feature Test Macro Requirements for glibc (see feature_test_macros(7)):
**
**       timer_create(): _POSIX_C_SOURCE >= 199309L
**
*/

#include <signal.h>
#include <time.h>
#include <unistd.h>
#include "aff.h"
#include "strace.h"

const char			*get_clockid(clockid_t id)
{
  static const clockid_t	ids[] = {CLOCK_REALTIME, CLOCK_MONOTONIC,
					 CLOCK_PROCESS_CPUTIME_ID,
					 CLOCK_THREAD_CPUTIME_ID};
  static const char * const	str[] = {"CLOCK_REALTIME", "CLOCK_MONOTONIC",
					 "CLOCK_PROCESS_CPUTIME_ID",
					 "CLOCK_THREAD_CPUTIME_ID"};
  unsigned		i;
  const char		*sent;

  i = 0;
  while (i < 4 && ids[i] != id)
    ++i;
  if (i < 4)
    sent = str[i];
  else
    sent = 0;
  return (sent);
}

void		print_args_timer_create(struct user_regs_struct *regs,
					pid_t pid)
{
  const char	*id;

  (void)pid;
  fprintf(stderr, "timer_create(");
  if ((id = get_clockid((clockid_t)regs->rdi)))
    fprintf(stderr, "%s", id);
  else
    fprintf(stderr, "%d", (clockid_t)regs->rdi);
  fprintf(stderr, "%p, %p", (void *)regs->rsi, (void *)regs->rdx);
  fprintf(stderr, ")\n");
}

void		print_ret_timer_create(struct user_regs_struct *regs,
				       pid_t pid)
{
  (void)pid;
  fprintf(stderr, "|Ret----timer_create--->(");
  print_errno((int)regs->rax);
  fprintf(stderr, ")\n");
}
