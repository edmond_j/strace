/*
** header.c for aff in /home/lequeu_m/
**
** Made by martin lequeux-gruninger
** Login   <lequeu_m@epitech.net>
**
** Started on  Fri May 10 17:38:41 2013 martin lequeux-gruninger
** Last update Wed May 15 17:08:48 2013 julien edmond
*/

/*
**       #include <mqueue.h>
**
**       int mq_unlink(const char *name);
**
**       Link with -lrt.
**
*/

#include <mqueue.h>
#include <unistd.h>
#include "aff.h"
#include "strace.h"

void		print_args_mq_unlink(struct user_regs_struct *regs, pid_t pid)
{
  fprintf(stderr, "mq_unlink(");
  print_string(pid, (void *)regs->rdi);
  fprintf(stderr, ")\n");
}

void		print_ret_mq_unlink(struct user_regs_struct *regs, pid_t pid)
{
  (void)pid;
  fprintf(stderr, "|Ret----mq_unlink--->(");
  print_errno((int)regs->rax);
  fprintf(stderr, ")\n");
}
