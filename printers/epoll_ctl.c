/*
** header.c for aff in /home/lequeu_m/
**
** Made by martin lequeux-gruninger
** Login   <lequeu_m@epitech.net>
**
** Started on  Fri May 10 17:38:41 2013 martin lequeux-gruninger
** Last update Thu May 16 10:24:49 2013 martin lequeux-gruninger
*/

/*
**       #include <sys/epoll.h>
**
**       int epoll_ctl(int epfd, int op, int fd, struct epoll_event *event);
**
*/

#include <sys/epoll.h>
#include <unistd.h>
#include "aff.h"
#include "strace.h"

void		print_args_epoll_ctl(struct user_regs_struct *regs, pid_t pid)
{
  (void)pid;
  fprintf(stderr, "epoll_ctl(");
  fprintf(stderr, "%u", (int)regs->rdi);
  fprintf(stderr, ", %d", (int)regs->rsi);
  fprintf(stderr, ", %d", (int)regs->rdx);
  fprintf(stderr, ", %p", (struct epoll_event *)regs->rcx);
  fprintf(stderr, ")\n");
}

void		print_ret_epoll_ctl(struct user_regs_struct *regs, pid_t pid)
{
  (void)pid;
  fprintf(stderr, "|Ret----epoll_ctl--->(");
  print_errno((int)regs->rax);
  fprintf(stderr, ")\n");
}
