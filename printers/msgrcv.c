/*
** header.c for aff in /home/lequeu_m/
**
** Made by martin lequeux-gruninger
** Login   <lequeu_m@epitech.net>
**
** Started on  Fri May 10 17:38:41 2013 martin lequeux-gruninger
** Last update Wed May 15 17:01:44 2013 julien edmond
*/

/*
**       #include <sys/types.h>
**       #include <sys/ipc.h>
**       #include <sys/msg.h>
**
**       int msgsnd(int msqid, const void *msgp, size_t msgsz, int msgflg);
**
**       ssize_t msgrcv(int msqid, void *msgp, size_t msgsz, long msgtyp,
**                      int msgflg);
**
*/

#include <sys/types.h>
#include <sys/ipc.h>
#include <sys/msg.h>
#include <unistd.h>
#include "aff.h"
#include "strace.h"

void		print_args_msgrcv(struct user_regs_struct *regs, pid_t pid)
{
  (void)pid;
  fprintf(stderr, "msgrcv(");
  fprintf(stderr, "%d, %p, %lu, %ld, %d", (int)regs->rdi, (void *)regs->rsi,
	  (size_t)regs->rdx, (long)regs->rcx, (int)regs->r8);
  fprintf(stderr, ")\n");
}

void		print_ret_msgrcv(struct user_regs_struct *regs, pid_t pid)
{
  (void)pid;
  fprintf(stderr, "|Ret----msgrcv--->(");
  if ((ssize_t)regs->rax < 0)
    print_errno((int)regs->rax);
  else
    fprintf(stderr, "%ld", (ssize_t)regs->rax);
  fprintf(stderr, ")\n");
}
