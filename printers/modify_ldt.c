/*
** header.c for aff in /home/lequeu_m/
**
** Made by martin lequeux-gruninger
** Login   <lequeu_m@epitech.net>
**
** Started on  Fri May 10 17:38:41 2013 martin lequeux-gruninger
** Last update Wed May 15 17:28:54 2013 julien edmond
*/

/*
**       #include <sys/types.h>
**
**       int modify_ldt(int func, void *ptr, unsigned long bytecount);
**
*/

#include <sys/types.h>
#include <unistd.h>
#include "aff.h"
#include "strace.h"

void		print_args_modify_ldt(struct user_regs_struct *regs, pid_t pid)
{
  (void)pid;
  fprintf(stderr, "modify_ldt(");
  fprintf(stderr, "%d, %p, %lu", (int)regs->rdi, (void *)regs->rsi,
	  (unsigned long)regs->rdx);
  fprintf(stderr, ")\n");
}

void		print_ret_modify_ldt(struct user_regs_struct *regs, pid_t pid)
{
  (void)pid;
  fprintf(stderr, "|Ret----modify_ldt--->(");
  print_errno((int)regs->rax);
  fprintf(stderr, ")\n");
}
