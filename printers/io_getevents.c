/*
** header.c for aff in /home/lequeu_m/
**
** Made by martin lequeux-gruninger
** Login   <lequeu_m@epitech.net>
**
** Started on  Fri May 10 17:38:41 2013 martin lequeux-gruninger
** Last update Thu May 16 12:03:00 2013 martin lequeux-gruninger
*/

/*
**       #include <linux/time.h>
**       #include <libaio.h>
**
**       int io_getevents(aio_context_t ctx_id, long min_nr, long nr,
**                        struct io_event *events, struct timespec *timeout);
**
**       Link with -laio.
**
*/

#include <linux/time.h>
#include <unistd.h>
#include "aff.h"
#include "strace.h"

void		print_args_io_getevents(struct user_regs_struct *regs, pid_t pid)
{
  (void)pid;
  fprintf(stderr, "io_getevents(");
  fprintf(stderr, "%lu", (unsigned long)regs->rdi);
  fprintf(stderr, ", %ld", (long)regs->rsi);
  fprintf(stderr, ", %ld", (long)regs->rdx);
  fprintf(stderr, ", %p", (struct io_event *)regs->rcx);
  fprintf(stderr, ", %p", (struct timespec *)regs->r8);
  fprintf(stderr, ")\n");
}

void		print_ret_io_getevents(struct user_regs_struct *regs, pid_t pid)
{
  (void)pid;
  fprintf(stderr, "|Ret----io_getevents--->(");
  print_errno((int)regs->rax);
  fprintf(stderr, ")\n");
}
