/*
** header.c for aff in /home/lequeu_m/
**
** Made by martin lequeux-gruninger
** Login   <lequeu_m@epitech.net>
**
** Started on  Fri May 10 17:38:41 2013 martin lequeux-gruninger
** Last update Wed May 15 14:30:22 2013 julien edmond
*/

/*
**       #include <linux/nfsd/syscall.h>
**
**       long nfsservctl(int cmd, struct nfsctl_arg *argp,
**                       union nfsctl_res *resp);
**
*/

#include <unistd.h>
#include "aff.h"
#include "strace.h"

void		print_args_nfsservctl(struct user_regs_struct *regs, pid_t pid)
{
  (void)pid;
  fprintf(stderr, "nfsservctl(");
  fprintf(stderr, "%d, %p, %p", (int)regs->rdi, (void *)regs->rsi,
	  (void *)regs->rdx);
  fprintf(stderr, ")\n");
}

void		print_ret_nfsservctl(struct user_regs_struct *regs, pid_t pid)
{
  (void)pid;
  fprintf(stderr, "|Ret----nfsservctl--->(");
  if ((long)regs->rax < 0)
    print_errno((int)regs->rax);
  else
    fprintf(stderr, "%ld", (long)regs->rax);
  fprintf(stderr, ")\n");
}
