/*
** header.c for aff in /home/lequeu_m/
**
** Made by martin lequeux-gruninger
** Login   <lequeu_m@epitech.net>
**
** Started on  Fri May 10 17:38:41 2013 martin lequeux-gruninger
** Last update Tue May 14 11:28:03 2013 julien edmond
*/

/*
**       #include <numaif.h>
**
**       int set_mempolicy(int mode, unsigned long *nodemask,
**                         unsigned long maxnode);
**
**       Link with -lnuma.
**
*/

#include <unistd.h>
#include "aff.h"
#include "strace.h"

void		print_args_set_mempolicy(struct user_regs_struct *regs,
					 pid_t pid)
{
  (void)pid;
  fprintf(stderr, "set_mempolicy(");
  fprintf(stderr, "%d, %lu, %lu", (int)regs->rdi, (unsigned long)regs->rsi,
	  (unsigned long)regs->rdx);
  fprintf(stderr, ")\n");
}

void		print_ret_set_mempolicy(struct user_regs_struct *regs,
					pid_t pid)
{
  (void)pid;
  fprintf(stderr, "|Ret----set_mempolicy--->(");
  print_errno((int)regs->rax);
  fprintf(stderr, ")\n");
}
