/*
** header.c for aff in /home/lequeu_m/
**
** Made by martin lequeux-gruninger
** Login   <lequeu_m@epitech.net>
**
** Started on  Fri May 10 17:38:41 2013 martin lequeux-gruninger
** Last update Tue May 14 10:19:44 2013 julien edmond
*/

/*
**       #include <sys/types.h>           See NOTES
**       #include <sys/socket.h>
**
**       int socket(int domain, int type, int protocol);
**
*/

#include <sys/types.h>
#include <sys/socket.h>
#include <unistd.h>
#include "aff.h"
#include "strace.h"

void		print_args_socket(struct user_regs_struct *regs, pid_t pid)
{
  (void)pid;
  fprintf(stderr, "socket(");
  fprintf(stderr, "%d, %d, %d", (int)regs->rdi, (int)regs->rsi,
	  (int)regs->rdx);
  fprintf(stderr, ")\n");
}

void		print_ret_socket(struct user_regs_struct *regs, pid_t pid)
{
  (void)pid;
  fprintf(stderr, "|Ret----socket--->(");
  print_errno((int)regs->rax);
  fprintf(stderr, ")\n");
}
