/*
** header.c for aff in /home/lequeu_m/
**
** Made by martin lequeux-gruninger
** Login   <lequeu_m@epitech.net>
**
** Started on  Fri May 10 17:38:41 2013 martin lequeux-gruninger
** Last update Thu May 16 13:37:12 2013 julien edmond
*/

/*
**       #define _GNU_SOURCE              See feature_test_macros(7)
**       #include <sched.h>
**
**       int unshare(int flags);
**
*/

#include <sched.h>
#include <unistd.h>
#include "aff.h"
#include "strace.h"

void		print_args_get_robust_list(struct user_regs_struct *regs,
					   pid_t pid)
{
  (void)pid;
  (void)regs;
  fprintf(stderr, "get_robust_list(");
  fprintf(stderr, "UNIMPLEMENTED_SYSCALL");
  fprintf(stderr, ")\n");
}

void		print_ret_get_robust_list(struct user_regs_struct *regs,
					  pid_t pid)
{
  (void)pid;
  (void)regs;
  fprintf(stderr, "|Ret----get_robust_list--->(");
  fprintf(stderr, "UNIMPLEMENTED_SYSCALL");
  fprintf(stderr, ")\n");
}
