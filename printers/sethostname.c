/*
** header.c for aff in /home/lequeu_m/
**
** Made by martin lequeux-gruninger
** Login   <lequeu_m@epitech.net>
**
** Started on  Fri May 10 17:38:41 2013 martin lequeux-gruninger
** Last update Tue May 14 11:45:02 2013 julien edmond
*/

/*
**       #include <unistd.h>
**
**       int gethostname(char *name, size_t len);
**       int sethostname(const char *name, size_t len);
**
**   Feature Test Macro Requirements for glibc (see feature_test_macros(7)):
**
**       gethostname():
**           Since glibc 2.12: _BSD_SOURCE || _XOPEN_SOURCE >= 500
**           ||  Since glibc 2.12:  _POSIX_C_SOURCE >= 200112L
**       sethostname():
**           _BSD_SOURCE || (_XOPEN_SOURCE && _XOPEN_SOURCE < 500)
**
*/

#include <unistd.h>
#include <unistd.h>
#include "aff.h"
#include "strace.h"

void		print_args_sethostname(struct user_regs_struct *regs,
				       pid_t pid)
{
  fprintf(stderr, "sethostname(");
  print_string(pid, (void *)regs->rdi);
  fprintf(stderr, ", %lu", (size_t)regs->rsi);
  fprintf(stderr, ")\n");
}

void		print_ret_sethostname(struct user_regs_struct *regs, pid_t pid)
{
  (void)pid;
  fprintf(stderr, "|Ret----sethostname--->(");
  print_errno((int)regs->rax);
  fprintf(stderr, ")\n");
}
