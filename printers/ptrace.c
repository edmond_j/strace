/*
** header.c for aff in /home/lequeu_m/
**
** Made by martin lequeux-gruninger
** Login   <lequeu_m@epitech.net>
**
** Started on  Fri May 10 17:38:41 2013 martin lequeux-gruninger
** Last update Wed May 15 13:30:38 2013 julien edmond
*/

/*
**       #include <sys/ptrace.h>
**
**       long ptrace(enum __ptrace_request request, pid_t pid,
**                   void *addr, void *data);
**
*/

#include <sys/ptrace.h>
#include <unistd.h>
#include "aff.h"
#include "strace.h"

void		print_args_ptrace(struct user_regs_struct *regs, pid_t pid)
{
  (void)pid;
  fprintf(stderr, "ptrace(");
  fprintf(stderr, "%u, %d, %p, %p", (enum __ptrace_request)regs->rdi,
	  (pid_t)regs->rsi, (void *)regs->rdx, (void *)regs->rcx);
  fprintf(stderr, ")\n");
}

void		print_ret_ptrace(struct user_regs_struct *regs, pid_t pid)
{
  (void)pid;
  fprintf(stderr, "|Ret----ptrace--->(");
  if ((long)regs->rax < 0)
    print_errno((int)regs->rax);
  else
    fprintf(stderr, "%ld", (long)regs->rax);
  fprintf(stderr, ")\n");
}
