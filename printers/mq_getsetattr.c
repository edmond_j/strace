/*
** header.c for aff in /home/lequeu_m/
**
** Made by martin lequeux-gruninger
** Login   <lequeu_m@epitech.net>
**
** Started on  Fri May 10 17:38:41 2013 martin lequeux-gruninger
** Last update Wed May 15 17:23:37 2013 julien edmond
*/

/*
**       #include <sys/types.h>
**       #include <mqueue.h>
**
**       int mq_getsetattr(mqd_t mqdes, struct mq_attr *newattr,
**                        struct mq_attr *oldattr);
**
*/

#include <sys/types.h>
#include <mqueue.h>
#include <unistd.h>
#include "aff.h"
#include "strace.h"

void		print_args_mq_getsetattr(struct user_regs_struct *regs,
					 pid_t pid)
{
  (void)pid;
  fprintf(stderr, "mq_getsetattr(");
  fprintf(stderr, "%d, %p, %p", (mqd_t)regs->rdi, (void *)regs->rsi,
	  (void *)regs->rdx);
  fprintf(stderr, ")\n");
}

void		print_ret_mq_getsetattr(struct user_regs_struct *regs,
					pid_t pid)
{
  (void)pid;
  fprintf(stderr, "|Ret----mq_getsetattr--->(");
  print_errno((int)regs->rax);
  fprintf(stderr, ")\n");
}
