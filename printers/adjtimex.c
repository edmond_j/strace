/*
** header.c for aff in /home/lequeu_m/
**
** Made by martin lequeux-gruninger
** Login   <lequeu_m@epitech.net>
**
** Started on  Fri May 10 17:38:41 2013 martin lequeux-gruninger
** Last update Wed May 15 09:48:22 2013 martin lequeux-gruninger
*/

/*
**       #include <sys/timex.h>
**
**       int adjtimex(struct timex *buf);
**
*/

#include <sys/timex.h>
#include <unistd.h>
#include "aff.h"
#include "strace.h"

void		print_args_adjtimex(struct user_regs_struct *regs, pid_t pid)
{
  (void)pid;
  fprintf(stderr, "adjtimex(");
  fprintf(stderr, "%p", (struct timex *)regs->rdi);
  fprintf(stderr, ")\n");
}

void		print_ret_adjtimex(struct user_regs_struct *regs, pid_t pid)
{
  (void)pid;
  fprintf(stderr, "|Ret----adjtimex--->(");
  print_errno((int)regs->rax);
  fprintf(stderr, ")\n");
}
