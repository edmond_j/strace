/*
** header.c for aff in /home/lequeu_m/
**
** Made by martin lequeux-gruninger
** Login   <lequeu_m@epitech.net>
**
** Started on  Fri May 10 17:38:41 2013 martin lequeux-gruninger
** Last update Tue May 14 08:58:42 2013 julien edmond
*/

/*
**       #include <unistd.h>
**       #include <asm/page.h>  to find PAGE_SIZE
**       #include <sys/swap.h>
**
**       int swapon(const char *path, int swapflags);
**       int swapoff(const char *path);
**
*/

#include <unistd.h>
#include <sys/swap.h>
#include <unistd.h>
#include "aff.h"
#include "strace.h"

void		print_args_swapoff(struct user_regs_struct *regs, pid_t pid)
{
  (void)pid;
  fprintf(stderr, "swapoff(");
  print_string(pid, (void *)regs->rdi);
  fprintf(stderr, ")\n");
}

void		print_ret_swapoff(struct user_regs_struct *regs, pid_t pid)
{
  (void)pid;
  fprintf(stderr, "|Ret----swapoff--->(");
  print_errno((int)regs->rax);
  fprintf(stderr, ")\n");
}
