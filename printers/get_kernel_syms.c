/*
** header.c for aff in /home/lequeu_m/
**
** Made by martin lequeux-gruninger
** Login   <lequeu_m@epitech.net>
**
** Started on  Fri May 10 17:38:41 2013 martin lequeux-gruninger
** Last update Thu May 16 13:37:00 2013 julien edmond
*/

/*
**       #include <linux/module.h>
**
**       int get_kernel_syms(struct kernel_sym *table);
**
*/

#include <unistd.h>
#include "aff.h"
#include "strace.h"

void		print_args_get_kernel_syms(struct user_regs_struct *regs,
					   pid_t pid)
{
  (void)pid;
  fprintf(stderr, "get_kernel_syms(");
  fprintf(stderr, "%p", (struct kernel_sym *)regs->rdi);
  fprintf(stderr, ")\n");
}

void		print_ret_get_kernel_syms(struct user_regs_struct *regs,
					  pid_t pid)
{
  (void)pid;
  fprintf(stderr, "|Ret----get_kernel_syms--->(");
  print_errno((int)regs->rax);
  fprintf(stderr, ")\n");
}
