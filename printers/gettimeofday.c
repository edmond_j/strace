/*
** header.c for aff in /home/lequeu_m/
**
** Made by martin lequeux-gruninger
** Login   <lequeu_m@epitech.net>
**
** Started on  Fri May 10 17:38:41 2013 martin lequeux-gruninger
** Last update Wed May 15 12:55:17 2013 martin lequeux-gruninger
*/

/*
**       #include <sys/time.h>
**
**       int gettimeofday(struct timeval *tv, struct timezone *tz);
**
**       int settimeofday(const struct timeval *tv, const struct timezone *tz);
**
**   Feature Test Macro Requirements for glibc (see feature_test_macros(7)):
**
**       settimeofday(): _BSD_SOURCE
**
*/

#include <sys/time.h>
#include <unistd.h>
#include "aff.h"
#include "strace.h"

void			print_timeval_get(int pid, struct timeval *from)
{
  struct timeval	tv;

  read_from_son(pid, from, &tv, sizeof(tv));
  fprintf(stderr, "%lu, %lu", tv.tv_sec, tv.tv_usec);
}

void			print_timezone_get(int pid, struct timezone *from)
{
  struct timezone	tz;

  read_from_son(pid, from, &tz, sizeof(tz));
  fprintf(stderr, "%d, %d", tz.tz_minuteswest, tz.tz_dsttime);
}

void		print_args_gettimeofday(struct user_regs_struct *regs, pid_t pid)
{
  fprintf(stderr, "gettimeofday(");
  print_timeval_get(pid, (struct timeval *)regs->rdi);
  fputs(", ", stderr);
  print_timezone_get(pid, (struct timezone *)regs->rsi);
  fprintf(stderr, ")\n");
}

void		print_ret_gettimeofday(struct user_regs_struct *regs, pid_t pid)
{
  fprintf(stderr, "|Ret----gettimeofday--->(");
  print_errno((int)regs->rax);
  fputs(", ", stderr);
  print_timeval_get(pid, (struct timeval *)regs->rdi);
  fputs(", ", stderr);
  print_timezone_get(pid, (struct timezone *)regs->rsi);
  fprintf(stderr, ")\n");
}
