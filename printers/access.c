/*
** header.c for aff in /home/lequeu_m/
**
** Made by martin lequeux-gruninger
** Login   <lequeu_m@epitech.net>
**
** Started on  Fri May 10 17:38:41 2013 martin lequeux-gruninger
** Last update Thu May 16 10:20:11 2013 martin lequeux-gruninger
*/

/*
**       #include <unistd.h>
**
**       int access(const char *pathname, int mode);
**
*/

#include <unistd.h>
#include <unistd.h>
#include "aff.h"
#include "strace.h"

void		print_args_access(struct user_regs_struct *regs, pid_t pid)
{
  fprintf(stderr, "access(");
  print_string(pid, (void *)regs->rdi);
  fprintf(stderr, ", %d", (int)regs->rsi);
  fprintf(stderr, ")\n");
}

void		print_ret_access(struct user_regs_struct *regs, pid_t pid)
{
  (void)pid;
  fprintf(stderr, "|Ret----access--->(");
  print_errno((int)regs->rax);
  fprintf(stderr, ")\n");
}
