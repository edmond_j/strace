/*
** header.c for aff in /home/lequeu_m/
**
** Made by martin lequeux-gruninger
** Login   <lequeu_m@epitech.net>
**
** Started on  Fri May 10 17:38:41 2013 martin lequeux-gruninger
** Last update Tue May 14 14:11:06 2013 julien edmond
*/

/*
**       #include <sched.h>
**
**       int sched_yield(void);
**
*/

#include <sched.h>
#include <unistd.h>
#include "aff.h"
#include "strace.h"

void		print_args_sched_yield(struct user_regs_struct *regs, pid_t pid)
{
  (void)pid;
  (void)regs;
  fprintf(stderr, "sched_yield(");
  fprintf(stderr, ")\n");
}

void		print_ret_sched_yield(struct user_regs_struct *regs, pid_t pid)
{
  (void)pid;
  fprintf(stderr, "|Ret----sched_yield--->(");
  print_errno((int)regs->rax);
  fprintf(stderr, ")\n");
}
