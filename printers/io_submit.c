/*
** header.c for aff in /home/lequeu_m/
**
** Made by martin lequeux-gruninger
** Login   <lequeu_m@epitech.net>
**
** Started on  Fri May 10 17:38:41 2013 martin lequeux-gruninger
** Last update Thu May 16 12:03:33 2013 martin lequeux-gruninger
*/

/*
**       #include <libaio.h>
**
**       int io_submit(aio_context_t ctx_id, long nr, struct iocb **iocbpp);
**
**       Link with -laio.
**
*/

#include <unistd.h>
#include "aff.h"
#include "strace.h"

void		print_args_io_submit(struct user_regs_struct *regs, pid_t pid)
{
  (void)pid;
  fprintf(stderr, "io_submit(");
  fprintf(stderr, "%lu", (long unsigned)regs->rdi);
  fprintf(stderr, ", %ld", (long)regs->rsi);
  fprintf(stderr, ", %p", (struct iocb **)regs->rdx);
  fprintf(stderr, ")\n");
}

void		print_ret_io_submit(struct user_regs_struct *regs, pid_t pid)
{
  (void)pid;
  fprintf(stderr, "|Ret----io_submit--->(");
  print_errno((int)regs->rax);
  fprintf(stderr, ")\n");
}
