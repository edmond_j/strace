/*
** header.c for aff in /home/lequeu_m/
**
** Made by martin lequeux-gruninger
** Login   <lequeu_m@epitech.net>
**
** Started on  Fri May 10 17:38:41 2013 martin lequeux-gruninger
** Last update Thu May 16 09:22:37 2013 martin lequeux-gruninger
*/

/*
**       #include <linux/getcpu.h>
**
**       int getcpu(unsigned *cpu, unsigned *node, struct getcpu_cache *tcache);
**
*/

#include <unistd.h>
#include "aff.h"
#include "strace.h"

void		print_args_getcpu(struct user_regs_struct *regs, pid_t pid)
{
  (void)pid;
  fprintf(stderr, "getcpu(");
  fprintf(stderr, "%p", (unsigned *)regs->rdi);
  fprintf(stderr, ", %p", (unsigned *)regs->rsi);
  fprintf(stderr, ", %p", (struct getcpu_cache *)regs->rdx);
  fprintf(stderr, ")\n");
}

void		print_ret_getcpu(struct user_regs_struct *regs, pid_t pid)
{
  (void)pid;
  fprintf(stderr, "|Ret----getcpu--->(");
  print_errno((int)regs->rax);
  fprintf(stderr, ")\n");
}
