/*
** header.c for aff in /home/lequeu_m/
**
** Made by martin lequeux-gruninger
** Login   <lequeu_m@epitech.net>
**
** Started on  Fri May 10 17:38:41 2013 martin lequeux-gruninger
** Last update Wed May 15 09:56:06 2013 martin lequeux-gruninger
*/

/*
**       #include <unistd.h>
**
**       unsigned int alarm(unsigned int seconds);
**
*/

#include <unistd.h>
#include <unistd.h>
#include "aff.h"
#include "strace.h"

void		print_args_alarm(struct user_regs_struct *regs, pid_t pid)
{
  (void)pid;
  fprintf(stderr, "alarm(");
  fprintf(stderr, ", %u", (unsigned)regs->rdi);
  fprintf(stderr, ")\n");
}

void		print_ret_alarm(struct user_regs_struct *regs, pid_t pid)
{
  (void)pid;
  fprintf(stderr, "|Ret----alarm--->(");
  print_errno((unsigned)regs->rax);
  fprintf(stderr, ")\n");
}
