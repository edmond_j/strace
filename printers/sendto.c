/*
** header.c for aff in /home/lequeu_m/
**
** Made by martin lequeux-gruninger
** Login   <lequeu_m@epitech.net>
**
** Started on  Fri May 10 17:38:41 2013 martin lequeux-gruninger
** Last update Tue May 14 13:18:34 2013 julien edmond
*/

/*
**       #include <sys/types.h>
**       #include <sys/socket.h>
**
**       ssize_t send(int sockfd, const void *buf, size_t len, int flags);
**
**       ssize_t sendto(int sockfd, const void *buf, size_t len, int flags,
**                      const struct sockaddr *dest_addr, socklen_t addrlen);
**
**       ssize_t sendmsg(int sockfd, const struct msghdr *msg, int flags);
**
*/

#include <sys/types.h>
#include <sys/socket.h>
#include <unistd.h>
#include "aff.h"
#include "strace.h"

void		print_args_sendto(struct user_regs_struct *regs, pid_t pid)
{
  (void)pid;
  fprintf(stderr, "sendto(");
  fprintf(stderr, "%d, %p, %lu, %d, %p, %u", (int)regs->rdi,
	  (void *)regs->rsi, (size_t)regs->rdx, (int)regs->rcx,
	  (void *)regs->r8, (socklen_t)regs->r9);
  fprintf(stderr, ")\n");
}

void		print_ret_sendto(struct user_regs_struct *regs, pid_t pid)
{
  (void)pid;
  fprintf(stderr, "|Ret----sendto--->(");
  if ((ssize_t)regs->rax < 0)
    print_errno((int)regs->rax);
  else
    fprintf(stderr, "%ld", (ssize_t)regs->rax);
  fprintf(stderr, ")\n");
}
