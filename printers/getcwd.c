/*
** header.c for aff in /home/lequeu_m/
**
** Made by martin lequeux-gruninger
** Login   <lequeu_m@epitech.net>
**
** Started on  Fri May 10 17:38:41 2013 martin lequeux-gruninger
** Last update Thu May 16 09:29:29 2013 martin lequeux-gruninger
*/

/*
**
**        * This page documents the getcwd(2) system call, which
**        * is not defined in any user-space header files; you should
**        * use getcwd(3) defined in <unistd.h> instead in applications.
**
**
**       long getcwd(char *buf, unsigned long size);
**
**       #include <unistd.h>
**
**       char *getcwd(char *buf, size_t size);
**
**       char *getwd(char *buf);
**
**       char *get_current_dir_name(void);
**
**   Feature Test Macro Requirements for glibc (see feature_test_macros(7)):
**
**       get_current_dir_name():
**              _GNU_SOURCE
**
**       getwd():
**           Since glibc 2.12:
**               _BSD_SOURCE ||
**                   (_XOPEN_SOURCE >= 500 ||
**                       _XOPEN_SOURCE && _XOPEN_SOURCE_EXTENDED) &&
**                   !(_POSIX_C_SOURCE >= 200809L || _XOPEN_SOURCE >= 700)
**           Before glibc 2.12:
**               _BSD_SOURCE || _XOPEN_SOURCE >= 500 ||
**               _XOPEN_SOURCE && _XOPEN_SOURCE_EXTENDED
**
*/

#include <unistd.h>
#include <unistd.h>
#include "aff.h"
#include "strace.h"

void		print_args_getcwd(struct user_regs_struct *regs, pid_t pid)
{
  (void)pid;
  fprintf(stderr, "getcwd(");
  fprintf(stderr, "%p", (char *)regs->rdi);
  fprintf(stderr, ", %lu", (size_t)regs->rsi);
  fprintf(stderr, ")\n");
}

void		print_ret_getcwd(struct user_regs_struct *regs, pid_t pid)
{
  fprintf(stderr, "|Ret----getcwd--->(");
  if ((long)regs->rax < 0)
    {
      print_errno((long)regs->rax);
      fprintf(stderr, ", ");
    }
  print_string(pid, (void *)regs->rdi);
  fprintf(stderr, ")\n");
}
