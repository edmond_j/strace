/*
** header.c for aff in /home/lequeu_m/
**
** Made by martin lequeux-gruninger
** Login   <lequeu_m@epitech.net>
**
** Started on  Fri May 10 17:38:41 2013 martin lequeux-gruninger
** Last update Wed May 15 13:00:09 2013 martin lequeux-gruninger
*/

/*
**       #include <sys/time.h>
**       #include <sys/resource.h>
**
**       int getrlimit(int resource, struct rlimit *rlim);
**       int setrlimit(int resource, const struct rlimit *rlim);
**
**       int prlimit(pid_t pid, int resource, const struct rlimit *new_limit,
**                   struct rlimit *old_limit);
**
**   Feature Test Macro Requirements for glibc (see feature_test_macros(7)):
**
**       prlimit(): _GNU_SOURCE && _FILE_OFFSET_BITS == 64
**
*/

#include <sys/time.h>
#include <sys/resource.h>
#include <unistd.h>
#include "aff.h"
#include "strace.h"

void		print_rlimit(int pid, struct rlimit *from)
{
  struct rlimit to;

  read_from_son(pid, from, &to, sizeof(to));
  fprintf(stderr, "{ %lu, %lu }", to.rlim_cur, to.rlim_max);
}

void		print_args_setrlimit(struct user_regs_struct *regs, pid_t pid)
{
  fprintf(stderr, "setrlimit(");
  fprintf(stderr, "%d, ", (int)regs->rdi);
  print_rlimit(pid, (struct rlimit *)regs->rsi);
  fprintf(stderr, ")\n");
}

void		print_ret_setrlimit(struct user_regs_struct *regs, pid_t pid)
{
  (void)pid;
  fprintf(stderr, "|Ret----setrlimit--->(");
  print_errno((int)regs->rax);
  fprintf(stderr, ")\n");
}
