/*
** header.c for aff in /home/lequeu_m/
**
** Made by martin lequeux-gruninger
** Login   <lequeu_m@epitech.net>
**
** Started on  Fri May 10 17:38:41 2013 martin lequeux-gruninger
** Last update Tue May 14 11:26:17 2013 julien edmond
*/

/*
**       #define _GNU_SOURCE              See feature_test_macros(7) 
**       #include <sched.h>
**
**       int setns(int fd, int nstype);
**
*/

#include <sched.h>
#include <unistd.h>
#include "aff.h"
#include "strace.h"

void		print_args_setns(struct user_regs_struct *regs, pid_t pid)
{
  (void)pid;
  fprintf(stderr, "setns(");
  fprintf(stderr, "%d, %d", (int)regs->rdi, (int)regs->rsi);
  fprintf(stderr, ")\n");
}

void		print_ret_setns(struct user_regs_struct *regs, pid_t pid)
{
  (void)pid;
  fprintf(stderr, "|Ret----setns--->(");
  print_errno((int)regs->rax);
  fprintf(stderr, ")\n");
}
