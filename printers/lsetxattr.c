/*
** header.c for aff in /home/lequeu_m/
**
** Made by martin lequeux-gruninger
** Login   <lequeu_m@epitech.net>
**
** Started on  Fri May 10 17:38:41 2013 martin lequeux-gruninger
** Last update Thu May 16 09:23:29 2013 julien edmond
*/

/*
**       #include <sys/types.h>
**       #include <attr/xattr.h>
**
**       int setxattr(const char *path, const char *name,
**                     const void *value, size_t size, int flags);
**       int lsetxattr(const char *path, const char *name,
**                     const void *value, size_t size, int flags);
**       int fsetxattr(int fd, const char *name,
**                     const void *value, size_t size, int flags);
**
*/

#include <sys/types.h>
#include <unistd.h>
#include "aff.h"
#include "strace.h"

void		print_args_lsetxattr(struct user_regs_struct *regs, pid_t pid)
{
  fprintf(stderr, "lsetxattr(");
  print_string(pid, (void *)regs->rdi);
  fputs(", ", stderr);
  print_string(pid, (void *)regs->rsi);
  fputs(", ", stderr);
  print_string(pid, (void *)regs->rdx);
  fprintf(stderr, ", %lu, %d", (size_t)regs->rcx, (int)regs->r8);
  fprintf(stderr, ")\n");
}

void		print_ret_lsetxattr(struct user_regs_struct *regs, pid_t pid)
{
  (void)pid;
  fprintf(stderr, "|Ret----lsetxattr--->(");
  print_errno((int)regs->rax);
  fprintf(stderr, ")\n");
}
