/*
** header.c for aff in /home/lequeu_m/
**
** Made by martin lequeux-gruninger
** Login   <lequeu_m@epitech.net>
**
** Started on  Fri May 10 17:38:41 2013 martin lequeux-gruninger
** Last update Thu May 16 10:30:55 2013 martin lequeux-gruninger
*/

/*
**       #include <sys/types.h>
**       #include <sys/stat.h>
**       #include <unistd.h>
**
**       int stat(const char *path, struct stat *buf);
**       int fstat(int fd, struct stat *buf);
**       int lstat(const char *path, struct stat *buf);
**
**   Feature Test Macro Requirements for glibc (see feature_test_macros(7)):
**
**       lstat():
**           _BSD_SOURCE || _XOPEN_SOURCE >= 500 ||
**           _XOPEN_SOURCE && _XOPEN_SOURCE_EXTENDED
**           ||  Since glibc 2.10:  _POSIX_C_SOURCE >= 200112L
**
*/

#include <sys/types.h>
#include <sys/stat.h>
#include <unistd.h>
#include <unistd.h>
#include "aff.h"
#include "strace.h"

void		print_fstat(int pid, struct stat *buf)
{
  struct stat	st;

  read_from_son(pid, buf, &st, sizeof(st));
  fprintf(stderr, "{st_mode=%04o, st_size=%lu, ...}", st.st_mode,
	  st.st_size);
}

void		print_args_fstat(struct user_regs_struct *regs, pid_t pid)
{
  (void)pid;
  fprintf(stderr, "fstat(");
  fprintf(stderr, "%d", (int)regs->rdi);
  fprintf(stderr, ", %p", (struct stat *)regs->rsi);
  fprintf(stderr, ")\n");
}

void		print_ret_fstat(struct user_regs_struct *regs, pid_t pid)
{
  fprintf(stderr, "|Ret----fstat--->(");
  print_errno((int)regs->rax);
  fputs(", ", stderr);
  print_fstat(pid, (struct stat *)regs->rsi);
  fprintf(stderr, ")\n");
}
