/*
** header.c for aff in /home/lequeu_m/
**
** Made by martin lequeux-gruninger
** Login   <lequeu_m@epitech.net>
**
** Started on  Fri May 10 17:38:41 2013 martin lequeux-gruninger
** Last update Wed May 15 13:40:17 2013 julien edmond
*/

/*
**       #include <unistd.h>
**
**       ssize_t pread(int fd, void *buf, size_t count, off_t offset);
**
**       ssize_t pwrite(int fd, const void *buf, size_t count, off_t offset);
**
**   Feature Test Macro Requirements for glibc (see feature_test_macros(7)):
**
**       pread(), pwrite():
**           _XOPEN_SOURCE >= 500
**           ||  Since glibc 2.12:  _POSIX_C_SOURCE >= 200809L
**
*/

#include <unistd.h>
#include <unistd.h>
#include "aff.h"
#include "strace.h"

void		print_args_pread(struct user_regs_struct *regs, pid_t pid)
{
  (void)pid;
  fprintf(stderr, "pread(");
  fprintf(stderr, "%d, %p, %lu, 0x%lx", (int)regs->rdi, (void *)regs->rsi,
	  (size_t)regs->rdx, (off_t)regs->rcx);
  fprintf(stderr, ")\n");
}

void		print_ret_pread(struct user_regs_struct *regs, pid_t pid)
{
  (void)pid;
  fprintf(stderr, "|Ret----pread--->(");
  if ((ssize_t)regs->rax < 0)
    print_errno((int)regs->rax);
  else
    fprintf(stderr, "%ld", regs->rax);
  fprintf(stderr, ")\n");
}
