/*
** header.c for aff in /home/lequeu_m/
**
** Made by martin lequeux-gruninger
** Login   <lequeu_m@epitech.net>
**
** Started on  Fri May 10 17:38:41 2013 martin lequeux-gruninger
** Last update Thu May 16 10:09:40 2013 martin lequeux-gruninger
*/

/*
**       #include <sys/socket.h>
**
**       int getsockname(int sockfd, struct sockaddr *addr, socklen_t *addrlen);
**
*/

#include <sys/socket.h>
#include <unistd.h>
#include <netinet/in.h>
#include <arpa/inet.h>
#include "aff.h"
#include "strace.h"

void		*get_in_addr_sockname(struct sockaddr *sa)
{
  if (sa->sa_family == AF_INET)
    return &(((struct sockaddr_in*)sa)->sin_addr);
  return &(((struct sockaddr_in6*)sa)->sin6_addr);
}

void		print_args_getsockname(struct user_regs_struct *regs, pid_t pid)
{
  unsigned int		addrlen;

  fprintf(stderr, "getsockname(");
  read_from_son(pid, (unsigned int *)regs->rdx, &addrlen, sizeof(addrlen));
  fprintf(stderr, "%d", (int)regs->rdi);
  fprintf(stderr, ", %p", (struct sockaddr *)regs->rsi);
  fprintf(stderr, ", %u", addrlen);
  fprintf(stderr, ")\n");
}

void		print_ret_getsockname(struct user_regs_struct *regs, pid_t pid)
{
  struct sockaddr	addr;
  unsigned int		addrlen;
  static char		*families[13] = { "AF_UNSPEC", "AF_UNIX", "AF_INET",
					  "AF_AX25", "AF_IPX", "AF_APPLETALK",
					  "AF_NETROM", "AF_BRIDGE", "AF_AAL5",
					  "AF_X25", "AF_INET6", "AF_MAX",
					  NULL };
  int			i;
  char			client_ip[INET6_ADDRSTRLEN];

  fprintf(stderr, "|Ret----getsockname--->(");
  print_errno((int)regs->rax);
  read_from_son(pid, (struct sockaddr *)regs->rsi, &addr, sizeof(addr));
  read_from_son(pid, (unsigned int *)regs->rdx, &addrlen, sizeof(addrlen));
  i = addr.sa_family;
  if (inet_ntop(i, get_in_addr_sockname(&addr),
		client_ip, addrlen) == NULL)
    return ;
  fprintf(stderr, ", {%s, %s}", ((i >= 0 && i <= 11) ? (families[i]) :
				 ("UNKNOWN")), client_ip);
  fprintf(stderr, ")\n");
}
