/*
** header.c for aff in /home/lequeu_m/
**
** Made by martin lequeux-gruninger
** Login   <lequeu_m@epitech.net>
**
** Started on  Fri May 10 17:38:41 2013 martin lequeux-gruninger
** Last update Wed May 15 17:21:50 2013 julien edmond
*/

/*
**       #include <fcntl.h>            For O_* constants
**       #include <sys/stat.h>         For mode constants
**       #include <mqueue.h>
**
**       mqd_t mq_open(const char *name, int oflag);
**       mqd_t mq_open(const char *name, int oflag, mode_t mode,
**                     struct mq_attr *attr);
**
**       Link with -lrt.
**
*/

#include <fcntl.h>
#include <sys/stat.h>
#include <mqueue.h>
#include <unistd.h>
#include "aff.h"
#include "strace.h"

void		print_args_mq_open(struct user_regs_struct *regs, pid_t pid)
{
  fprintf(stderr, "mq_open(");
  print_string(pid, (void *)regs->rdi);
  fputs(", ", stderr);
  print_open_flags((int)regs->rsi);
  if (((int)regs->rsi & O_CREAT) == O_CREAT)
    fprintf(stderr, ", %04o, %p", (mode_t)regs->rdx, (void *)regs->rcx);
  fprintf(stderr, ")\n");
}

void		print_ret_mq_open(struct user_regs_struct *regs, pid_t pid)
{
  (void)pid;
  fprintf(stderr, "|Ret----mq_open--->(");
  print_errno((mqd_t)regs->rax);
  fprintf(stderr, ")\n");
}
