/*
** header.c for aff in /home/lequeu_m/
**
** Made by martin lequeux-gruninger
** Login   <lequeu_m@epitech.net>
**
** Started on  Fri May 10 17:38:41 2013 martin lequeux-gruninger
** Last update Tue May 14 08:42:20 2013 julien edmond
*/

/*
**       #include <sys/sysinfo.h>
**
**       int sysinfo(struct sysinfo *info);
**
*/

#include <sys/sysinfo.h>
#include <unistd.h>
#include "aff.h"
#include "strace.h"

void		print_args_sysinfo(struct user_regs_struct *regs, pid_t pid)
{
  (void)pid;
  fprintf(stderr, "sysinfo(");
  fprintf(stderr, "%p", (void *)regs->rdi);
  fprintf(stderr, ")\n");
}

void		print_ret_sysinfo(struct user_regs_struct *regs, pid_t pid)
{
  (void)pid;
  fprintf(stderr, "|Ret----sysinfo--->(");
  print_errno((int)regs->rax);
  fprintf(stderr, ")\n");
}
