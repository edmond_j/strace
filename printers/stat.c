/*
** header.c for aff in /home/lequeu_m/
**
** Made by martin lequeux-gruninger
** Login   <lequeu_m@epitech.net>
**
** Started on  Fri May 10 17:38:41 2013 martin lequeux-gruninger
** Last update Thu May 16 09:27:19 2013 julien edmond
*/

/*
**       #include <sys/types.h>
**       #include <sys/stat.h>
**       #include <unistd.h>
**
**       int stat(const char *path, struct stat *buf);
**       int fstat(int fd, struct stat *buf);
**       int lstat(const char *path, struct stat *buf);
**
**   Feature Test Macro Requirements for glibc (see feature_test_macros(7)):
**
**       lstat():
**           _BSD_SOURCE || _XOPEN_SOURCE >= 500 ||
**           _XOPEN_SOURCE && _XOPEN_SOURCE_EXTENDED
**           ||  Since glibc 2.10:  _POSIX_C_SOURCE >= 200112L
**
*/

#include <sys/types.h>
#include <sys/stat.h>
#include <unistd.h>
#include <unistd.h>
#include "aff.h"
#include "strace.h"

void		print_stat(int pid, void *buf)
{
  struct stat	st;

  read_from_son(pid, buf, &st, sizeof(st));
  fprintf(stderr, "{st_mode=%04o, st_size=%lu, ...}", st.st_mode,
	  st.st_size);
}

void		print_args_stat(struct user_regs_struct *regs, pid_t pid)
{
  fprintf(stderr, "stat(");
  print_string(pid, (void *)regs->rdi);
  fprintf(stderr, ", %p", (struct stat *)regs->rsi);
  fprintf(stderr, ")\n");
}

void		print_ret_stat(struct user_regs_struct *regs, pid_t pid)
{
  fprintf(stderr, "|Ret----stat--->(");
  print_errno((int)regs->rax);
  if ((int)regs->rax >= 0)
    {
      fputs(", ", stderr);
      print_stat(pid, (struct stat *)regs->rsi);
    }
  fprintf(stderr, ")\n");
}
