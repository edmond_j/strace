/*
** header.c for aff in /home/lequeu_m/
**
** Made by martin lequeux-gruninger
** Login   <lequeu_m@epitech.net>
**
** Started on  Fri May 10 17:38:41 2013 martin lequeux-gruninger
** Last update Tue May 14 08:16:09 2013 julien edmond
*/

/*
**       #include <sys/timerfd.h>
**
**       int timerfd_create(int clockid, int flags);
**
**       int timerfd_settime(int fd, int flags,
**                           const struct itimerspec *new_value,
**                           struct itimerspec *old_value);
**
**       int timerfd_gettime(int fd, struct itimerspec *curr_value);
**
*/

#include <sys/timerfd.h>
#include <unistd.h>
#include "aff.h"
#include "strace.h"

void		print_args_timerfd_gettime(struct user_regs_struct *regs,
					   pid_t pid)
{
  (void)pid;
  fprintf(stderr, "timerfd_gettime(");
  fprintf(stderr, "%d, %p", (int)regs->rdi, (void *)regs->rsi);
  fprintf(stderr, ")\n");
}

void		print_ret_timerfd_gettime(struct user_regs_struct *regs,
					  pid_t pid)
{
  (void)pid;
  fprintf(stderr, "|Ret----timerfd_gettime--->(");
  print_errno((int)regs->rax);
  fprintf(stderr, ")\n");
}
