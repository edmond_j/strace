/*
** header.c for aff in /home/lequeu_m/
**
** Made by martin lequeux-gruninger
** Login   <lequeu_m@epitech.net>
**
** Started on  Fri May 10 17:38:41 2013 martin lequeux-gruninger
** Last update Tue May 14 13:59:00 2013 julien edmond
*/

/*
**       #include <unistd.h>
**
**       void sync(void);
**
**       void syncfs(int fd);
**
**   Feature Test Macro Requirements for glibc (see feature_test_macros(7)):
**
**       sync():
**           _BSD_SOURCE || _XOPEN_SOURCE >= 500 ||
**           _XOPEN_SOURCE && _XOPEN_SOURCE_EXTENDED
**
**       syncfs():
**           _GNU_SOURCE
**
*/

#include <unistd.h>
#include <unistd.h>
#include "aff.h"
#include "strace.h"

void		print_args_sendmmsg(struct user_regs_struct *regs, pid_t pid)
{
  (void)pid;
  fprintf(stderr, "sendmmsg(");
  fprintf(stderr, "%d, %p, %u, %u", (int)regs->rdi, (void *)regs->rsi,
	  (unsigned)regs->rdx, (unsigned)regs->rcx);
  fprintf(stderr, ")\n");
}

void		print_ret_sendmmsg(struct user_regs_struct *regs, pid_t pid)
{
  (void)pid;
  fprintf(stderr, "|Ret----sendmmsg--->(");
  print_errno((int)regs->rax);
  fprintf(stderr, ")\n");
}
