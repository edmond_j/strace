/*
** header.c for aff in /home/lequeu_m/
**
** Made by martin lequeux-gruninger
** Login   <lequeu_m@epitech.net>
**
** Started on  Fri May 10 17:38:41 2013 martin lequeux-gruninger
** Last update Tue May 14 09:26:04 2013 julien edmond
*/

/*
**       #define _GNU_SOURCE          See feature_test_macros(7)
**       #include <fcntl.h>
**
**       ssize_t splice(int fd_in, loff_t *off_in, int fd_out,
**                      loff_t *off_out, size_t len, unsigned int flags);
**
*/

#include <fcntl.h>
#include <unistd.h>
#include "aff.h"
#include "strace.h"

void		print_args_splice(struct user_regs_struct *regs, pid_t pid)
{
  (void)pid;
  fprintf(stderr, "splice(");
  fprintf(stderr, "%d, %p, %d, %p, %lu, %u",
	  (int)regs->rdi, (loff_t *)regs->rsi, (int)regs->rdx,
	  (loff_t *)regs->rcx, (size_t)regs->r8, (unsigned)regs->r9);
  fprintf(stderr, ")\n");
}

void		print_ret_splice(struct user_regs_struct *regs, pid_t pid)
{
  (void)pid;
  fprintf(stderr, "|Ret----splice--->(");
  print_errno((int)regs->rax);
  fprintf(stderr, ")\n");
}
