/*
** header.c for aff in /home/lequeu_m/
**
** Made by martin lequeux-gruninger
** Login   <lequeu_m@epitech.net>
**
** Started on  Fri May 10 17:38:41 2013 martin lequeux-gruninger
** Last update Thu May 16 10:11:49 2013 martin lequeux-gruninger
*/

/*
**       #include <sys/types.h>           See NOTES
**       #include <sys/socket.h>
**
**       int getsockopt(int sockfd, int level, int optname,
**                      void *optval, socklen_t *optlen);
**       int setsockopt(int sockfd, int level, int optname,
**                      const void *optval, socklen_t optlen);
**
*/

#include <sys/types.h>
#include <sys/socket.h>
#include <unistd.h>
#include <netinet/in.h>
#include <arpa/inet.h>
#include "aff.h"
#include "strace.h"

void		print_args_getsockopt(struct user_regs_struct *regs, pid_t pid)
{
  (void)pid;
  fprintf(stderr, "getsockopt(");
  fprintf(stderr, "%d", (int)regs->rdi);
  fprintf(stderr, ", %d", (int)regs->rsi);
  fprintf(stderr, ", %p", (void *)regs->rdx);
  fprintf(stderr, ", %p", (socklen_t *)regs->rcx);
  fprintf(stderr, ")\n");
}

void		print_ret_getsockopt(struct user_regs_struct *regs, pid_t pid)
{
  (void)pid;
  fprintf(stderr, "|Ret----getsockopt--->(");
  print_errno((int)regs->rax);
  fprintf(stderr, ")\n");
}
