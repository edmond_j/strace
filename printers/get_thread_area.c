/*
** header.c for aff in /home/lequeu_m/
**
** Made by martin lequeux-gruninger
** Login   <lequeu_m@epitech.net>
**
** Started on  Fri May 10 17:38:41 2013 martin lequeux-gruninger
** Last update Thu May 16 13:37:25 2013 julien edmond
*/

/*
**       #include <linux/unistd.h>
**       #include <asm/ldt.h>
**
**       int get_thread_area(struct user_desc *u_info);
**
*/

#include <linux/unistd.h>
#include <asm/ldt.h>
#include <unistd.h>
#include "aff.h"
#include "strace.h"

void		print_args_get_thread_area(struct user_regs_struct *regs,
					   pid_t pid)
{
  (void)pid;
  fprintf(stderr, "get_thread_area(");
  fprintf(stderr, "%p", (struct user_desc *)regs->rdi);
  fprintf(stderr, ")\n");
}

void		print_ret_get_thread_area(struct user_regs_struct *regs,
					  pid_t pid)
{
  (void)pid;
  fprintf(stderr, "|Ret----get_thread_area--->(");
  print_errno((int)regs->rax);
  fprintf(stderr, ")\n");
}
