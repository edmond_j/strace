/*
** header.c for aff in /home/lequeu_m/
**
** Made by martin lequeux-gruninger
** Login   <lequeu_m@epitech.net>
**
** Started on  Fri May 10 17:38:41 2013 martin lequeux-gruninger
** Last update Mon May 13 15:09:24 2013 julien edmond
*/

/*
**       #include <unistd.h>
**
**       int unlink(const char *pathname);
**
*/

#include <unistd.h>
#include <unistd.h>
#include "aff.h"
#include "strace.h"

void		print_args_unlink(struct user_regs_struct *regs, pid_t pid)
{
  fprintf(stderr, "unlink(");
  print_string(pid, (void *)regs->rdi);
  fprintf(stderr, ")\n");
}

void		print_ret_unlink(struct user_regs_struct *regs, pid_t pid)
{
  (void)pid;
  fprintf(stderr, "|Ret----unlink--->(");
  print_errno((int)regs->rax);
  fprintf(stderr, ")\n");
}
