/*
** header.c for aff in /home/lequeu_m/
**
** Made by martin lequeux-gruninger
** Login   <lequeu_m@epitech.net>
**
** Started on  Fri May 10 17:38:41 2013 martin lequeux-gruninger
** Last update Thu May 16 09:40:03 2013 julien edmond
*/

/*
**       #include <unistd.h>
**
**       int chown(const char *path, uid_t owner, gid_t group);
**       int fchown(int fd, uid_t owner, gid_t group);
**       int lchown(const char *path, uid_t owner, gid_t group);
**
**   Feature Test Macro Requirements for glibc (see feature_test_macros(7)):
**
**       fchown(), lchown():
**           _BSD_SOURCE || _XOPEN_SOURCE >= 500 ||
**           _XOPEN_SOURCE && _XOPEN_SOURCE_EXTENDED
**           ||  Since glibc 2.12:  _POSIX_C_SOURCE >= 200809L
**
*/

#include <unistd.h>
#include <unistd.h>
#include "aff.h"
#include "strace.h"

void		print_args_lchown(struct user_regs_struct *regs, pid_t pid)
{
  fprintf(stderr, "lchown(");
  print_string(pid, (char *)regs->rdi);
  fprintf(stderr, ", %d, %d", (uid_t)regs->rsi, (gid_t)regs->rdx);
  fprintf(stderr, ")\n");
}

void		print_ret_lchown(struct user_regs_struct *regs, pid_t pid)
{
  (void)pid;
  fprintf(stderr, "|Ret----lchown--->(");
  print_errno((int)regs->rax);
  fprintf(stderr, ")\n");
}
