/*
** header.c for aff in /home/lequeu_m/
**
** Made by martin lequeux-gruninger
** Login   <lequeu_m@epitech.net>
**
** Started on  Fri May 10 17:38:41 2013 martin lequeux-gruninger
** Last update Tue May 14 11:09:35 2013 julien edmond
*/

/*
**       #include <linux/unistd.h>
**
**       long set_tid_address(int *tidptr);
**
*/

#include <linux/unistd.h>
#include <unistd.h>
#include "aff.h"
#include "strace.h"

void		print_args_set_tid_address(struct user_regs_struct *regs,
					   pid_t pid)
{
  (void)pid;
  fprintf(stderr, "set_tid_address(");
  fprintf(stderr, "%p", (int *)regs->rdi);
  fprintf(stderr, ")\n");
}

void		print_ret_set_tid_address(struct user_regs_struct *regs,
					  pid_t pid)
{
  (void)pid;
  fprintf(stderr, "|Ret----set_tid_address--->(");
  if ((long)regs->rax < 0)
    print_errno((int)regs->rax);
  else
    fprintf(stderr, "%ld", regs->rax);
  fprintf(stderr, ")\n");
}
