/*
** header.c for aff in /home/lequeu_m/
**
** Made by martin lequeux-gruninger
** Login   <lequeu_m@epitech.net>
**
** Started on  Fri May 10 17:38:41 2013 martin lequeux-gruninger
** Last update Thu May 16 10:32:45 2013 martin lequeux-gruninger
*/

/*
**       #include <sys/time.h>
**       #include <sys/resource.h>
**
**       int getpriority(int which, int who);
**       int setpriority(int which, int who, int prio);
**
*/

#include <sys/time.h>
#include <sys/resource.h>
#include <unistd.h>
#include "aff.h"
#include "strace.h"

void		print_args_getpriority(struct user_regs_struct *regs, pid_t pid)
{
  (void)pid;
  fprintf(stderr, "getpriority(");
  fprintf(stderr, "%d", (int)regs->rdi);
  fprintf(stderr, ", %d", (int)regs->rsi);
  fprintf(stderr, ")\n");
}

void		print_ret_getpriority(struct user_regs_struct *regs, pid_t pid)
{
  (void)pid;
  (void)regs;
  fprintf(stderr, "|Ret----getpriority--->(");
  print_errno((int)regs->rax);
  fprintf(stderr, ")\n");
}
