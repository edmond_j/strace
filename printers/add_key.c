/*
** header.c for aff in /home/lequeu_m/
**
** Made by martin lequeux-gruninger
** Login   <lequeu_m@epitech.net>
**
** Started on  Fri May 10 17:38:41 2013 martin lequeux-gruninger
** Last update Wed May 15 10:33:55 2013 martin lequeux-gruninger
*/

/*
**       #include <keyutils.h>
**
**       key_serial_t add_key(const char *type, const char *description,
**       const void *payload, size_t plen, key_serial_t keyring);
**
*/

#include <unistd.h>
#include <keyutils.h>
#include "aff.h"
#include "strace.h"

void		print_args_add_key(struct user_regs_struct *regs, pid_t pid)
{
  fprintf(stderr, "add_key(");
  print_string(pid, (void *)regs->rdi);
  fprintf(stderr, ", ");
  print_string(pid, (void *)regs->rsi);
  fprintf(stderr, ", %p", (const void *)regs->rdx);
  fprintf(stderr, ", %lu", (size_t)regs->rcx);
  fprintf(stderr, ", %d", (key_serial_t)regs->r8);
  fprintf(stderr, ")\n");
}

void		print_ret_add_key(struct user_regs_struct *regs, pid_t pid)
{
  (void)pid;
  fprintf(stderr, "|Ret----add_key--->(");
  print_errno((int)regs->rax);
  fprintf(stderr, ")\n");
}
