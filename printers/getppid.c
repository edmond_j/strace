/*
** header.c for aff in /home/lequeu_m/
**
** Made by martin lequeux-gruninger
** Login   <lequeu_m@epitech.net>
**
** Started on  Fri May 10 17:38:41 2013 martin lequeux-gruninger
** Last update Wed May 15 13:03:46 2013 martin lequeux-gruninger
*/

/*
**       #include <sys/types.h>
**       #include <unistd.h>
**
**       pid_t getpid(void);
**       pid_t getppid(void);
**
*/

#include <sys/types.h>
#include <unistd.h>
#include <unistd.h>
#include "aff.h"
#include "strace.h"

void		print_args_getppid(struct user_regs_struct *regs, pid_t pid)
{
  (void)pid;
  (void)regs;
  fprintf(stderr, "getppid(");
  fprintf(stderr, ")\n");
}

void		print_ret_getppid(struct user_regs_struct *regs, pid_t pid)
{
  (void)pid;
  fprintf(stderr, "|Ret----getppid--->(");
  print_errno((pid_t)regs->rax);
  fprintf(stderr, ")\n");
}
