/*
** header.c for aff in /home/lequeu_m/
**
** Made by martin lequeux-gruninger
** Login   <lequeu_m@epitech.net>
**
** Started on  Fri May 10 17:38:41 2013 martin lequeux-gruninger
** Last update Thu May 16 12:00:13 2013 martin lequeux-gruninger
*/

/*
**       int ioprio_get(int which, int who);
**       int ioprio_set(int which, int who, int ioprio);
**
*/

#include <unistd.h>
#include "aff.h"
#include "strace.h"

void		print_args_ioprio_get(struct user_regs_struct *regs, pid_t pid)
{
  (void)pid;
  fprintf(stderr, "ioprio_get(");
  fprintf(stderr, "%d", (int)regs->rdi);
  fprintf(stderr, ", %d", (int)regs->rsi);
  fprintf(stderr, ")\n");
}

void		print_ret_ioprio_get(struct user_regs_struct *regs, pid_t pid)
{
  (void)pid;
  fprintf(stderr, "|Ret----ioprio_get--->(");
  print_errno((int)regs->rax);
  fprintf(stderr, ")\n");
}
