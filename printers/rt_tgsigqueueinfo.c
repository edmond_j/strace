/*
** header.c for aff in /home/lequeu_m/
**
** Made by martin lequeux-gruninger
** Login   <lequeu_m@epitech.net>
**
** Started on  Fri May 10 17:38:41 2013 martin lequeux-gruninger
** Last update Wed May 15 10:25:43 2013 julien edmond
*/

/*
**       long sys_rt_sigqueueinfo(int pid, int sig, siginfo_t * uinfo);
**
**       int rt_sigqueueinfo(pid_t tgid, int sig, siginfo_t *uinfo);
**
**       int rt_tgsigqueueinfo(pid_t tgid, pid_t tid, int sig,
**                             siginfo_t *uinfo);
**
*/

#include <unistd.h>
#include "aff.h"
#include "strace.h"

void		print_args_rt_tgsigqueueinfo(struct user_regs_struct *regs,
					     pid_t pid)
{
  (void)pid;
  fprintf(stderr, "rt_tgsigqueueinfo(");
  fprintf(stderr, "%d, %d, %d, %p", (pid_t)regs->rdi, (pid_t)regs->rsi,
	  (int)regs->rdx, (void *)regs->rcx);
  fprintf(stderr, ")\n");
}

void		print_ret_rt_tgsigqueueinfo(struct user_regs_struct *regs,
					    pid_t pid)
{
  (void)pid;
  fprintf(stderr, "|Ret----rt_tgsigqueueinfo--->(");
  print_errno((int)regs->rax);
  fprintf(stderr, ")\n");
}
