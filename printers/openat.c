/*
** header.c for aff in /home/lequeu_m/
**
** Made by martin lequeux-gruninger
** Login   <lequeu_m@epitech.net>
**
** Started on  Fri May 10 17:38:41 2013 martin lequeux-gruninger
** Last update Wed May 15 14:26:22 2013 julien edmond
*/

/*
**       #include <fcntl.h>
**
**       int openat(int dirfd, const char *pathname, int flags);
**       int openat(int dirfd, const char *pathname, int flags, mode_t mode);
**
**   Feature Test Macro Requirements for glibc (see feature_test_macros(7)):
**
**       openat():
**           Since glibc 2.10:
**               _XOPEN_SOURCE >= 700 || _POSIX_C_SOURCE >= 200809L
**           Before glibc 2.10:
**               _ATFILE_SOURCE
**
*/

#include <fcntl.h>
#include <unistd.h>
#include "aff.h"
#include "strace.h"

void		print_args_openat(struct user_regs_struct *regs, pid_t pid)
{
  fprintf(stderr, "openat(");
  fprintf(stderr, "%d, ", (int)regs->rdi);
  print_string(pid, (void *)regs->rsi);
  fputs(", ", stderr);
  print_open_flags((int)regs->rdx);
  if (((int)regs->rdx & O_CREAT) == O_CREAT)
    fprintf(stderr, ", %04o", (mode_t)regs->rcx);
  fprintf(stderr, ")\n");
}

void		print_ret_openat(struct user_regs_struct *regs, pid_t pid)
{
  (void)pid;
  fprintf(stderr, "|Ret----openat--->(");
  print_errno((int)regs->rax);
  fprintf(stderr, ")\n");
}
