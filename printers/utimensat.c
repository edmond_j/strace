/*
** header.c for aff in /home/lequeu_m/
**
** Made by martin lequeux-gruninger
** Login   <lequeu_m@epitech.net>
**
** Started on  Fri May 10 17:38:41 2013 martin lequeux-gruninger
** Last update Mon May 13 14:53:14 2013 julien edmond
*/

/*
**       #include <sys/stat.h>
**
**       int utimensat(int dirfd, const char *pathname,
**                     const struct timespec times[2], int flags);
**
**       int futimens(int fd, const struct timespec times[2]);
**
**   Feature Test Macro Requirements for glibc (see feature_test_macros(7)):
**
**       utimensat():
**           Since glibc 2.10:
**               _XOPEN_SOURCE >= 700 || _POSIX_C_SOURCE >= 200809L
**           Before glibc 2.10:
**               _ATFILE_SOURCE
**       futimens():
**           Since glibc 2.10:
**                  _XOPEN_SOURCE >= 700 || _POSIX_C_SOURCE >= 200809L
**           Before glibc 2.10:
**                  _GNU_SOURCE
**
*/

#include <sys/stat.h>
#include <unistd.h>
#include "aff.h"
#include "strace.h"

void			print_args_utimensat(struct user_regs_struct *regs,
					     pid_t pid)
{
  struct timespec	times[2];

  fprintf(stderr, "utimensat(");
  fprintf(stderr, "%d, ", (int)regs->rdi);
  read_from_son(pid, (void *)regs->rdx, times, sizeof(times));
  print_string(pid, (void *)regs->rsi);
  fprintf(stderr, "[ {%lu, %ld} | {%lu, %ld} ], %d",
	  times[0].tv_sec, times[0].tv_nsec, times[1].tv_sec, times[1].tv_nsec,
	  (int)regs->rcx);
  fprintf(stderr, ")\n");
}

void		print_ret_utimensat(struct user_regs_struct *regs, pid_t pid)
{
  (void)pid;
  fprintf(stderr, "|Ret----utimensat--->(");
  print_errno((int)regs->rax);
  fprintf(stderr, ")\n");
}
