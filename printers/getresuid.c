/*
** header.c for aff in /home/lequeu_m/
**
** Made by martin lequeux-gruninger
** Login   <lequeu_m@epitech.net>
**
** Started on  Fri May 10 17:38:41 2013 martin lequeux-gruninger
** Last update Thu May 16 10:17:29 2013 martin lequeux-gruninger
*/

/*
**       #define _GNU_SOURCE          See feature_test_macros(7)
**       #include <unistd.h>
**
**       int getresuid(uid_t *ruid, uid_t *euid, uid_t *suid);
**       int getresgid(gid_t *rgid, gid_t *egid, gid_t *sgid);
**
*/

#include <unistd.h>
#include <unistd.h>
#include "aff.h"
#include "strace.h"

void		print_args_getresuid(struct user_regs_struct *regs, pid_t pid)
{
  (void)pid;
  fprintf(stderr, "getresuid(");
  fprintf(stderr, "%p", (uid_t *)regs->rdi);
  fprintf(stderr, ", %p", (uid_t *)regs->rsi);
  fprintf(stderr, ", %p", (uid_t *)regs->rdx);
  fprintf(stderr, ")\n");
}

void		print_ret_getresuid(struct user_regs_struct *regs, pid_t pid)
{
  (void)pid;
  fprintf(stderr, "|Ret----getresuid--->(");
  print_errno((int)regs->rax);
  fprintf(stderr, ")\n");
}
