/*
** header.c for aff in /home/lequeu_m/
**
** Made by martin lequeux-gruninger
** Login   <lequeu_m@epitech.net>
**
** Started on  Fri May 10 17:38:41 2013 martin lequeux-gruninger
** Last update Thu May 16 10:31:44 2013 martin lequeux-gruninger
*/

/*
**       #include <unistd.h>
**       #include <sys/types.h>
**
**       int truncate(const char *path, off_t length);
**       int ftruncate(int fd, off_t length);
**
**   Feature Test Macro Requirements for glibc (see feature_test_macros(7)):
**
**       truncate():
**           _BSD_SOURCE || _XOPEN_SOURCE >= 500 ||
**           _XOPEN_SOURCE && _XOPEN_SOURCE_EXTENDED
**           ||  Since glibc 2.12:  _POSIX_C_SOURCE >= 200809L
**
**       ftruncate():
**           _BSD_SOURCE || _XOPEN_SOURCE >= 500 ||
**           _XOPEN_SOURCE && _XOPEN_SOURCE_EXTENDED
**           ||  Since glibc 2.3.5:  _POSIX_C_SOURCE >= 200112L
**
*/

#include <unistd.h>
#include <sys/types.h>
#include <unistd.h>
#include "aff.h"
#include "strace.h"

void		print_args_ftruncate(struct user_regs_struct *regs, pid_t pid)
{
  (void)pid;
  fprintf(stderr, "ftruncate(");
  fprintf(stderr, "%d", (int)regs->rdi);
  fprintf(stderr, ", %ld", (off_t)regs->rsi);
  fprintf(stderr, ")\n");
}

void		print_ret_ftruncate(struct user_regs_struct *regs, pid_t pid)
{
  (void)pid;
  fprintf(stderr, "|Ret----ftruncate--->(");
  print_errno((int)regs->rax);
  fprintf(stderr, ")\n");
}
