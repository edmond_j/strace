/*
** header.c for aff in /home/lequeu_m/
**
** Made by martin lequeux-gruninger
** Login   <lequeu_m@epitech.net>
**
** Started on  Fri May 10 17:38:41 2013 martin lequeux-gruninger
** Last update Mon May 13 15:08:32 2013 julien edmond
*/

/*
**       #define _GNU_SOURCE              See feature_test_macros(7)
**       #include <sched.h>
**
**       int unshare(int flags);
**
*/

#include <sched.h>
#include <unistd.h>
#include "aff.h"
#include "strace.h"

void		print_args_unshare(struct user_regs_struct *regs, pid_t pid)
{
  (void)pid;
  fprintf(stderr, "unshare(");
  fprintf(stderr, "%d", (int)regs->rdi);
  fprintf(stderr, ")\n");
}

void		print_ret_unshare(struct user_regs_struct *regs, pid_t pid)
{
  (void)pid;
  fprintf(stderr, "|Ret----unshare--->(");
  print_errno((int)regs->rax);
  fprintf(stderr, ")\n");
}
