/*
** header.c for aff in /home/lequeu_m/
**
** Made by martin lequeux-gruninger
** Login   <lequeu_m@epitech.net>
**
** Started on  Fri May 10 17:38:41 2013 martin lequeux-gruninger
** Last update Wed May 15 14:24:11 2013 julien edmond
*/

/*
**       #define _GNU_SOURCE
**       #include <sys/socket.h>
**
**       int recvmmsg(int sockfd, struct mmsghdr *msgvec, unsigned int vlen,
**                    unsigned int flags, struct timespec *timeout);
**
*/

#include <sys/socket.h>
#include <unistd.h>
#include "aff.h"
#include "strace.h"

void		print_args_open_by_handle_at(struct user_regs_struct *regs,
					     pid_t pid)
{
  (void)pid;
  fprintf(stderr, "open_by_handle_at(");
  fprintf(stderr, "%d, %p, %d", (int)regs->rdi, (void *)regs->rsi,
	  (int)regs->rdx);
  fprintf(stderr, ")\n");
}

void		print_ret_open_by_handle_at(struct user_regs_struct *regs,
					    pid_t pid)
{
  (void)pid;
  fprintf(stderr, "|Ret----open_by_handle_at--->(");
  print_errno((int)regs->rax);
  fprintf(stderr, ")\n");
}
