/*
** header.c for aff in /home/lequeu_m/
**
** Made by martin lequeux-gruninger
** Login   <lequeu_m@epitech.net>
**
** Started on  Fri May 10 17:38:41 2013 martin lequeux-gruninger
** Last update Tue May 14 11:08:45 2013 julien edmond
*/

/*
**       #define _GNU_SOURCE          See feature_test_macros(7)
**       #include <fcntl.h>
**
**       ssize_t tee(int fd_in, int fd_out, size_t len, unsigned int flags);
**
*/

#include <fcntl.h>
#include <unistd.h>
#include "aff.h"
#include "strace.h"

void		print_args_tee(struct user_regs_struct *regs, pid_t pid)
{
  (void)pid;
  fprintf(stderr, "tee(");
  fprintf(stderr, "%d, %d, %lu, %u",
	  (int)regs->rdi, (int)regs->rsi, (size_t)regs->rdx,
	  (unsigned)regs->rcx);
  fprintf(stderr, ")\n");
}

void		print_ret_tee(struct user_regs_struct *regs, pid_t pid)
{
  (void)pid;
  fprintf(stderr, "|Ret----tee--->(");
  if ((ssize_t)regs->rax < (ssize_t)0)
    print_errno((int)regs->rax);
  else
    fprintf(stderr, "%ld", (ssize_t)regs->rax);
  fprintf(stderr, ")\n");
}
