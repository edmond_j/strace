/*
** header.c for aff in /home/lequeu_m/
**
** Made by martin lequeux-gruninger
** Login   <lequeu_m@epitech.net>
**
** Started on  Fri May 10 17:38:41 2013 martin lequeux-gruninger
** Last update Thu May 16 10:25:32 2013 martin lequeux-gruninger
*/

/*
**       #include <unistd.h>
**
**       int execve(const char *filename, char *const argv[],
**                  char *const envp[]);
**
*/

#include <unistd.h>
#include <unistd.h>
#include "aff.h"
#include "strace.h"

void		print_args_execve(struct user_regs_struct *regs, pid_t pid)
{
  (void)pid;
  fprintf(stderr, "execve(");
  print_string(pid, (void *)regs->rdi);
  fprintf(stderr, ", %p", (char *)regs->rsi);
  fprintf(stderr, ", %p", (char *)regs->rdx);
  fprintf(stderr, ")\n");
}

void		print_ret_execve(struct user_regs_struct *regs, pid_t pid)
{
  (void)pid;
  fprintf(stderr, "|Ret----execve--->(");
  print_errno((int)regs->rax);
  fprintf(stderr, ")\n");
}
