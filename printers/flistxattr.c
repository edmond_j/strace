/*
** header.c for aff in /home/lequeu_m/
**
** Made by martin lequeux-gruninger
** Login   <lequeu_m@epitech.net>
**
** Started on  Fri May 10 17:38:41 2013 martin lequeux-gruninger
** Last update Thu May 16 10:30:20 2013 martin lequeux-gruninger
*/

/*
**       #include <sys/types.h>
**       #include <attr/xattr.h>
**
**       ssize_t listxattr(const char *path, char *list, size_t size);
**       ssize_t llistxattr(const char *path, char *list, size_t size);
**       ssize_t flistxattr(int fd, char *list, size_t size);
**
*/

#include <sys/types.h>
#include <unistd.h>
#include "aff.h"
#include "strace.h"

void		print_args_flistxattr(struct user_regs_struct *regs, pid_t pid)
{
  (void)pid;
  fprintf(stderr, "flistxattr(");
  fprintf(stderr, "%d", (int)regs->rdi);
  fprintf(stderr, ", ");
  print_string(pid, (void *)regs->rsi);
  fprintf(stderr, ", %lu", (size_t)regs->rdx);
  fprintf(stderr, ")\n");
}

void		print_ret_flistxattr(struct user_regs_struct *regs, pid_t pid)
{
  (void)pid;
  fprintf(stderr, "|Ret----flistxattr--->(");
  print_errno((int)regs->rax);
  fprintf(stderr, ")\n");
}
