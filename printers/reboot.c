/*
** header.c for aff in /home/lequeu_m/
**
** Made by martin lequeux-gruninger
** Login   <lequeu_m@epitech.net>
**
** Started on  Fri May 10 17:38:41 2013 martin lequeux-gruninger
** Last update Wed May 15 12:37:23 2013 julien edmond
*/

/*
**        For libc4 and libc5 the library call and the system call
**          are identical, and since kernel version 2.1.30 there are
**          symbolic names LINUX_REBOOT_* for the constants and a
**          fourth argument to the call:
**
**       #include <unistd.h>
**       #include <linux/reboot.h>
**
**       int reboot(int magic, int magic2, int cmd, void *arg);
**
**        Under glibc some of the constants involved have gotten
**          symbolic names RB_*, and the library call is a 1-argument
**          wrapper around the 3-argument system call:
**
**       #include <unistd.h>
**       #include <sys/reboot.h>
**
**       int reboot(int cmd);
**
*/

#include <unistd.h>
#include <linux/reboot.h>
#include <unistd.h>
#include <sys/reboot.h>
#include <unistd.h>
#include "aff.h"
#include "strace.h"

void		print_args_reboot(struct user_regs_struct *regs, pid_t pid)
{
  (void)pid;
  fprintf(stderr, "reboot(");
  fprintf(stderr, "%d", (int)regs->rdi);
  if ((int)regs->rdi == (int)LINUX_REBOOT_MAGIC1
      && (int)regs->rsi == (int)LINUX_REBOOT_MAGIC2)
    fprintf(stderr, ", %d, %d, %p", (int)regs->rsi, (int)regs->rdx,
	    (void *)regs->rcx);
  fprintf(stderr, ")\n");
}

void		print_ret_reboot(struct user_regs_struct *regs, pid_t pid)
{
  (void)pid;
  fprintf(stderr, "|Ret----reboot--->(");
  print_errno((int)regs->rax);
  fprintf(stderr, ")\n");
}
