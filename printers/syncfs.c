/*
** header.c for aff in /home/lequeu_m/
**
** Made by martin lequeux-gruninger
** Login   <lequeu_m@epitech.net>
**
** Started on  Fri May 10 17:38:41 2013 martin lequeux-gruninger
** Last update Tue May 14 08:49:52 2013 julien edmond
*/

/*
**       #include <unistd.h>
**
**       void sync(void);
**
**       void syncfs(int fd);
**
**   Feature Test Macro Requirements for glibc (see feature_test_macros(7)):
**
**       sync():
**           _BSD_SOURCE || _XOPEN_SOURCE >= 500 ||
**           _XOPEN_SOURCE && _XOPEN_SOURCE_EXTENDED
**
**       syncfs():
**           _GNU_SOURCE
**
*/

#include <unistd.h>
#include <unistd.h>
#include "aff.h"
#include "strace.h"

void		print_args_syncfs(struct user_regs_struct *regs, pid_t pid)
{
  (void)pid;
  fprintf(stderr, "syncfs(");
  fprintf(stderr, "%d", (int)regs->rdi);
  fprintf(stderr, ")\n");
}

void		print_ret_syncfs(struct user_regs_struct *regs, pid_t pid)
{
  (void)pid;
  (void)regs;
  fprintf(stderr, "|Ret----syncfs--->(");
  fprintf(stderr, ")\n");
}
