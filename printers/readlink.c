/*
** header.c for aff in /home/lequeu_m/
**
** Made by martin lequeux-gruninger
** Login   <lequeu_m@epitech.net>
**
** Started on  Fri May 10 17:38:41 2013 martin lequeux-gruninger
** Last update Wed May 15 12:40:13 2013 julien edmond
*/

/*
**       #include <unistd.h>
**
**       ssize_t readlink(const char *path, char *buf, size_t bufsiz);
**
**   Feature Test Macro Requirements for glibc (see feature_test_macros(7)):
**
**       readlink():
**           _BSD_SOURCE || _XOPEN_SOURCE >= 500 ||
**           _XOPEN_SOURCE && _XOPEN_SOURCE_EXTENDED ||
**           _POSIX_C_SOURCE >= 200112L
**
*/

#include <unistd.h>
#include <unistd.h>
#include "aff.h"
#include "strace.h"

void		print_args_readlink(struct user_regs_struct *regs, pid_t pid)
{
  fprintf(stderr, "readlink(");
  print_string(pid, (void *)regs->rdi);
  fprintf(stderr, ", %p, %lu", (char *)regs->rsi, (size_t)regs->rdx);
  fprintf(stderr, ")\n");
}

void		print_ret_readlink(struct user_regs_struct *regs, pid_t pid)
{
  (void)pid;
  fprintf(stderr, "|Ret----readlink--->(");
  if ((ssize_t)regs->rax < 0)
    print_errno((int)regs->rax);
  else
    fprintf(stderr, "%ld", regs->rax);
  fprintf(stderr, ")\n");
}
