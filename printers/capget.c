/*
** header.c for aff in /home/lequeu_m/
**
** Made by martin lequeux-gruninger
** Login   <lequeu_m@epitech.net>
**
** Started on  Fri May 10 17:38:41 2013 martin lequeux-gruninger
** Last update Wed May 15 10:16:40 2013 martin lequeux-gruninger
*/

/*
**       #include <sys/capability.h> //NOT IN SUSEBOCAL
**
**       int capget(cap_user_header_t hdrp, cap_user_data_t datap);
**
**       int capset(cap_user_header_t hdrp, const cap_user_data_t datap);
**
*/

#include <unistd.h>
#include "aff.h"
#include "strace.h"

void		print_args_capget(struct user_regs_struct *regs, pid_t pid)
{
  (void)pid;
  (void)regs;
  fprintf(stderr, "capget(");
  fprintf(stderr, "UNIMPLEMENTED SYSCALL");
  fprintf(stderr, ")\n");
}

void		print_ret_capget(struct user_regs_struct *regs, pid_t pid)
{
  (void)pid;
  (void)regs;
  fprintf(stderr, "|Ret----capget--->(");
  fprintf(stderr, "UNIMPLEMENTED SYSCALL");
  fprintf(stderr, ")\n");
}
