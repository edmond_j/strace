/*
** header.c for aff in /home/lequeu_m/
**
** Made by martin lequeux-gruninger
** Login   <lequeu_m@epitech.net>
**
** Started on  Fri May 10 17:38:41 2013 martin lequeux-gruninger
** Last update Tue May 14 09:26:11 2013 julien edmond
*/

/*
**       #define _GNU_SOURCE          See feature_test_macros(7)
**       #include <fcntl.h>
**
**       int sync_file_range(int fd, off64_t offset, off64_t nbytes,
**                           unsigned int flags);
**
*/

#include <fcntl.h>
#include <unistd.h>
#include "aff.h"
#include "strace.h"

void		print_args_sync_file_range(struct user_regs_struct *regs,
					   pid_t pid)
{
  (void)pid;
  fprintf(stderr, "sync_file_range(");
  fprintf(stderr, "%d, %lu, %lu, %u", (int)regs->rdi, (off64_t)regs->rsi,
	  (off64_t)regs->rdx, (unsigned)regs->rcx);
  fprintf(stderr, ")\n");
}

void		print_ret_sync_file_range(struct user_regs_struct *regs,
					  pid_t pid)
{
  (void)pid;
  fprintf(stderr, "|Ret----sync_file_range--->(");
  print_errno((int)regs->rax);
  fprintf(stderr, ")\n");
}
