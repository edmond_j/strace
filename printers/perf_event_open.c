/*
** header.c for aff in /home/lequeu_m/
**
** Made by martin lequeux-gruninger
** Login   <lequeu_m@epitech.net>
**
** Started on  Fri May 10 17:38:41 2013 martin lequeux-gruninger
** Last update Wed May 15 13:48:54 2013 julien edmond
*/

/*
**       long sys_rt_sigqueueinfo(int pid, int sig, siginfo_t * uinfo);
**
**       int rt_sigqueueinfo(pid_t tgid, int sig, siginfo_t *uinfo);
**
**       int rt_tgsigqueueinfo(pid_t tgid, pid_t tid, int sig,
**                             siginfo_t *uinfo);
**
*/

#include <unistd.h>
#include "aff.h"
#include "strace.h"

void		print_args_perf_event_open(struct user_regs_struct *regs,
					   pid_t pid)
{
  (void)pid;
  fprintf(stderr, "perf_event_open(");
  fprintf(stderr, "%p, %d, %d, %d, %lu", (void *)regs->rdi, (pid_t)regs->rsi,
	  (int)regs->rdx, (int)regs->rcx, (unsigned long)regs->r8);
  fprintf(stderr, ")\n");
}

void		print_ret_perf_event_open(struct user_regs_struct *regs,
					  pid_t pid)
{
  (void)pid;
  fprintf(stderr, "|Ret----perf_event_open--->(");
  print_errno((int)regs->rax);
  fprintf(stderr, ")\n");
}
