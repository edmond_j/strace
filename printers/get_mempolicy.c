/*
** header.c for aff in /home/lequeu_m/
**
** Made by martin lequeux-gruninger
** Login   <lequeu_m@epitech.net>
**
** Started on  Fri May 10 17:38:41 2013 martin lequeux-gruninger
** Last update Thu May 16 10:17:32 2013 martin lequeux-gruninger
*/

/*
**       #include <numaif.h>
**
**       int get_mempolicy(int *mode, unsigned long *nodemask,
**                         unsigned long maxnode, unsigned long addr,
**                         unsigned long flags);
**
**       Link with -lnuma.
**
*/

#include <unistd.h>
#include "aff.h"
#include "strace.h"

void		print_args_get_mempolicy(struct user_regs_struct *regs, pid_t pid)
{
  (void)pid;
  fprintf(stderr, "get_mempolicy(");
  fprintf(stderr, "%p", (int *)regs->rdi);
  fprintf(stderr, ", %p", (unsigned long *)regs->rsi);
  fprintf(stderr, ", %lu", (unsigned long)regs->rdx);
  fprintf(stderr, ", %lu", (unsigned long)regs->rcx);
  fprintf(stderr, ", %lu", (unsigned long)regs->r8);
  fprintf(stderr, ")\n");
}

void		print_ret_get_mempolicy(struct user_regs_struct *regs, pid_t pid)
{
  (void)pid;
  fprintf(stderr, "|Ret----get_mempolicy--->(");
  print_errno((int)regs->rax);
  fprintf(stderr, ")\n");
}
