/*
** header.c for aff in /home/lequeu_m/
**
** Made by martin lequeux-gruninger
** Login   <lequeu_m@epitech.net>
**
** Started on  Fri May 10 17:38:41 2013 martin lequeux-gruninger
** Last update Wed May 15 17:16:47 2013 julien edmond
*/

/*
**       #include <mqueue.h>
**
**       int mq_send(mqd_t mqdes, const char *msg_ptr,
**                     size_t msg_len, unsigned msg_prio);
**
**       #include <time.h>
**       #include <mqueue.h>
**
**       int mq_timedsend(mqd_t mqdes, const char *msg_ptr,
**                     size_t msg_len, unsigned msg_prio,
**                     const struct timespec *abs_timeout);
**
**       Link with -lrt.
**
**   Feature Test Macro Requirements for glibc (see feature_test_macros(7)):
**
**       mq_timedsend():
**           _XOPEN_SOURCE >= 600 || _POSIX_C_SOURCE >= 200112L
**
*/

#include <mqueue.h>
#include <time.h>
#include <mqueue.h>
#include <unistd.h>
#include "aff.h"
#include "strace.h"

void		print_args_mq_timedsend(struct user_regs_struct *regs,
					pid_t pid)
{
  (void)pid;
  fprintf(stderr, "mq_timedsend(");
  fprintf(stderr, "%d, %p, %lu, %u, %p", (mqd_t)regs->rdi,
	  (const char *)regs->rsi, (size_t)regs->rdx, (unsigned)regs->rcx,
	  (void *)regs->r8);
  fprintf(stderr, ")\n");
}

void		print_ret_mq_timedsend(struct user_regs_struct *regs, pid_t pid)
{
  (void)pid;
  fprintf(stderr, "|Ret----mq_timedsend--->(");
  print_errno((int)regs->rax);
  fprintf(stderr, ")\n");
}
