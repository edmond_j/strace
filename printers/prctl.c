/*
** header.c for aff in /home/lequeu_m/
**
** Made by martin lequeux-gruninger
** Login   <lequeu_m@epitech.net>
**
** Started on  Fri May 10 17:38:41 2013 martin lequeux-gruninger
** Last update Wed May 15 13:41:39 2013 julien edmond
*/

/*
**       #include <sys/prctl.h>
**
**       int prctl(int option, unsigned long arg2, unsigned long arg3,
**                 unsigned long arg4, unsigned long arg5);
**
*/

#include <sys/prctl.h>
#include <unistd.h>
#include "aff.h"
#include "strace.h"

void		print_args_prctl(struct user_regs_struct *regs, pid_t pid)
{
  (void)pid;
  fprintf(stderr, "prctl(");
  fprintf(stderr, "%d, %lu, %lu, %lu, %lu", (int)regs->rdi,
	  (unsigned long)regs->rsi, (unsigned long)regs->rdx,
	  (unsigned long)regs->rcx, (unsigned long)regs->r8);
  fprintf(stderr, ")\n");
}

void		print_ret_prctl(struct user_regs_struct *regs, pid_t pid)
{
  (void)pid;
  fprintf(stderr, "|Ret----prctl--->(");
  print_errno((int)regs->rax);
  fprintf(stderr, ")\n");
}
