/*
** header.c for aff in /home/lequeu_m/
**
** Made by martin lequeux-gruninger
** Login   <lequeu_m@epitech.net>
**
** Started on  Fri May 10 17:38:41 2013 martin lequeux-gruninger
** Last update Wed May 15 12:44:52 2013 julien edmond
*/

/*
**       #define _GNU_SOURCE              See feature_test_macros(7)
**       #include <fcntl.h>
**
**       ssize_t readahead(int fd, off64_t offset, size_t count);
**
*/

#include <fcntl.h>
#include <unistd.h>
#include "aff.h"
#include "strace.h"

void		print_args_readahead(struct user_regs_struct *regs, pid_t pid)
{
  (void)pid;
  fprintf(stderr, "readahead(");
  fprintf(stderr, "%d, 0x%lx, %lu", (int)regs->rdi, (off64_t)regs->rsi,
	  (size_t)regs->rdx);
  fprintf(stderr, ")\n");
}

void		print_ret_readahead(struct user_regs_struct *regs, pid_t pid)
{
  (void)pid;
  fprintf(stderr, "|Ret----readahead--->(");
  if ((ssize_t)regs->rax < 0)
    print_errno((int)regs->rax);
  else
    fprintf(stderr, "%ld", regs->rax);
  fprintf(stderr, ")\n");
}
