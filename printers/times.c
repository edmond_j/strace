/*
** header.c for aff in /home/lequeu_m/
**
** Made by martin lequeux-gruninger
** Login   <lequeu_m@epitech.net>
**
** Started on  Fri May 10 17:38:41 2013 martin lequeux-gruninger
** Last update Tue May 14 11:08:19 2013 julien edmond
*/

/*
**       #include <sys/times.h>
**
**       clock_t times(struct tms *buf);
**
*/

#include <sys/times.h>
#include <unistd.h>
#include "aff.h"
#include "strace.h"

void		print_args_times(struct user_regs_struct *regs, pid_t pid)
{
  (void)pid;
  fprintf(stderr, "times(");
  fprintf(stderr, "%p", (struct tms *)regs->rdi);
  fprintf(stderr, ")\n");
}

void		print_ret_times(struct user_regs_struct *regs, pid_t pid)
{
  (void)pid;
  fprintf(stderr, "|Ret----times--->(");
  if ((clock_t)regs->rax < 0)
    print_errno((int)regs->rax);
  else
    fprintf(stderr, "%lu", (clock_t)regs->rax);
  fprintf(stderr, ")\n");
}
