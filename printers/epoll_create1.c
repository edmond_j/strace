/*
** header.c for aff in /home/lequeu_m/
**
** Made by martin lequeux-gruninger
** Login   <lequeu_m@epitech.net>
**
** Started on  Fri May 10 17:38:41 2013 martin lequeux-gruninger
** Last update Thu May 16 10:24:35 2013 martin lequeux-gruninger
*/

/*
**       #include <sys/epoll.h>
**
**       int epoll_create(int size);
**       int epoll_create1(int flags);
**
*/

#include <sys/epoll.h>
#include <unistd.h>
#include "aff.h"
#include "strace.h"

void		print_args_epoll_create1(struct user_regs_struct *regs, pid_t pid)
{
  (void)pid;
  fprintf(stderr, "epoll_create1(");
  fprintf(stderr, "%d", (int)regs->rdi);
  fprintf(stderr, ")\n");
}

void		print_ret_epoll_create1(struct user_regs_struct *regs, pid_t pid)
{
  (void)pid;
  fprintf(stderr, "|Ret----epoll_create1--->(");
  print_errno((int)regs->rax);
  fprintf(stderr, ")\n");
}
