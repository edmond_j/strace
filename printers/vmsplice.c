/*
** header.c for aff in /home/lequeu_m/
**
** Made by martin lequeux-gruninger
** Login   <lequeu_m@epitech.net>
**
** Started on  Fri May 10 17:38:41 2013 martin lequeux-gruninger
** Last update Mon May 13 18:15:41 2013 julien edmond
*/

/*
**       #define _GNU_SOURCE          See feature_test_macros(7)
**       #include <fcntl.h>
**       #include <sys/uio.h>
**
**       ssize_t vmsplice(int fd, const struct iovec *iov,
**                        unsigned long nr_segs, unsigned int flags);
**
*/

#include <fcntl.h>
#include <sys/uio.h>
#include <unistd.h>
#include "aff.h"
#include "strace.h"

void		print_args_vmsplice(struct user_regs_struct *regs, pid_t pid)
{
  (void)pid;
  fprintf(stderr, "vmsplice(");
  fprintf(stderr, "%d, %p, %lu, %u",
	  (int)regs->rdi, (const struct iovec *)regs->rsi,
	  (unsigned long)regs->rdx, (unsigned)regs->rcx);
  fprintf(stderr, ")\n");
}

void		print_ret_vmsplice(struct user_regs_struct *regs, pid_t pid)
{
  (void)pid;
  fprintf(stderr, "|Ret----vmsplice--->(");
  print_errno((ssize_t)regs->rax);
  fprintf(stderr, ")\n");
}
