/*
** header.c for aff in /home/lequeu_m/
**
** Made by martin lequeux-gruninger
** Login   <lequeu_m@epitech.net>
**
** Started on  Fri May 10 17:38:41 2013 martin lequeux-gruninger
** Last update Tue May 14 11:15:47 2013 julien edmond
*/

/*
**       #include <sys/types.h>
**       #include <unistd.h>
**
**       int setreuid(uid_t ruid, uid_t euid);
**       int setregid(gid_t rgid, gid_t egid);
**
**   Feature Test Macro Requirements for glibc (see feature_test_macros(7)):
**
**       setreuid(), setregid():
**           _BSD_SOURCE || _XOPEN_SOURCE >= 500 ||
**           _XOPEN_SOURCE && _XOPEN_SOURCE_EXTENDED
**
*/

#include <sys/types.h>
#include <unistd.h>
#include <unistd.h>
#include "aff.h"
#include "strace.h"

void		print_args_setreuid(struct user_regs_struct *regs, pid_t pid)
{
  (void)pid;
  fprintf(stderr, "setreuid(");
  fprintf(stderr, "%d, %d", (uid_t)regs->rdi, (uid_t)regs->rsi);
  fprintf(stderr, ")\n");
}

void		print_ret_setreuid(struct user_regs_struct *regs, pid_t pid)
{
  (void)pid;
  fprintf(stderr, "|Ret----setreuid--->(");
  print_errno((int)regs->rax);
  fprintf(stderr, ")\n");
}
