/*
** header.c for aff in /home/lequeu_m/
**
** Made by martin lequeux-gruninger
** Login   <lequeu_m@epitech.net>
**
** Started on  Fri May 10 17:38:41 2013 martin lequeux-gruninger
** Last update Thu May 16 10:21:42 2013 martin lequeux-gruninger
*/

/*
**       #include <sys/stat.h>
**
**       int chmod(const char *path, mode_t mode);
**       int fchmod(int fd, mode_t mode);
**
**   Feature Test Macro Requirements for glibc (see feature_test_macros(7)):
**
**       fchmod():
**           _BSD_SOURCE || _XOPEN_SOURCE >= 500 ||
**           _XOPEN_SOURCE && _XOPEN_SOURCE_EXTENDED
**           ||  Since glibc 2.12:  _POSIX_C_SOURCE >= 200809L
**
*/

#include <sys/stat.h>
#include <unistd.h>
#include "aff.h"
#include "strace.h"

void		print_args_chmod(struct user_regs_struct *regs, pid_t pid)
{
  fprintf(stderr, "chmod(");
  print_string(pid, (void *)regs->rdi);
  fprintf(stderr, ", %d", (int)regs->rsi);
  fprintf(stderr, ")\n");
}

void		print_ret_chmod(struct user_regs_struct *regs, pid_t pid)
{
  (void)pid;
  fprintf(stderr, "|Ret----chmod--->(");
  print_errno((int)regs->rax);
  fprintf(stderr, ")\n");
}
