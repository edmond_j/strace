/*
** header.c for aff in /home/lequeu_m/
**
** Made by martin lequeux-gruninger
** Login   <lequeu_m@epitech.net>
**
** Started on  Fri May 10 17:38:41 2013 martin lequeux-gruninger
** Last update Wed May 15 11:34:44 2013 julien edmond
*/

/*
**       long sys_rt_sigqueueinfo(int pid, int sig, siginfo_t * uinfo);
**
**       int rt_sigqueueinfo(pid_t tgid, int sig, siginfo_t *uinfo);
**
**       int rt_tgsigqueueinfo(pid_t tgid, pid_t tid, int sig,
**                             siginfo_t *uinfo);
**
*/

#include <unistd.h>
#include "aff.h"
#include "strace.h"

void		print_args_rt_sigqueueinfo(struct user_regs_struct *regs,
					   pid_t pid)
{
  (void)pid;
  fprintf(stderr, "rt_sigqueueinfo(");
  fprintf(stderr, "%d, %d, %p", (int)regs->rdi, (int)regs->rsi,
	  (void *)regs->rdx);
  fprintf(stderr, ")\n");
}

void		print_ret_rt_sigqueueinfo(struct user_regs_struct *regs,
					  pid_t pid)
{
  (void)pid;
  fprintf(stderr, "|Ret----rt_sigqueueinfo--->(");
  if ((long)regs->rax < 0)
    print_errno((int)regs->rax);
  else
    fprintf(stderr, "%ld", regs->rax);
  fprintf(stderr, ")\n");
}
