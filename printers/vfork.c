/*
** header.c for aff in /home/lequeu_m/
**
** Made by martin lequeux-gruninger
** Login   <lequeu_m@epitech.net>
**
** Started on  Fri May 10 17:38:41 2013 martin lequeux-gruninger
** Last update Mon May 13 14:33:38 2013 julien edmond
*/

/*
**       #include <sys/types.h>
**       #include <unistd.h>
**
**       pid_t vfork(void);
**
**   Feature Test Macro Requirements for glibc (see feature_test_macros(7)):
**
**       vfork():
**           Since glibc 2.12:
**               _BSD_SOURCE ||
**                   (_XOPEN_SOURCE >= 500 ||
**                       _XOPEN_SOURCE && _XOPEN_SOURCE_EXTENDED) &&
**                   !(_POSIX_C_SOURCE >= 200809L || _XOPEN_SOURCE >= 700)
**           Before glibc 2.12:
**               _BSD_SOURCE || _XOPEN_SOURCE >= 500 ||
**               _XOPEN_SOURCE && _XOPEN_SOURCE_EXTENDED
**
*/

#include <sys/types.h>
#include <unistd.h>
#include <unistd.h>
#include "aff.h"
#include "strace.h"

void		print_args_vfork(struct user_regs_struct *regs, pid_t pid)
{
  (void)pid;
  (void)regs;
  fprintf(stderr, "vfork(");
  fprintf(stderr, ")\n");
}

void		print_ret_vfork(struct user_regs_struct *regs, pid_t pid)
{
  (void)pid;
  fprintf(stderr, "|Ret----vfork--->(");
  print_errno((int)regs->rax);
  fprintf(stderr, ")\n");
}
